import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:test/test.dart';

void main() {
  test('Valid input urls', () {
    const String filePath =
        '/home/fastocloud/streamer/proxy/Media/TV/Ballers/Season 5/Ballers.2015.S05E02.720p.HEVC.x265-MeGusta.mkv';
    final server = LiveServer.createDefault();
    final generated = server.generateHttpProxyUrl(filePath);
    expect(generated.toString(),
        'http://0.0.0.0:8000/fastocloud/proxy/Media/TV/Ballers/Season%205/Ballers.2015.S05E02.720p.HEVC.x265-MeGusta.mkv');
  });
}

import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:test/test.dart';

void main() {
  test('Valid License Key', () {
    const String valid =
        '006f68a6d700005eb12ee30000aa54e02c0170455b1af40980901c4ef60470eea82cf50b3044e891c1569c5a9fafd7f00';
    expect(valid.isValidActivationKey(), true);

    const String invalid =
        '006f68a6d700005eb12ee30000aa54e02c0170455b1af40980901c4ef60470eea82cf50b3044e891c1569c5a9fafd7f0';
    expect(invalid.isValidActivationKey(), false);

    const String invalidLast =
        '006f68a6d700005eb12ee30000aa54e02c0170455b1af40980901c4ef60470eea82cf50b3044e891c1569c5a9fafd7f0F';
    expect(invalidLast.isValidActivationKey(), false);
  });
}

import 'dart:convert';

import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:test/test.dart';

void main() {
  test('VODs/Series parse', () {
    const j =
        '{"country":"USA","created_date":1590167538319,"description":"","duration":3600000,"groups":[],"iarc":21,'
        '"id":"5ec807f2b06990fd1f37350f","meta":[],"name":"9-1-1: Lone Star S01 E01", '
        '"output":[{"id":0,"uri":"http://live.realiptv.to:25461/series/yourtviptv/S3GwaVdGKY/48981.mp4"}],"parts":[],'
        '"price":0,"prime_date":0,"trailer_url":"https://fastocloud.com/static/video/invalid_trailer.m3u8","tvg_id":"",'
        '"tvg_logo":"http://live.realiptv.to:25461/images/2912ece109615b91af9f54c483b098f9.jpg","tvg_name":"","type":1,'
        '"user_score":0,"view_count":0,"visible":true,"vod_type":0, "archive":false}';
    const j2 =
        '{"audio_parser":"aacparse","country":"USA","cpu":0,"created_date":1590682868814,"description":"",'
        '"duration":3600000,"feedback_directory":"~/streamer/feedback/8/5ecfe4f4fe39662d49ceed96",'
        '"groups":["SERIES"],"have_audio":true,"have_video":true,"iarc":18,"id":"5ecfe4f4fe39662d49ceed96",'
        '"idle_time":0,"input":[{"_cls":"pyfastocloud_models.common_entries.InputUrl","id":1,"uri":"https://youtu.be/qN4sAmZnHws"}],'
        '"input_streams":[],"log_level":6,"loop":false,"loop_start_time":0,"meta":[],"name":"Cennet",'
        '"output":[{"hls_type":0,"http_root":"~/streamer/vods/8/5ecfe4f4fe39662d49ceed96/0","id":0,'
        '"uri":"http://yourtvmedia.net:7000/8/5ecfe4f4fe39662d49ceed96/0/master.m3u8"}],"output_streams":[],"parts":[],'
        '"price":0,"prime_date":0,"quality":100,"restart_attempts":10,"restarts":0,"rss":0,"start_time":0,"status":0,'
        '"timestamp":0,"trailer_url":"https://fastocloud.com/static/video/invalid_trailer.m3u8","tvg_id":"",'
        '"tvg_logo":"https://m.media-amazon.com/images/M/MV5BOTMyNDgxMmUtNGQxZC00YzM5LThhZWYtNTNjYjIwZWQyMTg5XkEyXkFqcGdeQXVyODY1MDkwOQ@@._V1_UY268_CR12,0,182,268_AL_.jpg",'
        '"tvg_name":"","type":8,"archive":false,"user_score":0,"video_parser":"h264parse","view_count":2,"visible":true,"vod_type":1, "relay_video_type":0, "relay_audio_type":1, "phoenix":false, "auto_start":false, "audio_tracks_count":1}';

    final proxyVod = makeStream(json.decode(j));
    final restreamVod = makeStream(json.decode(j2));

    expect(proxyVod?.isSerial(), false);
    expect(restreamVod?.isSerial(), true);
  });

  test('TimeShift recorder create', () {
    final recorder = TimeshiftRecorderStream(
        id: "1234", input: [InputUrl(id: 0, uri: "https://youtu.be/qN4sAmZnHws")]);
    expect(recorder.isValid(), true);
  });
}

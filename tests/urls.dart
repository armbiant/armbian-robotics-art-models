import 'package:fastocloud_dart_models/models.dart';
import 'package:test/test.dart';

void main() {
  test('Valid input urls', () {
    final url = InputUrl(
        id: 0,
        uri:
            'file:///home/ytvmedia/Media/TV/Ballers/Season 5/Ballers.2015.S05E02.720p.HEVC.x265-MeGusta.mkv');
    expect(url.isValid(), true);
  });

  test('Valid output urls', () {
    final url = OutputUrl(
        id: 0,
        uri:
            'file:///home/ytvmedia/Media/TV/Ballers/Season 5/Ballers.2015.S05E02.720p.HEVC.x265-MeGusta.mkv');
    expect(url.isValid(), true);
  });

  test('Valid rtmp', () {
    final url = CustomRtmpOut(id: 0, uri: 'rtmp://sg1.example.com:1935/live/ssss22');
    expect(url.isValid(), true);
    expect(url.root(), 'rtmp://sg1.example.com:1935/live/');
    expect(url.key(), 'ssss22');
    expect(url.uri, IRtmpOutputUrl.generateUrl(url.root(), url.key()));
  });

  test('Empty rtmp', () {
    final url = CustomRtmpOut(id: 0, uri: '');
    expect(url.isValid(), false);
    expect(url.root(), null);
    expect(url.key(), null);
    expect(null, IRtmpOutputUrl.generateUrl(url.root(), url.key()));
  });

  test('Valid facebook rtmp', () {
    final url = FacebookRtmpOut(
        id: 0,
        uri:
            'rtmps://live-api-s.facebook.com:443/rtmp/23111?s_bl=1&s_ps=1&s_psm=1&s_sw=0&s_vt=api-s&a=gree');
    expect(url.isValid(), true);
    expect(url.root(), 'rtmps://live-api-s.facebook.com:443/rtmp/');
    expect(url.key(), '23111?s_bl=1&s_ps=1&s_psm=1&s_sw=0&s_vt=api-s&a=gree');
    expect(url.uri, IRtmpOutputUrl.generateUrl(url.root(), url.key()));
  });

  test('Valid youtube rtmp', () {
    final url =
        YouTubeRtmpOut(id: 0, uri: 'rtmp://a.rtmp.youtube.com/live2/up36-rtmj-t98g-vxc6-bkzy');
    expect(url.isValid(), true);
    expect(url.root(), 'rtmp://a.rtmp.youtube.com/live2/');
    expect(url.key(), 'up36-rtmj-t98g-vxc6-bkzy');
    expect(url.uri, IRtmpOutputUrl.generateUrl(url.root(), url.key()));
  });

  test('Valid youtube rtmp', () {
    final url = VkontakteRtmpOut(
        id: 0, uri: 'rtmp://stream.vkuserlive.com:443/vlive.567.eyJuMDh9/Fv3_JmFZp05G4p');
    expect(url.isValid(), true);
    expect(url.root(), 'rtmp://stream.vkuserlive.com:443/vlive.567.eyJuMDh9/');
    expect(url.key(), 'Fv3_JmFZp05G4p');
    expect(url.uri, IRtmpOutputUrl.generateUrl(url.root(), url.key()));
  });

  test('Valid fake output', () {
    final url = UnknownOutputUrl.createFake(id: 0);
    expect(url.isValid(), true);
    expect(Protocol.UNKNOWN, Protocol.fromString(url.uri));
  });
}

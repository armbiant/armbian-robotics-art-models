import 'package:fastocloud_dart_models/src/models/ml.dart';
import 'package:test/test.dart';

void main() {
  test('Color background', () {
    final Map<String, dynamic> json = {
      BackgroundEffect.TYPE_FIELD: BackgroundEffectType.COLOR.toInt(),
      ColorBackgroundEffect.COLOR_FIELD: 0xFFFFFFFF
    };
    final result = BackgroundEffect.fromJson(json);
    expect(result.type, BackgroundEffectType.COLOR);
    expect((result as ColorBackgroundEffect).hex, 0xFFFFFFFF);
  });
}

class BackgroundEffectType {
  static const int _BLUR = 0;
  static const int _IMAGE = 1;
  static const int _COLOR = 2;

  final int _value;

  const BackgroundEffectType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _BLUR) {
      return 'Blur';
    } else if (_value == _IMAGE) {
      return 'Image';
    }
    assert(_value == _COLOR);
    return 'Color';
  }

  factory BackgroundEffectType.fromInt(int type) {
    if (type == _BLUR) {
      return BackgroundEffectType.BLUR;
    } else if (type == _IMAGE) {
      return BackgroundEffectType.IMAGE;
    } else if (type == _COLOR) {
      return BackgroundEffectType.COLOR;
    }

    throw 'Unknown background type: $type';
  }

  static List<BackgroundEffectType> get values => [BLUR, IMAGE, COLOR];

  static const BackgroundEffectType BLUR = BackgroundEffectType._(_BLUR);
  static const BackgroundEffectType IMAGE = BackgroundEffectType._(_IMAGE);
  static const BackgroundEffectType COLOR = BackgroundEffectType._(_COLOR);
}

abstract class BackgroundEffect {
  static const String TYPE_FIELD = 'type';

  final BackgroundEffectType type;

  const BackgroundEffect(this.type);

  bool isValid();

  factory BackgroundEffect.fromJson(Map<String, dynamic> json) {
    final BackgroundEffectType type = BackgroundEffectType.fromInt(json[TYPE_FIELD]);
    if (type == BackgroundEffectType.BLUR) {
      return BlurBackgroundEffect.fromJson(json);
    } else if (type == BackgroundEffectType.IMAGE) {
      return ImageBackgroundEffect.fromJson(json);
    } else if (type == BackgroundEffectType.COLOR) {
      return ColorBackgroundEffect.fromJson(json);
    }

    throw 'Unknown background type: $type';
  }

  Map<String, dynamic> toJson() {
    return {TYPE_FIELD: type.toInt()};
  }
}

class BlurBackgroundEffect extends BackgroundEffect {
  static const String STRENGTH_FIELD = 'strength';

  BlurBackgroundEffect([this.strength = 0.5]) : super(BackgroundEffectType.BLUR);

  BlurBackgroundEffect copy() {
    return BlurBackgroundEffect();
  }

  double strength;

  @override
  bool isValid() {
    return strength >= 0 && strength <= 1.0;
  }

  factory BlurBackgroundEffect.fromJson(Map<String, dynamic> json) {
    return BlurBackgroundEffect(json[STRENGTH_FIELD]);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[STRENGTH_FIELD] = strength;
    return result;
  }
}

class ImageBackgroundEffect extends BackgroundEffect {
  static const String IMAGE_FIELD = 'image';

  ImageBackgroundEffect(this.path) : super(BackgroundEffectType.IMAGE);

  ImageBackgroundEffect copy() {
    return ImageBackgroundEffect(path);
  }

  ImageBackgroundEffect.createDefault()
      : path = 'file:///home/fastocloud/background.png',
        super(BackgroundEffectType.IMAGE);

  String path;

  @override
  bool isValid() {
    return path.isNotEmpty;
  }

  factory ImageBackgroundEffect.fromJson(Map<String, dynamic> json) {
    return ImageBackgroundEffect(json[IMAGE_FIELD]);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[IMAGE_FIELD] = path;
    return result;
  }
}

class ColorBackgroundEffect extends BackgroundEffect {
  static const String COLOR_FIELD = 'color';

  ColorBackgroundEffect([this.hex = 0xFF00FF00]) : super(BackgroundEffectType.COLOR);

  ColorBackgroundEffect copy() {
    return ColorBackgroundEffect();
  }

  int hex;

  @override
  bool isValid() {
    return true;
  }

  factory ColorBackgroundEffect.fromJson(Map<String, dynamic> json) {
    return ColorBackgroundEffect(json[COLOR_FIELD]);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[COLOR_FIELD] = hex;
    return result;
  }
}

import 'package:fastocloud_dart_models/src/models/stream.dart';

class UserStreamInfo {
  static const _FAVORITE_FIELD = 'favorite';
  static const _PRIVATE_FIELD = 'private';
  static const _RECENT_FIELD = 'recent';
  static const _LOCKED_FIELD = 'locked';
  static const _INTERRUPTION_TIME_FIELD = 'interruption_time';

  final bool favorite; // not changed
  final bool locked;
  final bool private;
  final int recent;
  final int interruptionTime;
  final IStream sid;

  UserStreamInfo(
      {required this.favorite,
      required this.locked,
      required this.private,
      required this.recent,
      required this.interruptionTime,
      required this.sid});

  UserStreamInfo copy() {
    return UserStreamInfo(
        favorite: favorite,
        locked: locked,
        private: private,
        recent: recent,
        interruptionTime: interruptionTime,
        sid: sid);
  }

  factory UserStreamInfo.fromJson(Map<String, dynamic> json) {
    final favorite = json[_FAVORITE_FIELD];
    final private = json[_PRIVATE_FIELD];
    final recent = json[_RECENT_FIELD];
    final locked = json[_LOCKED_FIELD];
    final interruption = json[_INTERRUPTION_TIME_FIELD];
    final sid = makeStream(json);
    if (sid == null) {
      throw 'Invalid input. Unable to make stream';
    }
    return UserStreamInfo(
        favorite: favorite,
        private: private,
        recent: recent,
        locked: locked,
        interruptionTime: interruption,
        sid: sid);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = sid.toJson();
    result[_FAVORITE_FIELD] = favorite;
    result[_PRIVATE_FIELD] = private;
    result[_RECENT_FIELD] = recent;
    result[_LOCKED_FIELD] = locked;
    result[_INTERRUPTION_TIME_FIELD] = interruptionTime;
    return result;
  }
}

import 'dart:convert';

import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/ott_server_info.dart';
import 'package:fastotv_dart/commands_info/types.dart';

class WSServer {
  static const URL_FIELD = "url";
  static const LOGIN_FIELD = "login";
  static const PASSWORD_FIELD = "password";

  static const DEFAULT_URL = "https://api.fastocloud.com";
  static const DEFAULT_LOGIN = "fastocloud";
  static const DEFAULT_PASSWORD = "fastocloud";

  String url;
  String? login;
  String? password;

  WSServer({required this.url, this.login, this.password});

  bool isValid() {
    final req = url.isNotEmpty;
    return req;
  }

  bool needAuth() {
    return login != null && password != null;
  }

  String? encodedAuth() {
    return needAuth() ? base64Encode(utf8.encode('$login:$password')) : null;
  }

  String? urlScheme() {
    final uri = Uri.tryParse(url);
    if (uri == null) {
      return null;
    }
    return uri.scheme;
  }

  WSServer.createDefault()
      : url = DEFAULT_URL,
        login = DEFAULT_LOGIN,
        password = DEFAULT_PASSWORD;

  factory WSServer.fromJson(Map<String, dynamic> json) {
    final url = json[URL_FIELD];
    String? login;
    String? password;
    if (json.containsKey(LOGIN_FIELD)) {
      login = json[LOGIN_FIELD];
    }
    if (json.containsKey(PASSWORD_FIELD)) {
      password = json[PASSWORD_FIELD];
    }
    return WSServer(url: url, login: login, password: password);
  }

  WSServer copy() {
    return WSServer(url: url, login: login, password: password);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[URL_FIELD] = url;
    if (login != null) {
      result[LOGIN_FIELD] = login;
    }
    if (password != null) {
      result[PASSWORD_FIELD] = password;
    }
    return result;
  }

  String? makeWsUrl(String version, GlobalTheme theme, WsMode mode, String languageCode) {
    final scheme = urlScheme();
    if (scheme == null) {
      return null;
    }

    return makeWsUrlWithHost(
        '$scheme://$version.$scheme.fastocloud.com', theme, mode, languageCode);
  }

  String makeWsUrlWithHost(
      String schemeAndHost, GlobalTheme theme, WsMode mode, String languageCode) {
    String prepareConnectionUrl(GlobalTheme theme, WsMode mode, String languageCode) {
      String query = '/?url=$url&theme=${theme.toInt()}&locale=$languageCode';
      final auth = encodedAuth();
      if (auth != null) {
        query += '&secret=$auth';
      }

      query += '&mode=${mode.toInt()}';
      return base64Encode(utf8.encode(query));
    }

    final stabled = prepareConnectionUrl(theme, mode, languageCode);
    return '$schemeAndHost/#/?url=$stabled';
  }
}

WSServer? makeWSServerFromEncodedAuth(String url, String encoded) {
  String? login;
  String? password;
  try {
    final data = base64Decode(encoded);
    final creds = utf8.decode(data);
    final parts = creds.split(':');
    if (parts.length == 2) {
      login = parts[0];
      password = parts[1];
    }
  } catch (error) {
    return null;
  }
  return WSServer(url: url, login: login, password: password);
}

class GoServer {
  static const _ID_FIELD = "id";
  static const _NAME_FIELD = "name";
  static const _HOST_FIELD = "host";
  static const _PRICE_FIELD = "price";
  static const _DESCRIPTION_FIELD = "description";
  static const _VISIBLE_FIELD = "visible";
  static const _CREATED_DATE_FIELD = 'created_date';
  static const _BACKGROUND_URL_FIELD = 'background_url';
  static const _LOCAL_FIELD = 'local';

  static const String DEFAULT_SERVER_NAME = 'Package';

  final String? id;
  String name;
  WSServer host;
  PricePack? pricePackage;
  String description;
  bool visible;
  String backgroundUrl;
  bool local;
  final int? createdDate;

  GoServer(
      {required this.id,
      this.createdDate,
      required this.name,
      required this.host,
      required this.local,
      required this.pricePackage,
      required this.description,
      required this.visible,
      required this.backgroundUrl});

  GoServer.createDefault(String background, WSServer ws, {bool isLocal = false})
      : id = null,
        createdDate = null,
        name = DEFAULT_SERVER_NAME,
        host = ws,
        local = isLocal,
        visible = true,
        backgroundUrl = background,
        pricePackage = null,
        description = 'This is awesome package!';

  GoServer copy() {
    return GoServer(
        id: id,
        name: name,
        createdDate: createdDate,
        host: host.copy(),
        local: local,
        visible: visible,
        backgroundUrl: backgroundUrl,
        description: description,
        pricePackage: pricePackage);
  }

  bool isValid() {
    final req = name.isValidServiceName() && host.isValid();
    return req;
  }

  factory GoServer.fromJson(Map<String, dynamic> json) {
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    int? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    PricePack? price;
    if (json.containsKey(_PRICE_FIELD)) {
      price = PricePack.fromJson(json[_PRICE_FIELD]);
    }
    return GoServer(
        id: id,
        createdDate: createdDate,
        name: json[_NAME_FIELD],
        host: WSServer.fromJson(json[_HOST_FIELD]),
        local: json[_LOCAL_FIELD],
        pricePackage: price,
        description: json[_DESCRIPTION_FIELD],
        visible: json[_VISIBLE_FIELD],
        backgroundUrl: json[_BACKGROUND_URL_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    result[_NAME_FIELD] = name;
    result[_HOST_FIELD] = host.toJson();
    result[_LOCAL_FIELD] = local;
    if (pricePackage != null) {
      result[_PRICE_FIELD] = pricePackage!.toJson();
    }
    result[_DESCRIPTION_FIELD] = description;
    result[_BACKGROUND_URL_FIELD] = backgroundUrl;
    result[_VISIBLE_FIELD] = visible;
    return result;
  }
}

class PaymentType {
  static const int _STRIPE = 0;
  static const int _PAYPAL = 1;

  final int _value;

  const PaymentType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _STRIPE) {
      return 'Stripe';
    }
    assert(_value == _PAYPAL);
    return 'Paypal';
  }

  factory PaymentType.fromInt(int type) {
    if (type == _STRIPE) {
      return PaymentType.STRIPE;
    } else if (type == _PAYPAL) {
      return PaymentType.PAYPAL;
    }

    throw 'Unknown payment type: $type';
  }

  static List<PaymentType> get values => [STRIPE, PAYPAL];

  static const PaymentType STRIPE = PaymentType._(_STRIPE);
  static const PaymentType PAYPAL = PaymentType._(_PAYPAL);
}

abstract class IPayment {
  static const TYPE_FIELD = 'type';

  final PaymentType type;

  IPayment({required this.type});

  IPayment copy();

  factory IPayment.fromJson(Map<String, dynamic> json) {
    final type = PaymentType.fromInt(json[TYPE_FIELD]);

    if (type == PaymentType.STRIPE) {
      return PaymentStripe.fromJson(json);
    } else if (type == PaymentType.PAYPAL) {
      return PaymentPaypal.fromJson(json);
    }

    throw 'Unknown payment type: $type';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {TYPE_FIELD: type.toInt()};
    return result;
  }
}

class PaymentStripe extends IPayment {
  static const STRIPE_FIELD = 'stripe';

  PaymentStripeData stripe;

  PaymentStripe({required this.stripe}) : super(type: PaymentType.STRIPE);

  PaymentStripe.createDefault()
      : stripe = PaymentStripeData.createDefault(),
        super(type: PaymentType.STRIPE);

  @override
  PaymentStripe copy() {
    return PaymentStripe(stripe: stripe.copy());
  }

  factory PaymentStripe.fromJson(Map<String, dynamic> json) {
    return PaymentStripe(stripe: PaymentStripeData.fromJson(json[STRIPE_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[STRIPE_FIELD] = stripe.toJson();
    return result;
  }
}

class PaymentStripeData {
  static const PUB_KEY_FIELD = 'pub_key';
  static const SECRET_KEY_FIELD = 'secret_key';

  String pubKey;
  String secretKey;

  PaymentStripeData({required this.pubKey, required this.secretKey});

  PaymentStripeData.createDefault()
      : pubKey = 'SOME_PUB_KEY',
        secretKey = 'SOME_SECRET_KEY';

  PaymentStripeData copy() {
    return PaymentStripeData(pubKey: pubKey, secretKey: secretKey);
  }

  factory PaymentStripeData.fromJson(Map<String, dynamic> json) {
    return PaymentStripeData(pubKey: json[PUB_KEY_FIELD], secretKey: json[SECRET_KEY_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[PUB_KEY_FIELD] = pubKey;
    result[SECRET_KEY_FIELD] = secretKey;
    return result;
  }
}

class PaymentPaypal extends IPayment {
  static const PAYPAL_FIELD = 'paypal';

  PaymentPaypalData paypal;

  PaymentPaypal({required this.paypal}) : super(type: PaymentType.PAYPAL);

  PaymentPaypal.createDefault()
      : paypal = PaymentPaypalData.createDefault(),
        super(type: PaymentType.PAYPAL);

  @override
  PaymentPaypal copy() {
    return PaymentPaypal(paypal: paypal.copy());
  }

  factory PaymentPaypal.fromJson(Map<String, dynamic> json) {
    return PaymentPaypal(paypal: PaymentPaypalData.fromJson(json[PAYPAL_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[PAYPAL_FIELD] = paypal.toJson();
    return result;
  }
}

class PaymentPaypalData {
  static const CLIENT_ID_FIELD = 'client_id';
  static const CLIENT_SECRET_FIELD = 'client_secret';
  static const MODE_FIELD = 'mode';

  String clientId;
  String clientSecret;
  PaypalMode mode;

  PaymentPaypalData({required this.clientId, required this.clientSecret, required this.mode});

  PaymentPaypalData.createDefault()
      : clientId = 'SOME_CLIENT_ID',
        clientSecret = 'SOME_CLIENT_SECRET',
        mode = PaypalMode.SANDBOX;

  PaymentPaypalData copy() {
    return PaymentPaypalData(clientId: clientId, clientSecret: clientSecret, mode: mode);
  }

  factory PaymentPaypalData.fromJson(Map<String, dynamic> json) {
    return PaymentPaypalData(
        clientId: json[CLIENT_ID_FIELD],
        clientSecret: json[CLIENT_SECRET_FIELD],
        mode: PaypalMode.fromInt(json[MODE_FIELD]));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[CLIENT_ID_FIELD] = clientId;
    result[CLIENT_SECRET_FIELD] = clientSecret;
    result[MODE_FIELD] = mode.toInt();
    return result;
  }
}

class PaypalMode {
  static const int _SANDBOX = 0;
  static const int _LIVE = 1;

  final int _value;

  const PaypalMode._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _SANDBOX) {
      return 'Sandbox';
    }

    assert(_value == _LIVE);
    return 'Live';
  }

  factory PaypalMode.fromInt(int type) {
    if (type == _SANDBOX) {
      return PaypalMode.SANDBOX;
    } else if (type == _LIVE) {
      return PaypalMode.LIVE;
    }

    throw 'Unknown paypal mode type: $type';
  }

  static List<PaypalMode> get values => [SANDBOX, LIVE];

  static const PaypalMode SANDBOX = PaypalMode._(_SANDBOX);
  static const PaypalMode LIVE = PaypalMode._(_LIVE);
}

// enum
class NotificationType {
  static const int _MAIL = 0;
  static const int _PHONE = 1;

  final int _value;

  const NotificationType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _MAIL) {
      return 'Email';
    }
    assert(_value == _PHONE);
    return 'Phone';
  }

  factory NotificationType.fromInt(int type) {
    if (type == _MAIL) {
      return NotificationType.MAIL;
    } else if (type == _PHONE) {
      return NotificationType.PHONE;
    }

    throw 'Unknown payment type: $type';
  }

  static List<NotificationType> get values => [MAIL, PHONE];

  static const NotificationType MAIL = NotificationType._(_MAIL);
  static const NotificationType PHONE = NotificationType._(_PHONE);
}

abstract class INotification {
  static const TYPE_FIELD = 'type';

  final NotificationType type;

  INotification({required this.type});

  factory INotification.fromJson(Map<String, dynamic> json) {
    final type = NotificationType.fromInt(json[TYPE_FIELD]);

    if (type == NotificationType.MAIL) {
      return MailConfig.fromJson(json);
    }

    throw 'Unknown notification type: $type';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {TYPE_FIELD: type.toInt()};
    return result;
  }
}

class MailConfig extends INotification {
  static const MAIL_SERVER_FIELD = 'mail_server';
  static const USER_FIELD = 'user';
  static const MAIL_PASSWORD_FIELD = 'mail_password';
  static const MAIL_PORT_FIELD = 'mail_port';
  static const EMAIL_SERVER_SETTINGS = 'email_server_settings';

  String mailServer;
  String user;
  String mailPassword;
  int mailPort;

  MailConfig({
    required this.mailServer,
    required this.user,
    required this.mailPassword,
    required this.mailPort,
  }) : super(type: NotificationType.MAIL);

  MailConfig.createDefault()
      : mailServer = 'smtp.office365.com',
        user = 'support@fastocloud.com',
        mailPassword = '1111',
        mailPort = 587,
        super(type: NotificationType.MAIL);

  factory MailConfig.fromJson(Map<String, dynamic> json) {
    final result = json[EMAIL_SERVER_SETTINGS];
    return MailConfig(
        mailServer: result[MAIL_SERVER_FIELD],
        user: result[USER_FIELD],
        mailPassword: result[MAIL_PASSWORD_FIELD],
        mailPort: result[MAIL_PORT_FIELD]);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[EMAIL_SERVER_SETTINGS] = {
      MAIL_SERVER_FIELD: mailServer,
      USER_FIELD: user,
      MAIL_PASSWORD_FIELD: mailPassword,
      MAIL_PORT_FIELD: mailPort
    };
    return result;
  }
}

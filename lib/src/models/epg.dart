import 'package:fastocloud_dart_models/src/models/base.dart';
import 'package:fastocloud_dart_models/src/models/machine.dart';
import 'package:fastocloud_dart_models/src/models/server_base.dart';
import 'package:fastocloud_dart_models/src/models/server_provider.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/client_info.dart';

class EpgOnlineUsers {
  static const _DAEMON_FIELD = 'daemon';

  final int daemon;

  EpgOnlineUsers({required this.daemon});

  EpgOnlineUsers copy() {
    return EpgOnlineUsers(daemon: daemon);
  }

  factory EpgOnlineUsers.fromJson(Map<String, dynamic> json) {
    final daemon = json[_DAEMON_FIELD];
    return EpgOnlineUsers(daemon: daemon);
  }

  Map<String, dynamic> toJson() {
    return {_DAEMON_FIELD: daemon};
  }
}

class EpgServerInfo extends Machine {
  static const _ONLINE_USERS_FIELDS = 'online_users';

  EpgOnlineUsers? onlineUsers;

  EpgServerInfo(
      {required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      required int totalBytesIn,
      required int totalBytesOut,
      int? syncTime,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      this.onlineUsers})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            syncTime: syncTime,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os);

  EpgServerInfo.createInit()
      : onlineUsers = null,
        super.createInit();

  @override
  EpgServerInfo copy() {
    return EpgServerInfo(
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        timestamp: timestamp,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        syncTime: syncTime,
        version: version,
        project: project,
        status: status,
        expirationTime: expirationTime,
        os: os);
  }

  factory EpgServerInfo.fromJson(Map<String, dynamic> json) {
    final Machine mach = Machine.fromJson(json);
    EpgOnlineUsers? onlineUsers;
    if (json.containsKey(_ONLINE_USERS_FIELDS)) {
      onlineUsers = EpgOnlineUsers.fromJson(json[_ONLINE_USERS_FIELDS]);
    }
    return EpgServerInfo(
        cpu: mach.cpu,
        gpu: mach.gpu,
        loadAverage: mach.loadAverage,
        memoryTotal: mach.memoryTotal,
        memoryFree: mach.memoryFree,
        hddTotal: mach.hddTotal,
        hddFree: mach.hddFree,
        bandwidthIn: mach.bandwidthIn,
        bandwidthOut: mach.bandwidthOut,
        uptime: mach.uptime,
        timestamp: mach.timestamp,
        totalBytesIn: mach.totalBytesIn,
        totalBytesOut: mach.totalBytesOut,
        syncTime: mach.syncTime,
        version: mach.version,
        project: mach.project,
        status: mach.status,
        expirationTime: mach.expirationTime,
        os: mach.os,
        onlineUsers: onlineUsers);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[_ONLINE_USERS_FIELDS] = onlineUsers?.toJson();
    return data;
  }
}

class Epg extends EpgServerInfo {
  static const ID_FIELD = 'id';
  static const NAME_FIELD = 'name';
  static const HOST_FIELD = 'host';
  static const PROVIDERS_FIELD = 'providers';
  static const AUTO_START_FIELD = 'auto_start';
  static const ACTIVATION_KEY_FIELD = 'activation_key';
  static const MONITORING_FIELD = 'monitoring';
  static const AUTO_UPDATE_FIELD = 'auto_update';
  static const AUTO_UPDATE_PERIOD_FIELD = 'auto_update_period';

  static const String DEFAULT_SERVER_NAME = 'Epg';
  static const MIN_NAME_LENGTH = ServiceName.MIN_LENGTH;
  static const MAX_NAME_LENGTH = ServiceName.MAX_LENGTH;

  final String? id;
  String name;
  HostAndPort host;
  bool monitoring;
  bool autoStart;
  bool autoUpdate;
  List<ServerProvider> providers;

  String? activationKey;
  int? autoUpdatePeriod;

  Epg(
      {required this.id,
      required this.name,
      required this.host,
      required this.providers,
      required this.monitoring,
      required this.autoStart,
      required this.autoUpdate,
      this.activationKey,
      this.autoUpdatePeriod,
      required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      required int totalBytesIn,
      required int totalBytesOut,
      int? syncTime,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      EpgOnlineUsers? onlineUsers})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            syncTime: syncTime,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os,
            onlineUsers: onlineUsers);

  Epg.createDefault()
      : id = null,
        name = DEFAULT_SERVER_NAME,
        host = HostAndPort.createLocalHostV4(port: 4317),
        monitoring = false,
        autoStart = false,
        autoUpdate = false,
        providers = [],
        activationKey = null,
        super.createInit();

  @override
  Epg copy() {
    return Epg(
        id: id,
        name: name,
        host: host.copy(),
        providers: providers,
        autoStart: autoStart,
        autoUpdate: autoUpdate,
        monitoring: monitoring,
        activationKey: activationKey,
        autoUpdatePeriod: autoUpdatePeriod,
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        timestamp: timestamp,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        syncTime: syncTime,
        version: version,
        project: project,
        status: status,
        expirationTime: expirationTime,
        os: os,
        onlineUsers: onlineUsers);
  }

  @override
  bool isActive() {
    return status == ServerStatus.ACTIVE;
  }

  bool isValid() {
    bool req = name.isValidServiceName() && host.isValid();
    if (req && activationKey != null) {
      req &= activationKey!.isValidActivationKey();
    }
    if (req && autoUpdatePeriod != null) {
      req &= autoUpdatePeriod!.isValidEpgUpdatePeriod();
    }
    return req;
  }

  factory Epg.fromJson(Map<String, dynamic> json) {
    final id = json[ID_FIELD];
    final _json = json[PROVIDERS_FIELD];
    final List<ServerProvider> _providers = [];
    _json.forEach((element) => _providers.add(ServerProvider.fromJson(element)));

    final String? activationKey = json[ACTIVATION_KEY_FIELD];
    final int? autoUpdatePeriod = json[AUTO_UPDATE_PERIOD_FIELD];

    final EpgServerInfo epg = EpgServerInfo.fromJson(json);
    return Epg(
        id: id,
        name: json[NAME_FIELD],
        host: HostAndPort.fromJson(json[HOST_FIELD]),
        providers: _providers,
        monitoring: json[MONITORING_FIELD],
        activationKey: activationKey,
        autoUpdatePeriod: autoUpdatePeriod,
        autoStart: json[AUTO_START_FIELD],
        autoUpdate: json[AUTO_UPDATE_FIELD],
        cpu: epg.cpu,
        gpu: epg.gpu,
        loadAverage: epg.loadAverage,
        memoryTotal: epg.memoryTotal,
        memoryFree: epg.memoryFree,
        hddTotal: epg.hddTotal,
        hddFree: epg.hddFree,
        bandwidthIn: epg.bandwidthIn,
        bandwidthOut: epg.bandwidthOut,
        version: epg.version,
        project: epg.project,
        uptime: epg.uptime,
        totalBytesIn: epg.totalBytesIn,
        totalBytesOut: epg.totalBytesOut,
        syncTime: epg.syncTime,
        timestamp: epg.timestamp,
        status: epg.status,
        onlineUsers: epg.onlineUsers,
        os: epg.os,
        expirationTime: epg.expirationTime);
  }

  @override
  Map<String, dynamic> toJson() {
    final _providersJson = [];
    for (final element in providers) {
      _providersJson.add(element.toJson());
    }
    final Map<String, dynamic> result = super.toJson();
    result[ID_FIELD] = id;
    result[NAME_FIELD] = name;
    result[HOST_FIELD] = host.toJson();
    result[PROVIDERS_FIELD] = providers;
    result[MONITORING_FIELD] = monitoring;
    result[AUTO_START_FIELD] = autoStart;
    result[AUTO_UPDATE_FIELD] = autoUpdate;
    if (activationKey != null) {
      result[ACTIVATION_KEY_FIELD] = activationKey;
    }
    if (autoUpdatePeriod != null) {
      result[AUTO_UPDATE_PERIOD_FIELD] = autoUpdatePeriod;
    }
    return result;
  }

  ProviderRole getProviderRoleById(String pid) {
    for (final ServerProvider provider in providers) {
      if (provider.id == pid) {
        return provider.role;
      }
    }
    return ProviderRole.READ;
  }
}

import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class YouTubeRtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'YouTube';
  static const BASE_URL = 'rtmp://a.rtmp.youtube.com/live2/';
  static const String WEB_URL = 'https://studio.youtube.com';

  YouTubeRtmpOut(
      {required int id,
      String uri = BASE_URL,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.YOUTUBE);

  // must be
  @override
  YouTubeRtmpOut copy() {
    return YouTubeRtmpOut(id: id, uri: uri, name: name, webUrl: webUrl, rtmpSinkType: rtmpSinkType);
  }

  factory YouTubeRtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return YouTubeRtmpOut(id: id, uri: uri, webUrl: web, rtmpSinkType: rtmp);
  }

  @override
  String baseUrl() {
    return BASE_URL;
  }
}

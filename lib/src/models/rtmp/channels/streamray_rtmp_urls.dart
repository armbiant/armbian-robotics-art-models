import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class StreamRayRtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'Streamray';
  static const String WEB_URL = 'https://models.streamray.com';

  StreamRayRtmpOut(
      {required int id,
      required String uri,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.STREAMRAY);

  // must be
  @override
  StreamRayRtmpOut copy() {
    return StreamRayRtmpOut(
        id: id, uri: uri, name: name, webUrl: webUrl, rtmpSinkType: rtmpSinkType);
  }

  factory StreamRayRtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return StreamRayRtmpOut(id: id, uri: uri, webUrl: web, rtmpSinkType: rtmp);
  }
}

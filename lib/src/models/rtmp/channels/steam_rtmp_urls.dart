import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class SteamRtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'Steam';
  static const String WEB_URL = 'https://store.steampowered.com/login';

  SteamRtmpOut(
      {required int id,
      required String uri,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.STEAM);

  // must be
  @override
  SteamRtmpOut copy() {
    return SteamRtmpOut(id: id, uri: uri, name: name, webUrl: webUrl, rtmpSinkType: rtmpSinkType);
  }

  factory SteamRtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return SteamRtmpOut(id: id, uri: uri, webUrl: web, rtmpSinkType: rtmp);
  }
}

import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class GoodGameRtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'GoodGame';
  static const BASE_URL = 'rtmp://msk.goodgame.ru:1940/live/';
  static const String WEB_URL = 'https://goodgame.ru';

  GoodGameRtmpOut(
      {required int id,
      String uri = BASE_URL,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.GOODGAME);

  // must be
  @override
  GoodGameRtmpOut copy() {
    return GoodGameRtmpOut(
        id: id, uri: uri, name: name, webUrl: webUrl, rtmpSinkType: rtmpSinkType);
  }

  factory GoodGameRtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return GoodGameRtmpOut(id: id, uri: uri, webUrl: web, rtmpSinkType: rtmp);
  }

  @override
  String baseUrl() {
    return BASE_URL;
  }
}

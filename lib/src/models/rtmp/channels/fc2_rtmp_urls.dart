import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class FC2RtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'FC2 Live';
  static const String WEB_URL = 'https://live.fc2.com';

  FC2RtmpOut(
      {required int id,
      required String uri,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.FC2);

  // must be
  @override
  FC2RtmpOut copy() {
    return FC2RtmpOut(id: id, uri: uri, name: name, webUrl: webUrl, rtmpSinkType: rtmpSinkType);
  }

  factory FC2RtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return FC2RtmpOut(id: id, uri: uri, webUrl: web, rtmpSinkType: rtmp);
  }
}

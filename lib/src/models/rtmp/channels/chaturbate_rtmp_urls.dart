import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class ChaturbateRtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'Breakers TV';
  static const String WEB_URL = 'https://chaturbate.com/auth/login';
  static const BASE_URL = 'rtmp://live.stream.highwebmedia.com/live-origin/';

  ChaturbateRtmpOut(
      {required int id,
      String uri = BASE_URL,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.CHATURBATE);

  // must be
  @override
  ChaturbateRtmpOut copy() {
    return ChaturbateRtmpOut(
        id: id, uri: uri, name: name, webUrl: webUrl, rtmpSinkType: rtmpSinkType);
  }

  factory ChaturbateRtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return ChaturbateRtmpOut(id: id, uri: uri, webUrl: web, rtmpSinkType: rtmp);
  }

  @override
  String baseUrl() {
    return BASE_URL;
  }
}

import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';

class AfreecaTVRtmpOut extends IRtmpOutputUrl {
  static const String NAME = 'Afreeca TV';
  static const String WEB_URL = 'http://dashboard.afreecatv.com';

  AfreecaTVRtmpOut(
      {required int id,
      required String uri,
      RtmpSinkType? rtmpSinkType,
      String name = NAME,
      String webUrl = WEB_URL})
      : super(
            id: id,
            uri: uri,
            rtmpSinkType: rtmpSinkType,
            name: name,
            webUrl: webUrl,
            type: PubSubStreamType.AFREECATV);

  // must be
  @override
  AfreecaTVRtmpOut copy() {
    return AfreecaTVRtmpOut(
        id: id, uri: uri, rtmpSinkType: rtmpSinkType, name: name, webUrl: webUrl);
  }

  factory AfreecaTVRtmpOut.fromJson(Map<String, dynamic> json) {
    final int id = json[OutputUrl.ID_FIELD];
    final String uri = json[OutputUrl.URI_FIELD];
    final String web = json[IRtmpOutputUrl.WEB_URL_FIELD];
    RtmpSinkType? rtmp;
    if (json.containsKey(RtmpOutputUrl.RTMPSINK_TYPE_FIELD)) {
      rtmp = RtmpSinkType.fromInt(json[RtmpOutputUrl.RTMPSINK_TYPE_FIELD]);
    }
    return AfreecaTVRtmpOut(id: id, uri: uri, rtmpSinkType: rtmp, webUrl: web);
  }
}

import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/all_channels.dart';

class PubSubStreamType {
  static const int _CUSTOM_RTMP_VALUE = 0;
  static const int _AFREECATV_RTMP_VALUE = 1;
  static const int _BILIBILI_RTMP_VALUE = 2;
  static const int _BONGACAMS_RTMP_VALUE = 3;
  static const int _CAM4_RTMP_VALUE = 4;
  static const int _CAMPLACE_RTMP_VALUE = 5;
  static const int _CAMSODA_RTMP_VALUE = 6;
  static const int _BREAKERSTV_RTMP_VALUE = 7;
  static const int _CHATURBATE_RTMP_VALUE = 8;
  static const int _DLIVE_RTMP_VALUE = 9;
  static const int _DOUYU_RTMP_VALUE = 10;
  static const int _FACEBOOK_RTMP_VALUE = 11;
  static const int _FC2_RTMP_VALUE = 12;
  static const int _FLIRT4FREE_RTMP_VALUE = 13;
  static const int _GOODGAME_RTMP_VALUE = 14;
  static const int _HUYA_RTMP_VALUE = 15;
  static const int _KAKAOTV_RTMP_VALUE = 16;
  static const int _MIXER_RTMP_VALUE = 17;
  static const int _MYFREECAMS_RTMP_VALUE = 18;
  static const int _NAVERTV_RTMP_VALUE = 19;
  static const int _NIMOTV_RTMP_VALUE = 20;
  static const int _ODNOKLASSNIKI_RTMP_VALUE = 21;
  static const int _PERISCOPE_RTMP_VALUE = 22;
  static const int _PICARTO_RTMP_VALUE = 23;
  static const int _SMASHCAST_RTMP_VALUE = 24;
  static const int _STREAMRAY_RTMP_VALUE = 25;
  static const int _STRIPCHAT_RTMP_VALUE = 26;
  static const int _TWITCH_RTMP_VALUE = 27;
  static const int _VAUGHNLIVE_RTMP_VALUE = 28;
  static const int _VIMEO_RTMP_VALUE = 29;
  static const int _VIRTWISH_RTMP_VALUE = 30;
  static const int _STEAM_RTMP_VALUE = 31;
  static const int _STEAMMATE_RTMP_VALUE = 32;
  static const int _VKONTAKTE_RTMP_VALUE = 33;
  static const int _VLIVE_RTMP_VALUE = 34;
  static const int _XLOVECAM_RTMP_VALUE = 35;
  static const int _YOUTUBE_RTMP_VALUE = 36;
  static const int _ZHANQITV_RTMP_VALUE = 37;

  final int _value;

  const PubSubStreamType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _CUSTOM_RTMP_VALUE) {
      return CustomRtmpOut.NAME;
    } else if (_value == _AFREECATV_RTMP_VALUE) {
      return AfreecaTVRtmpOut.NAME;
    } else if (_value == _BILIBILI_RTMP_VALUE) {
      return BiliBiliRtmpOut.NAME;
    } else if (_value == _BONGACAMS_RTMP_VALUE) {
      return BongaCamsRtmpOut.NAME;
    } else if (_value == _CAM4_RTMP_VALUE) {
      return Cam4RtmpOut.NAME;
    } else if (_value == _CAMPLACE_RTMP_VALUE) {
      return CamPlaceRtmpOut.NAME;
    } else if (_value == _CAMSODA_RTMP_VALUE) {
      return CamSodaRtmpOut.NAME;
    } else if (_value == _BREAKERSTV_RTMP_VALUE) {
      return BreakersTVRtmpOut.NAME;
    } else if (_value == _CHATURBATE_RTMP_VALUE) {
      return ChaturbateRtmpOut.NAME;
    } else if (_value == _DLIVE_RTMP_VALUE) {
      return DLiveRtmpOut.NAME;
    } else if (_value == _DOUYU_RTMP_VALUE) {
      return DouyuRtmpOut.NAME;
    } else if (_value == _FACEBOOK_RTMP_VALUE) {
      return FacebookRtmpOut.NAME;
    } else if (_value == _FC2_RTMP_VALUE) {
      return FC2RtmpOut.NAME;
    } else if (_value == _FLIRT4FREE_RTMP_VALUE) {
      return Flirt4FreeRtmpOut.NAME;
    } else if (_value == _GOODGAME_RTMP_VALUE) {
      return GoodGameRtmpOut.NAME;
    } else if (_value == _HUYA_RTMP_VALUE) {
      return HuyaRtmpOut.NAME;
    } else if (_value == _KAKAOTV_RTMP_VALUE) {
      return KakaoTVRtmpOut.NAME;
    } else if (_value == _MIXER_RTMP_VALUE) {
      return MixerRtmpOut.NAME;
    } else if (_value == _MYFREECAMS_RTMP_VALUE) {
      return MyFreeCamsRtmpOut.NAME;
    } else if (_value == _NAVERTV_RTMP_VALUE) {
      return NaverTVRtmpOut.NAME;
    } else if (_value == _NIMOTV_RTMP_VALUE) {
      return NimoTVRtmpOut.NAME;
    } else if (_value == _ODNOKLASSNIKI_RTMP_VALUE) {
      return OdnoklassnikiRtmpOut.NAME;
    } else if (_value == _PERISCOPE_RTMP_VALUE) {
      return PeriscopeRtmpOut.NAME;
    } else if (_value == _PICARTO_RTMP_VALUE) {
      return PicartoRtmpOut.NAME;
    } else if (_value == _SMASHCAST_RTMP_VALUE) {
      return SmashCastRtmpOut.NAME;
    } else if (_value == _STREAMRAY_RTMP_VALUE) {
      return StreamRayRtmpOut.NAME;
    } else if (_value == _STRIPCHAT_RTMP_VALUE) {
      return StripchatRtmpOut.NAME;
    } else if (_value == _TWITCH_RTMP_VALUE) {
      return TwitchRtmpOut.NAME;
    } else if (_value == _VAUGHNLIVE_RTMP_VALUE) {
      return VaughnLiveRtmpOut.NAME;
    } else if (_value == _VIMEO_RTMP_VALUE) {
      return VimeoRtmpOut.NAME;
    } else if (_value == _VIRTWISH_RTMP_VALUE) {
      return VirtWishRtmpOut.NAME;
    } else if (_value == _STEAM_RTMP_VALUE) {
      return SteamRtmpOut.NAME;
    } else if (_value == _STEAMMATE_RTMP_VALUE) {
      return SteamMateRtmpOut.NAME;
    } else if (_value == _VKONTAKTE_RTMP_VALUE) {
      return VkontakteRtmpOut.NAME;
    } else if (_value == _VLIVE_RTMP_VALUE) {
      return VLiveRtmpOut.NAME;
    } else if (_value == _XLOVECAM_RTMP_VALUE) {
      return XLoveCamRtmpOut.NAME;
    } else if (_value == _YOUTUBE_RTMP_VALUE) {
      return YouTubeRtmpOut.NAME;
    }

    assert(_value == _ZHANQITV_RTMP_VALUE);
    return ZhanqiTVRtmpOut.NAME;
  }

  factory PubSubStreamType.fromInt(int value) {
    if (value == _CUSTOM_RTMP_VALUE) {
      return CUSTOM;
    } else if (value == _AFREECATV_RTMP_VALUE) {
      return AFREECATV;
    } else if (value == _BILIBILI_RTMP_VALUE) {
      return BILIBILI;
    } else if (value == _BONGACAMS_RTMP_VALUE) {
      return BONGACAMS;
    } else if (value == _CAM4_RTMP_VALUE) {
      return CAM4;
    } else if (value == _CAMPLACE_RTMP_VALUE) {
      return CAMPLACE;
    } else if (value == _CAMSODA_RTMP_VALUE) {
      return CAMSODA;
    } else if (value == _BREAKERSTV_RTMP_VALUE) {
      return BREAKERSTV;
    } else if (value == _CHATURBATE_RTMP_VALUE) {
      return CHATURBATE;
    } else if (value == _DLIVE_RTMP_VALUE) {
      return DLIVE;
    } else if (value == _DOUYU_RTMP_VALUE) {
      return DOUYU;
    } else if (value == _FACEBOOK_RTMP_VALUE) {
      return FACEBOOK;
    } else if (value == _FC2_RTMP_VALUE) {
      return FC2;
    } else if (value == _FLIRT4FREE_RTMP_VALUE) {
      return FLIRT4FREE;
    } else if (value == _GOODGAME_RTMP_VALUE) {
      return GOODGAME;
    } else if (value == _HUYA_RTMP_VALUE) {
      return HUYA;
    } else if (value == _KAKAOTV_RTMP_VALUE) {
      return KAKAOTV;
    } else if (value == _MIXER_RTMP_VALUE) {
      return MIXER;
    } else if (value == _MYFREECAMS_RTMP_VALUE) {
      return MYFREECAMS;
    } else if (value == _NAVERTV_RTMP_VALUE) {
      return NAVERTV;
    } else if (value == _NIMOTV_RTMP_VALUE) {
      return NIMOTV;
    } else if (value == _ODNOKLASSNIKI_RTMP_VALUE) {
      return ODNOKLASSNIKI;
    } else if (value == _PERISCOPE_RTMP_VALUE) {
      return PERISCOPE;
    } else if (value == _PICARTO_RTMP_VALUE) {
      return PICARTO;
    } else if (value == _SMASHCAST_RTMP_VALUE) {
      return SMASHCAST;
    } else if (value == _STREAMRAY_RTMP_VALUE) {
      return STREAMRAY;
    } else if (value == _STRIPCHAT_RTMP_VALUE) {
      return STRIPCHAT;
    } else if (value == _TWITCH_RTMP_VALUE) {
      return TWITCH;
    } else if (value == _VAUGHNLIVE_RTMP_VALUE) {
      return VAUGHNLIVE;
    } else if (value == _VIMEO_RTMP_VALUE) {
      return VIMEO;
    } else if (value == _VIRTWISH_RTMP_VALUE) {
      return VIRTWISH;
    } else if (value == _STEAM_RTMP_VALUE) {
      return STEAM;
    } else if (value == _STEAMMATE_RTMP_VALUE) {
      return STEAMMATE;
    } else if (value == _VKONTAKTE_RTMP_VALUE) {
      return VKONTAKTE;
    } else if (value == _VLIVE_RTMP_VALUE) {
      return VLIVE;
    } else if (value == _XLOVECAM_RTMP_VALUE) {
      return XLOVECAM;
    } else if (value == _YOUTUBE_RTMP_VALUE) {
      return YOUTUBE;
    } else if (value == _ZHANQITV_RTMP_VALUE) {
      return ZHANQITV;
    }

    throw 'Unknown pubsub type: $value';
  }

  static List<PubSubStreamType> get values => [
        CUSTOM,
        AFREECATV,
        BILIBILI,
        BONGACAMS,
        CAM4,
        CAMPLACE,
        CAMSODA,
        BREAKERSTV,
        CHATURBATE,
        DLIVE,
        DOUYU,
        FACEBOOK,
        FC2,
        FLIRT4FREE,
        GOODGAME,
        HUYA,
        KAKAOTV,
        MIXER,
        MYFREECAMS,
        NAVERTV,
        NIMOTV,
        ODNOKLASSNIKI,
        PERISCOPE,
        PICARTO,
        SMASHCAST,
        STREAMRAY,
        STRIPCHAT,
        TWITCH,
        VAUGHNLIVE,
        VIMEO,
        VIRTWISH,
        STEAM,
        STEAMMATE,
        VKONTAKTE,
        VLIVE,
        XLOVECAM,
        YOUTUBE,
        ZHANQITV
      ];

  static const PubSubStreamType CUSTOM = PubSubStreamType._(_CUSTOM_RTMP_VALUE);
  static const PubSubStreamType AFREECATV = PubSubStreamType._(_AFREECATV_RTMP_VALUE);
  static const PubSubStreamType BILIBILI = PubSubStreamType._(_BILIBILI_RTMP_VALUE);
  static const PubSubStreamType BONGACAMS = PubSubStreamType._(_BONGACAMS_RTMP_VALUE);
  static const PubSubStreamType CAM4 = PubSubStreamType._(_CAM4_RTMP_VALUE);
  static const PubSubStreamType CAMPLACE = PubSubStreamType._(_CAMPLACE_RTMP_VALUE);
  static const PubSubStreamType CAMSODA = PubSubStreamType._(_CAMSODA_RTMP_VALUE);
  static const PubSubStreamType BREAKERSTV = PubSubStreamType._(_BREAKERSTV_RTMP_VALUE);
  static const PubSubStreamType CHATURBATE = PubSubStreamType._(_CHATURBATE_RTMP_VALUE);
  static const PubSubStreamType DLIVE = PubSubStreamType._(_DLIVE_RTMP_VALUE);
  static const PubSubStreamType DOUYU = PubSubStreamType._(_DOUYU_RTMP_VALUE);
  static const PubSubStreamType FACEBOOK = PubSubStreamType._(_FACEBOOK_RTMP_VALUE);
  static const PubSubStreamType FC2 = PubSubStreamType._(_FC2_RTMP_VALUE);
  static const PubSubStreamType FLIRT4FREE = PubSubStreamType._(_FLIRT4FREE_RTMP_VALUE);
  static const PubSubStreamType GOODGAME = PubSubStreamType._(_GOODGAME_RTMP_VALUE);
  static const PubSubStreamType HUYA = PubSubStreamType._(_HUYA_RTMP_VALUE);
  static const PubSubStreamType KAKAOTV = PubSubStreamType._(_KAKAOTV_RTMP_VALUE);
  static const PubSubStreamType MIXER = PubSubStreamType._(_MIXER_RTMP_VALUE);
  static const PubSubStreamType MYFREECAMS = PubSubStreamType._(_MYFREECAMS_RTMP_VALUE);
  static const PubSubStreamType NAVERTV = PubSubStreamType._(_NAVERTV_RTMP_VALUE);
  static const PubSubStreamType NIMOTV = PubSubStreamType._(_NIMOTV_RTMP_VALUE);
  static const PubSubStreamType ODNOKLASSNIKI = PubSubStreamType._(_ODNOKLASSNIKI_RTMP_VALUE);
  static const PubSubStreamType PERISCOPE = PubSubStreamType._(_PERISCOPE_RTMP_VALUE);
  static const PubSubStreamType PICARTO = PubSubStreamType._(_PICARTO_RTMP_VALUE);
  static const PubSubStreamType SMASHCAST = PubSubStreamType._(_SMASHCAST_RTMP_VALUE);
  static const PubSubStreamType STREAMRAY = PubSubStreamType._(_STREAMRAY_RTMP_VALUE);
  static const PubSubStreamType STRIPCHAT = PubSubStreamType._(_STRIPCHAT_RTMP_VALUE);
  static const PubSubStreamType TWITCH = PubSubStreamType._(_TWITCH_RTMP_VALUE);
  static const PubSubStreamType VAUGHNLIVE = PubSubStreamType._(_VAUGHNLIVE_RTMP_VALUE);
  static const PubSubStreamType VIMEO = PubSubStreamType._(_VIMEO_RTMP_VALUE);
  static const PubSubStreamType VIRTWISH = PubSubStreamType._(_VIRTWISH_RTMP_VALUE);
  static const PubSubStreamType STEAM = PubSubStreamType._(_STEAM_RTMP_VALUE);
  static const PubSubStreamType STEAMMATE = PubSubStreamType._(_STEAMMATE_RTMP_VALUE);
  static const PubSubStreamType VKONTAKTE = PubSubStreamType._(_VKONTAKTE_RTMP_VALUE);
  static const PubSubStreamType VLIVE = PubSubStreamType._(_VLIVE_RTMP_VALUE);
  static const PubSubStreamType XLOVECAM = PubSubStreamType._(_XLOVECAM_RTMP_VALUE);
  static const PubSubStreamType YOUTUBE = PubSubStreamType._(_YOUTUBE_RTMP_VALUE);
  static const PubSubStreamType ZHANQITV = PubSubStreamType._(_ZHANQITV_RTMP_VALUE);
}

class IRtmpOutputUrl extends RtmpOutputUrl {
  static const TYPE_FIELD = 'rtmp_type';
  static const String WEB_URL_FIELD = 'rtmp_web_url';

  final PubSubStreamType type;

  String name;

  String webUrl;

  IRtmpOutputUrl({
    required int id,
    required String uri,
    RtmpSinkType? rtmpSinkType,
    this.type = PubSubStreamType.CUSTOM,
    required this.webUrl,
    required this.name,
  }) : super(id: id, uri: uri, rtmpSinkType: rtmpSinkType);

  @override
  IRtmpOutputUrl copy() {
    return IRtmpOutputUrl(
        id: id, uri: uri, name: name, webUrl: webUrl, type: type, rtmpSinkType: rtmpSinkType);
  }

  factory IRtmpOutputUrl.fromJson(PubSubStreamType type, Map<String, dynamic> json) {
    if (type == PubSubStreamType.CUSTOM) {
      return CustomRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.TWITCH) {
      return TwitchRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.AFREECATV) {
      return AfreecaTVRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.BILIBILI) {
      return BiliBiliRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.BONGACAMS) {
      return BongaCamsRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.CAM4) {
      return Cam4RtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.CAMPLACE) {
      return CamPlaceRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.CAMSODA) {
      return CamSodaRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.BREAKERSTV) {
      return BreakersTVRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.DLIVE) {
      return DLiveRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.DOUYU) {
      return DouyuRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.FACEBOOK) {
      return FacebookRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.FC2) {
      return FC2RtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.FLIRT4FREE) {
      return Flirt4FreeRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.GOODGAME) {
      return GoodGameRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.HUYA) {
      return HuyaRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.KAKAOTV) {
      return KakaoTVRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.MIXER) {
      return MixerRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.MYFREECAMS) {
      return MyFreeCamsRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.NAVERTV) {
      return NaverTVRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.NIMOTV) {
      return NimoTVRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.ODNOKLASSNIKI) {
      return OdnoklassnikiRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.PERISCOPE) {
      return PeriscopeRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.PICARTO) {
      return PicartoRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.SMASHCAST) {
      return SmashCastRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.STREAMRAY) {
      return StreamRayRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.STRIPCHAT) {
      return StripchatRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.VAUGHNLIVE) {
      return VaughnLiveRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.VIMEO) {
      return VimeoRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.VIRTWISH) {
      return VirtWishRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.STEAM) {
      return SteamRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.STEAMMATE) {
      return SteamMateRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.VKONTAKTE) {
      return VkontakteRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.VLIVE) {
      return VLiveRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.XLOVECAM) {
      return XLoveCamRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.YOUTUBE) {
      return YouTubeRtmpOut.fromJson(json);
    } else if (type == PubSubStreamType.ZHANQITV) {
      return ZhanqiTVRtmpOut.fromJson(json);
    }

    throw 'Unknown rtmp output type: $type';
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[IRtmpOutputUrl.WEB_URL_FIELD] = webUrl;
    result[IRtmpOutputUrl.TYPE_FIELD] = type.toInt();
    return result;
  }

  String? key() {
    if (uri.isEmpty) {
      return null;
    }

    final parsed = Uri.tryParse(uri);
    if (parsed == null) {
      return null;
    }

    if (parsed.pathSegments.isEmpty) {
      return null;
    }

    final String result = parsed.pathSegments.last;
    if (parsed.hasQuery) {
      return '$result?${parsed.query}';
    }
    return result + parsed.query;
  }

  String? root() {
    if (uri.isEmpty) {
      return null;
    }

    final parsed = Uri.tryParse(uri);
    if (parsed == null) {
      return null;
    }

    final List<String> copy = [];
    for (int i = 0; i < parsed.pathSegments.length; ++i) {
      if (i < parsed.pathSegments.length - 1) {
        copy.add(parsed.pathSegments[i]);
      }
    }

    final stabled = Uri(
        scheme: parsed.scheme,
        userInfo: parsed.userInfo,
        host: parsed.host,
        port: parsed.port,
        pathSegments: copy);
    String result = stabled.toString();
    if (!result.endsWith('/')) {
      result += '/';
    }
    return result;
  }

  String? baseUrl() {
    return null;
  }

  String? generateBaseUrl(String key) {
    return generateUrl(baseUrl(), key);
  }

  static String? generateUrl(String? base, String? key) {
    if (key == null || key.isEmpty) {
      return null;
    }

    if (base == null || base.isEmpty) {
      return null;
    }

    if (!base.endsWith('/')) {
      base += '/';
    }

    return base + key;
  }
}

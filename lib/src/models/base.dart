import 'package:fastocloud_dart_models/src/models/alpha_method.dart';
import 'package:fastocloud_dart_models/src/models/server_provider.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastocloud_dart_models/src/utils/check_field.dart';
import 'package:fastocloud_dart_models/src/utils/validation.dart';

// enums
class MlBackend {
  static const int _NVIDIA_CONSTANT = 0;

  final int _value;

  const MlBackend._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    return 'NVIDIA';
  }

  //ignore: avoid_unused_constructor_parameters
  factory MlBackend.fromInt(int value) {
    return MlBackend.NVIDIA;
  }

  static List<MlBackend> get values => [NVIDIA];

  static const MlBackend NVIDIA = MlBackend._(_NVIDIA_CONSTANT);
}

class StreamType {
  static const int _PROXY_VALUE = 0;
  static const int _VOD_SERIAL_PROXY_VALUE = 1;
  static const int _RESTREAM_VALUE = 2;
  static const int _ENCODE_VALUE = 3;
  static const int _TIMESHIFT_PLAYER_VALUE = 4;
  static const int _TIMESHIFT_RECORDER_VALUE = 5;
  static const int _CATCHUP_VALUE = 6;
  static const int _TEST_LIFE_VALUE = 7;
  static const int _VOD_SERIAL_RESTREAM_VALUE = 8;
  static const int _VOD_SERIAL_ENCODE_VALUE = 9;
  static const int _COD_RESTREAM_VALUE = 10;
  static const int _COD_ENCODE_VALUE = 11;
  static const int _EVENT_VALUE = 12;
  static const int _CV_DATA_VALUE = 13;
  static const int _CHANGER_RESTREAM_VALUE = 14;
  static const int _CHANGER_ENCODE_VALUE = 15;

  final int _value;

  const StreamType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _PROXY_VALUE) {
      return 'External';
    } else if (_value == _VOD_SERIAL_PROXY_VALUE) {
      return 'VOD/Serial External';
    } else if (_value == _RESTREAM_VALUE) {
      return 'Restream';
    } else if (_value == _ENCODE_VALUE) {
      return 'Encode';
    } else if (_value == _TIMESHIFT_PLAYER_VALUE) {
      return 'Timeshift Player';
    } else if (_value == _TIMESHIFT_RECORDER_VALUE) {
      return 'Timeshift Recorder';
    } else if (_value == _CATCHUP_VALUE) {
      return 'Catchup';
    } else if (_value == _TEST_LIFE_VALUE) {
      return 'Test Life';
    } else if (_value == _VOD_SERIAL_RESTREAM_VALUE) {
      return 'VOD/Serial Restream';
    } else if (_value == _VOD_SERIAL_ENCODE_VALUE) {
      return 'VOD/Serial Encode';
    } else if (_value == _COD_RESTREAM_VALUE) {
      return 'COD(Chanel on Demand) Restream';
    } else if (_value == _COD_ENCODE_VALUE) {
      return 'COD(Chanel on Demand) Encode';
    } else if (_value == _EVENT_VALUE) {
      return 'Event';
    } else if (_value == _CV_DATA_VALUE) {
      return 'CV Stream';
    } else if (_value == _CHANGER_RESTREAM_VALUE) {
      return 'Changer Restream';
    }

    assert(_value == _CHANGER_ENCODE_VALUE);
    return 'Changer Encode';
  }

  String toHumanReadableWS() {
    if (_value == _PROXY_VALUE) {
      return 'External';
    } else if (_value == _VOD_SERIAL_PROXY_VALUE) {
      return 'VOD External';
    } else if (_value == _RESTREAM_VALUE) {
      return 'Restream';
    } else if (_value == _ENCODE_VALUE) {
      return 'Encode';
    } else if (_value == _TIMESHIFT_PLAYER_VALUE) {
      return 'Timeshift Player';
    } else if (_value == _TIMESHIFT_RECORDER_VALUE) {
      return 'Timeshift Recorder';
    } else if (_value == _CATCHUP_VALUE) {
      return 'Catchup';
    } else if (_value == _TEST_LIFE_VALUE) {
      return 'Test Life';
    } else if (_value == _VOD_SERIAL_RESTREAM_VALUE) {
      return 'VOD Restream';
    } else if (_value == _VOD_SERIAL_ENCODE_VALUE) {
      return 'VOD Encode';
    } else if (_value == _COD_RESTREAM_VALUE) {
      return 'COD(Chanel on Demand) Restream';
    } else if (_value == _COD_ENCODE_VALUE) {
      return 'COD(Chanel on Demand) Encode';
    } else if (_value == _EVENT_VALUE) {
      return 'Event';
    } else if (_value == _CV_DATA_VALUE) {
      return 'CV Stream';
    } else if (_value == _CHANGER_RESTREAM_VALUE) {
      return 'Changer Restream';
    }

    assert(_value == _CHANGER_ENCODE_VALUE);
    return 'Changer Encode';
  }

  factory StreamType.fromInt(int type) {
    if (type == _PROXY_VALUE) {
      return StreamType.PROXY;
    } else if (type == _VOD_SERIAL_PROXY_VALUE) {
      return StreamType.VOD_PROXY;
    } else if (type == _RESTREAM_VALUE) {
      return StreamType.RELAY;
    } else if (type == _ENCODE_VALUE) {
      return StreamType.ENCODE;
    } else if (type == _TIMESHIFT_PLAYER_VALUE) {
      return StreamType.TIMESHIFT_PLAYER;
    } else if (type == _TIMESHIFT_RECORDER_VALUE) {
      return StreamType.TIMESHIFT_RECORDER;
    } else if (type == _CATCHUP_VALUE) {
      return StreamType.CATCHUP;
    } else if (type == _TEST_LIFE_VALUE) {
      return StreamType.TEST_LIFE;
    } else if (type == _VOD_SERIAL_RESTREAM_VALUE) {
      return StreamType.VOD_RELAY;
    } else if (type == _VOD_SERIAL_ENCODE_VALUE) {
      return StreamType.VOD_ENCODE;
    } else if (type == _COD_RESTREAM_VALUE) {
      return StreamType.COD_RELAY;
    } else if (type == _COD_ENCODE_VALUE) {
      return StreamType.COD_ENCODE;
    } else if (type == _EVENT_VALUE) {
      return StreamType.EVENT;
    } else if (type == _CV_DATA_VALUE) {
      return StreamType.CV_DATA;
    } else if (type == _CHANGER_RESTREAM_VALUE) {
      return StreamType.CHANGER_RELAY;
    } else if (type == _CHANGER_ENCODE_VALUE) {
      return StreamType.CHANGER_ENCODE;
    }

    throw 'Unknown stream type: $type';
  }

  static List<StreamType> get values => [
        PROXY,
        VOD_PROXY,
        RELAY,
        ENCODE,
        TIMESHIFT_PLAYER,
        TIMESHIFT_RECORDER,
        CATCHUP,
        TEST_LIFE,
        VOD_RELAY,
        VOD_ENCODE,
        COD_RELAY,
        COD_ENCODE,
        EVENT,
        CV_DATA,
        CHANGER_RELAY,
        CHANGER_ENCODE
      ];

  static const StreamType PROXY = StreamType._(_PROXY_VALUE);
  static const StreamType VOD_PROXY = StreamType._(_VOD_SERIAL_PROXY_VALUE);
  static const StreamType RELAY = StreamType._(_RESTREAM_VALUE);
  static const StreamType ENCODE = StreamType._(_ENCODE_VALUE);
  static const StreamType TIMESHIFT_PLAYER = StreamType._(_TIMESHIFT_PLAYER_VALUE);
  static const StreamType TIMESHIFT_RECORDER = StreamType._(_TIMESHIFT_RECORDER_VALUE);
  static const StreamType CATCHUP = StreamType._(_CATCHUP_VALUE);
  static const StreamType TEST_LIFE = StreamType._(_TEST_LIFE_VALUE);
  static const StreamType VOD_RELAY = StreamType._(_VOD_SERIAL_RESTREAM_VALUE);
  static const StreamType VOD_ENCODE = StreamType._(_VOD_SERIAL_ENCODE_VALUE);
  static const StreamType COD_RELAY = StreamType._(_COD_RESTREAM_VALUE);
  static const StreamType COD_ENCODE = StreamType._(_COD_ENCODE_VALUE);
  static const StreamType EVENT = StreamType._(_EVENT_VALUE);
  static const StreamType CV_DATA = StreamType._(_CV_DATA_VALUE);
  static const StreamType CHANGER_RELAY = StreamType._(_CHANGER_RESTREAM_VALUE);
  static const StreamType CHANGER_ENCODE = StreamType._(_CHANGER_ENCODE_VALUE);
}

bool isVodStreamType(StreamType type) {
  return type == StreamType.VOD_PROXY ||
      type == StreamType.VOD_RELAY ||
      type == StreamType.VOD_ENCODE ||
      type == StreamType.EVENT;
}

class StreamStatus {
  static const int _STREAM_STATUS_NEW = 0;
  static const int _STREAM_STATUS_INIT = 1;
  static const int _STREAM_STATUS_STARTED = 2;
  static const int _STREAM_STATUS_READY = 3;
  static const int _STREAM_STATUS_PLAYING = 4;
  static const int _STREAM_STATUS_FROZEN = 5;
  static const int _STREAM_STATUS_WAITING = 6;

  final int _value;

  const StreamStatus._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _STREAM_STATUS_NEW) {
      return 'New';
    } else if (_value == _STREAM_STATUS_INIT) {
      return 'Init';
    } else if (_value == _STREAM_STATUS_STARTED) {
      return 'Started';
    } else if (_value == _STREAM_STATUS_READY) {
      return 'Ready';
    } else if (_value == _STREAM_STATUS_PLAYING) {
      return 'Streaming';
    } else if (_value == _STREAM_STATUS_FROZEN) {
      return 'Idle';
    }

    assert(_value == _STREAM_STATUS_WAITING);
    return 'Waiting';
  }

  factory StreamStatus.fromInt(int status) {
    if (status == _STREAM_STATUS_NEW) {
      return StreamStatus.NEW;
    } else if (status == _STREAM_STATUS_INIT) {
      return StreamStatus.INIT;
    } else if (status == _STREAM_STATUS_STARTED) {
      return StreamStatus.STARTED;
    } else if (status == _STREAM_STATUS_READY) {
      return StreamStatus.READY;
    } else if (status == _STREAM_STATUS_PLAYING) {
      return StreamStatus.PLAYING;
    } else if (status == _STREAM_STATUS_FROZEN) {
      return StreamStatus.FROZEN;
    } else if (status == _STREAM_STATUS_WAITING) {
      return StreamStatus.WAITING;
    }

    throw 'Unknown stream status: $status';
  }

  static List<StreamStatus> get values => [NEW, INIT, STARTED, READY, PLAYING, FROZEN, WAITING];

  static const StreamStatus NEW = StreamStatus._(_STREAM_STATUS_NEW);
  static const StreamStatus INIT = StreamStatus._(_STREAM_STATUS_INIT);
  static const StreamStatus STARTED = StreamStatus._(_STREAM_STATUS_STARTED);
  static const StreamStatus READY = StreamStatus._(_STREAM_STATUS_READY);
  static const StreamStatus PLAYING = StreamStatus._(_STREAM_STATUS_PLAYING);
  static const StreamStatus FROZEN = StreamStatus._(_STREAM_STATUS_FROZEN);
  static const StreamStatus WAITING = StreamStatus._(_STREAM_STATUS_WAITING);
}

class VodType {
  static const _VOD_TYPE_VOD = 0;
  static const _VOD_TYPE_SERIAL = 1;

  final int _value;

  const VodType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _VOD_TYPE_VOD) {
      return 'VODs';
    }

    assert(_value == _VOD_TYPE_SERIAL);
    return 'Serial';
  }

  factory VodType.fromInt(int type) {
    if (type == _VOD_TYPE_VOD) {
      return VodType.VOD;
    } else if (type == _VOD_TYPE_SERIAL) {
      return VodType.SERIAL;
    }

    throw 'Unknown VOD type: $type';
  }

  static List<VodType> get values => [VOD, SERIAL];

  static const VodType VOD = VodType._(_VOD_TYPE_VOD);
  static const VodType SERIAL = VodType._(_VOD_TYPE_SERIAL);
}

class StreamLogLevel {
  static const int _STREAM_LOG_LEVEL_EMRG = 0;
  static const int _STREAM_LOG_LEVEL_ALERT = 1;
  static const int _STREAM_LOG_LEVEL_CRIT = 2;
  static const int _STREAM_LOG_LEVEL_ERROR = 3;
  static const int _STREAM_LOG_LEVEL_WARNING = 4;
  static const int _STREAM_LOG_LEVEL_NOTICE = 5;
  static const int _STREAM_LOG_LEVEL_INFO = 6;
  static const int _STREAM_LOG_LEVEL_DEBUG = 7;

  final int _value;

  const StreamLogLevel._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _STREAM_LOG_LEVEL_EMRG) {
      return 'Emerg';
    } else if (_value == _STREAM_LOG_LEVEL_ALERT) {
      return 'Alert';
    } else if (_value == _STREAM_LOG_LEVEL_CRIT) {
      return 'Critical';
    } else if (_value == _STREAM_LOG_LEVEL_ERROR) {
      return 'Error';
    } else if (_value == _STREAM_LOG_LEVEL_WARNING) {
      return 'Warning';
    } else if (_value == _STREAM_LOG_LEVEL_NOTICE) {
      return 'Notice';
    } else if (_value == _STREAM_LOG_LEVEL_INFO) {
      return 'Info';
    }

    assert(_value == _STREAM_LOG_LEVEL_DEBUG);
    return 'Debug';
  }

  factory StreamLogLevel.fromInt(int level) {
    if (level == _STREAM_LOG_LEVEL_EMRG) {
      return StreamLogLevel.EMERG;
    } else if (level == _STREAM_LOG_LEVEL_ALERT) {
      return StreamLogLevel.ALERT;
    } else if (level == _STREAM_LOG_LEVEL_CRIT) {
      return StreamLogLevel.CRIT;
    } else if (level == _STREAM_LOG_LEVEL_ERROR) {
      return StreamLogLevel.ERR;
    } else if (level == _STREAM_LOG_LEVEL_WARNING) {
      return StreamLogLevel.WARNING;
    } else if (level == _STREAM_LOG_LEVEL_NOTICE) {
      return StreamLogLevel.NOTICE;
    } else if (level == _STREAM_LOG_LEVEL_INFO) {
      return StreamLogLevel.INFO;
    } else if (level == _STREAM_LOG_LEVEL_DEBUG) {
      return StreamLogLevel.DEBUG;
    }

    throw 'Unknown stream log level: $level';
  }

  static const StreamLogLevel EMERG = StreamLogLevel._(_STREAM_LOG_LEVEL_EMRG);
  static const StreamLogLevel ALERT = StreamLogLevel._(_STREAM_LOG_LEVEL_ALERT);
  static const StreamLogLevel CRIT = StreamLogLevel._(_STREAM_LOG_LEVEL_CRIT);
  static const StreamLogLevel ERR = StreamLogLevel._(_STREAM_LOG_LEVEL_ERROR);
  static const StreamLogLevel WARNING = StreamLogLevel._(_STREAM_LOG_LEVEL_WARNING);
  static const StreamLogLevel NOTICE = StreamLogLevel._(_STREAM_LOG_LEVEL_NOTICE);
  static const StreamLogLevel INFO = StreamLogLevel._(_STREAM_LOG_LEVEL_INFO);
  static const StreamLogLevel DEBUG = StreamLogLevel._(_STREAM_LOG_LEVEL_DEBUG);
}

class VideoCodec {
  static const _EAVC_CODEC_GSTREAMER = 'eavcenc';
  static const _OPENH264_CODEC_GSTREAMER = 'openh264enc';
  static const _X264_CODEC_GSTREAMER = 'x264enc';
  static const _X265_CODEC_GSTREAMER = 'x265enc';
  static const _NVIDIA_H264_CODEC_GSTREAMER = 'nvh264enc';
  static const _NVIDIA_H265_CODEC_GSTREAMER = 'nvh265enc';
  static const _NVIDIA_4VL2_H264_CODEC_GSTREAMER = 'nvv4l2h264enc';
  static const _NVIDIA_4VL2_H265_CODEC_GSTREAMER = 'nvv4l2h265enc';
  static const _NVIDIA_4VL2_VP8_CODEC_GSTREAMER = 'nvv4l2vp8enc';
  static const _NVIDIA_4VL2_VP9_CODEC_GSTREAMER = 'nvv4l2vp9enc';
  static const _VAAPI_H264_CODEC_GSTREAMER = 'vaapih264enc';
  static const _VAAPI_H265_CODEC_GSTREAMER = 'vaapih265enc';
  static const _VAAPI_MPEG2_CODEC_GSTREAMER = 'vaapimpeg2enc';
  static const _VAAPI_VP8_CODEC_GSTREAMER = 'vaapivp8enc';
  static const _VAAPI_VP9_CODEC_GSTREAMER = 'vaapivp9enc';
  static const _MFX_H264_CODEC_GSTREAMER = 'mfxh264enc';
  static const _VP8_CODEC_GSTREAMER = 'vp8enc';
  static const _VP9_CODEC_GSTREAMER = 'vp9enc';
  static const _MSDK_H264_CODEC_GSTREAMER = 'msdkh264enc';

  final String _value;

  const VideoCodec._(this._value);

  @override
  String toString() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _EAVC_CODEC_GSTREAMER) {
      return 'EAVC';
    } else if (_value == _OPENH264_CODEC_GSTREAMER) {
      return 'OPEN.H264';
    } else if (_value == _X264_CODEC_GSTREAMER) {
      return 'X264';
    } else if (_value == _NVIDIA_H264_CODEC_GSTREAMER) {
      return 'NVIDIA.H264';
    } else if (_value == _NVIDIA_H265_CODEC_GSTREAMER) {
      return 'NVIDIA.H265';
    } else if (_value == _NVIDIA_4VL2_H264_CODEC_GSTREAMER) {
      return 'NVIDIA.V4L.H264';
    } else if (_value == _NVIDIA_4VL2_H265_CODEC_GSTREAMER) {
      return 'NVIDIA.V4L.H265';
    } else if (_value == _NVIDIA_4VL2_VP8_CODEC_GSTREAMER) {
      return 'NVIDIA.V4L.VP8';
    } else if (_value == _NVIDIA_4VL2_VP9_CODEC_GSTREAMER) {
      return 'NVIDIA.V4L.VP9';
    } else if (_value == _VAAPI_H264_CODEC_GSTREAMER) {
      return 'VAAPI.H264';
    } else if (_value == _VAAPI_H265_CODEC_GSTREAMER) {
      return 'VAAPI.H265';
    } else if (_value == _VAAPI_MPEG2_CODEC_GSTREAMER) {
      return 'VAAPI.MPEG2';
    } else if (_value == _VAAPI_VP8_CODEC_GSTREAMER) {
      return 'VAAPI.VP8';
    } else if (_value == _VAAPI_VP9_CODEC_GSTREAMER) {
      return 'VAAPI.VP9';
    } else if (_value == _MFX_H264_CODEC_GSTREAMER) {
      return 'INTEL.H264';
    } else if (_value == _VP8_CODEC_GSTREAMER) {
      return 'VP8';
    } else if (_value == _VP9_CODEC_GSTREAMER) {
      return 'VP9';
    } else if (_value == _X265_CODEC_GSTREAMER) {
      return 'X265';
    }

    assert(_value == _MSDK_H264_CODEC_GSTREAMER);
    return 'MSDK.H264';
  }

  factory VideoCodec.fromString(String value) {
    if (value == _EAVC_CODEC_GSTREAMER) {
      return EAVC_ENC;
    } else if (value == _OPENH264_CODEC_GSTREAMER) {
      return OPEN_H264_ENC;
    } else if (value == _X264_CODEC_GSTREAMER) {
      return X264_ENC;
    } else if (value == _NVIDIA_H264_CODEC_GSTREAMER) {
      return NV_H264_ENC;
    } else if (value == _NVIDIA_H265_CODEC_GSTREAMER) {
      return NV_H265_ENC;
    } else if (value == _NVIDIA_4VL2_H264_CODEC_GSTREAMER) {
      return NV_V4L_H264_ENC;
    } else if (value == _NVIDIA_4VL2_H265_CODEC_GSTREAMER) {
      return NV_V4L_H265_ENC;
    } else if (value == _NVIDIA_4VL2_VP8_CODEC_GSTREAMER) {
      return NV_V4L_VP8_ENC;
    } else if (value == _NVIDIA_4VL2_VP9_CODEC_GSTREAMER) {
      return NV_V4L_VP9_ENC;
    } else if (value == _VAAPI_H264_CODEC_GSTREAMER) {
      return VAAPI_H264_ENC;
    } else if (value == _VAAPI_H265_CODEC_GSTREAMER) {
      return VAAPI_H265_ENC;
    } else if (value == _VAAPI_MPEG2_CODEC_GSTREAMER) {
      return VAAPI_MPEG2_ENC;
    } else if (value == _VAAPI_VP8_CODEC_GSTREAMER) {
      return VAAPI_VP8_ENC;
    } else if (value == _VAAPI_VP9_CODEC_GSTREAMER) {
      return VAAPI_VP9_ENC;
    } else if (value == _MFX_H264_CODEC_GSTREAMER) {
      return MFX_H264_ENC;
    } else if (value == _VP8_CODEC_GSTREAMER) {
      return VP8_ENC;
    } else if (value == _VP9_CODEC_GSTREAMER) {
      return VP9_ENC;
    } else if (value == _X265_CODEC_GSTREAMER) {
      return X265_ENC;
    } else if (value == _MSDK_H264_CODEC_GSTREAMER) {
      return MSDK_H264_ENC;
    }

    throw 'Unknown video codec value: $value';
  }

  static const VideoCodec DEFAULT = VideoCodec.X264_ENC;

  static List<VideoCodec> get values => [
        EAVC_ENC,
        OPEN_H264_ENC,
        X264_ENC,
        NV_H264_ENC,
        NV_H265_ENC,
        NV_V4L_H264_ENC,
        NV_V4L_H265_ENC,
        NV_V4L_VP8_ENC,
        NV_V4L_VP9_ENC,
        VAAPI_H264_ENC,
        VAAPI_H265_ENC,
        VAAPI_MPEG2_ENC,
        VAAPI_VP8_ENC,
        VAAPI_VP9_ENC,
        MFX_H264_ENC,
        VP8_ENC,
        VP9_ENC,
        X265_ENC,
        MSDK_H264_ENC
      ];

  static const VideoCodec EAVC_ENC = VideoCodec._(_EAVC_CODEC_GSTREAMER);
  static const VideoCodec OPEN_H264_ENC = VideoCodec._(_OPENH264_CODEC_GSTREAMER);
  static const VideoCodec X264_ENC = VideoCodec._(_X264_CODEC_GSTREAMER);
  static const VideoCodec NV_H264_ENC = VideoCodec._(_NVIDIA_H264_CODEC_GSTREAMER);
  static const VideoCodec NV_H265_ENC = VideoCodec._(_NVIDIA_H265_CODEC_GSTREAMER);
  static const VideoCodec NV_V4L_H264_ENC = VideoCodec._(_NVIDIA_4VL2_H264_CODEC_GSTREAMER);
  static const VideoCodec NV_V4L_H265_ENC = VideoCodec._(_NVIDIA_4VL2_H265_CODEC_GSTREAMER);
  static const VideoCodec NV_V4L_VP8_ENC = VideoCodec._(_NVIDIA_4VL2_VP8_CODEC_GSTREAMER);
  static const VideoCodec NV_V4L_VP9_ENC = VideoCodec._(_NVIDIA_4VL2_VP9_CODEC_GSTREAMER);
  static const VideoCodec VAAPI_H264_ENC = VideoCodec._(_VAAPI_H264_CODEC_GSTREAMER);
  static const VideoCodec VAAPI_H265_ENC = VideoCodec._(_VAAPI_H265_CODEC_GSTREAMER);
  static const VideoCodec VAAPI_MPEG2_ENC = VideoCodec._(_VAAPI_MPEG2_CODEC_GSTREAMER);
  static const VideoCodec VAAPI_VP8_ENC = VideoCodec._(_VAAPI_VP8_CODEC_GSTREAMER);
  static const VideoCodec VAAPI_VP9_ENC = VideoCodec._(_VAAPI_VP9_CODEC_GSTREAMER);
  static const VideoCodec MFX_H264_ENC = VideoCodec._(_MFX_H264_CODEC_GSTREAMER);
  static const VideoCodec VP8_ENC = VideoCodec._(_VP8_CODEC_GSTREAMER);
  static const VideoCodec VP9_ENC = VideoCodec._(_VP9_CODEC_GSTREAMER);
  static const VideoCodec X265_ENC = VideoCodec._(_X265_CODEC_GSTREAMER);
  static const VideoCodec MSDK_H264_ENC = VideoCodec._(_MSDK_H264_CODEC_GSTREAMER);
}

class AudioCodec {
  static const _MP3_CODEC_GSTREAMER = 'lamemp3enc';
  static const _AAC_CODEC_GSTREAMER = 'faac';
  static const _VOAAC_CODEC_GSTREAMER = 'voaacenc';
  static const _OPUS_CODEC_GSTREAMER = 'opusenc';

  final String _value;

  const AudioCodec._(this._value);

  @override
  String toString() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _MP3_CODEC_GSTREAMER) {
      return 'LAMEMP3';
    } else if (_value == _AAC_CODEC_GSTREAMER) {
      return 'AAC';
    } else if (_value == _VOAAC_CODEC_GSTREAMER) {
      return 'VOAAC';
    }

    assert(_value == _OPUS_CODEC_GSTREAMER);
    return 'OPUS';
  }

  factory AudioCodec.fromString(String value) {
    if (value == _MP3_CODEC_GSTREAMER) {
      return LAME_MP3_ENC;
    } else if (value == _AAC_CODEC_GSTREAMER) {
      return FAAC;
    } else if (value == _VOAAC_CODEC_GSTREAMER) {
      return VOAAC_ENC;
    } else if (value == _OPUS_CODEC_GSTREAMER) {
      return OPUS;
    }

    throw 'Unknown audio codec value: $value';
  }

  static const AudioCodec DEFAULT = AudioCodec.FAAC;

  static List<AudioCodec> get values => [LAME_MP3_ENC, FAAC, VOAAC_ENC, OPUS];

  static const AudioCodec LAME_MP3_ENC = AudioCodec._(_MP3_CODEC_GSTREAMER);
  static const AudioCodec FAAC = AudioCodec._(_AAC_CODEC_GSTREAMER);
  static const AudioCodec OPUS = AudioCodec._(_OPUS_CODEC_GSTREAMER);
  static const AudioCodec VOAAC_ENC = AudioCodec._(_VOAAC_CODEC_GSTREAMER);
}

class VideoParser {
  static const _MPEGTS_PARSER_GSTREAMER = 'tsparse';
  static const _H264_PARSER_GSTREAMER = 'h264parse';
  static const _H265_PARSER_GSTREAMER = 'h265parse';
  static const _VP8_PARSER_GSTREAMER = 'vp8parse';
  static const _VP9_PARSER_GSTREAMER = 'vp9parse';

  final String _value;

  const VideoParser._(this._value);

  @override
  String toString() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _MPEGTS_PARSER_GSTREAMER) {
      return 'MPEGTS';
    } else if (_value == _H264_PARSER_GSTREAMER) {
      return 'H264';
    } else if (_value == _H265_PARSER_GSTREAMER) {
      return 'H265';
    } else if (_value == _VP8_PARSER_GSTREAMER) {
      return 'VP8';
    }

    assert(_value == _VP9_PARSER_GSTREAMER);
    return 'VP9';
  }

  factory VideoParser.fromString(String value) {
    if (value == _MPEGTS_PARSER_GSTREAMER) {
      return TS;
    } else if (value == _H264_PARSER_GSTREAMER) {
      return H264;
    } else if (value == _H265_PARSER_GSTREAMER) {
      return H265;
    } else if (value == _VP8_PARSER_GSTREAMER) {
      return VP8;
    } else if (value == _VP9_PARSER_GSTREAMER) {
      return VP9;
    }

    throw 'Unknown video parser value: $value';
  }

  static List<VideoParser> get values => [TS, H264, H265, VP8, VP9];

  static const VideoParser TS = VideoParser._(_MPEGTS_PARSER_GSTREAMER);
  static const VideoParser H264 = VideoParser._(_H264_PARSER_GSTREAMER);
  static const VideoParser H265 = VideoParser._(_H265_PARSER_GSTREAMER);
  static const VideoParser VP8 = VideoParser._(_VP8_PARSER_GSTREAMER);
  static const VideoParser VP9 = VideoParser._(_VP9_PARSER_GSTREAMER);
}

class AudioParser {
  static const _AAC_PARSER_GSTREAMER = 'aacparse';
  static const _AC3_PARSER_GSTREAMER = 'ac3parse';
  static const _MPEG_PARSER_GSTREAMER = 'mpegaudioparse';
  static const _OPUS_PARSER_GSTREAMER = 'opusparse';
  static const _RAW_PARSER_GSTREAMER = 'rawaudioparse';

  final String _value;

  const AudioParser._(this._value);

  @override
  String toString() => _value;

  String toHumanReadable() {
    if (_value == _AAC_PARSER_GSTREAMER) {
      return 'AAC';
    } else if (_value == _AC3_PARSER_GSTREAMER) {
      return 'AC3';
    } else if (_value == _MPEG_PARSER_GSTREAMER) {
      return 'MPEG';
    } else if (_value == _OPUS_PARSER_GSTREAMER) {
      return 'OPUS';
    }

    assert(_value == _RAW_PARSER_GSTREAMER);
    return 'RAW';
  }

  factory AudioParser.fromString(String value) {
    if (value == _AAC_PARSER_GSTREAMER) {
      return AAC;
    } else if (value == _AC3_PARSER_GSTREAMER) {
      return AC3;
    } else if (value == _MPEG_PARSER_GSTREAMER) {
      return MPEG;
    } else if (value == _OPUS_PARSER_GSTREAMER) {
      return OPUS;
    } else if (value == _RAW_PARSER_GSTREAMER) {
      return RAW;
    }

    throw 'Unknown audio parser value: $value';
  }

  static List<AudioParser> get values => [AAC, AC3, MPEG, OPUS, RAW];

  static const AudioParser AAC = AudioParser._(_AAC_PARSER_GSTREAMER);
  static const AudioParser AC3 = AudioParser._(_AC3_PARSER_GSTREAMER);
  static const AudioParser MPEG = AudioParser._(_MPEG_PARSER_GSTREAMER);
  static const AudioParser OPUS = AudioParser._(_OPUS_PARSER_GSTREAMER);
  static const AudioParser RAW = AudioParser._(_RAW_PARSER_GSTREAMER);
}

// base models
class Size {
  static const WIDTH_FIELD = 'width';
  static const HEIGHT_FIELD = 'height';

  static const MIN_WIDTH = 1;
  static const MIN_HEIGHT = 1;

  int width;
  int height;

  Size({required this.width, required this.height});

  Size.create640x480()
      : width = 640,
        height = 480;

  Size.create240x120()
      : width = 240,
        height = 120;

  Size copy() {
    return Size(width: width, height: height);
  }

  factory Size.fromJson(Map<String, dynamic> json) {
    final width = json[WIDTH_FIELD];
    if (width == null) {
      throw ParseJsonException('Invalid input, $WIDTH_FIELD required');
    }

    final height = json[HEIGHT_FIELD];
    if (height == null) {
      throw ParseJsonException('Invalid input, $HEIGHT_FIELD required');
    }
    return Size(width: width, height: height);
  }

  Map<String, dynamic> toJson() {
    return {WIDTH_FIELD: width, HEIGHT_FIELD: height};
  }

  bool isValid() {
    return width >= MIN_WIDTH && height >= MIN_HEIGHT;
  }
}

class Rational {
  static const NUM_FIELD = 'num';
  static const DEN_FIELD = 'den';

  static const INVALID_NUM = 0;
  static const INVALID_DEN = 0;

  int numerator;
  int denominator;

  Rational({required this.numerator, required this.denominator});

  Rational.create4x3()
      : numerator = 4,
        denominator = 3;

  Rational.create25x1()
      : numerator = 25,
        denominator = 1;

  Rational copy() {
    return Rational(numerator: numerator, denominator: denominator);
  }

  factory Rational.fromJson(Map<String, dynamic> json) {
    final numerator = json[NUM_FIELD];
    final denominator = json[DEN_FIELD];
    return Rational(numerator: numerator, denominator: denominator);
  }

  Map<String, dynamic> toJson() {
    return {NUM_FIELD: numerator, DEN_FIELD: denominator};
  }

  bool isValid() {
    return numerator != INVALID_NUM && denominator != INVALID_DEN;
  }
}

class Point {
  static const X_FIELD = 'x';
  static const Y_FIELD = 'y';

  static const DEFAULT_X = 0;
  static const DEFAULT_Y = 0;

  int x;
  int y;

  Point({this.x = DEFAULT_X, this.y = DEFAULT_Y});

  Point.createStart()
      : x = 0,
        y = 0;

  Point copy() {
    return Point(x: x, y: y);
  }

  factory Point.fromJson(Map<String, dynamic> json) {
    final x = json[X_FIELD];
    final y = json[Y_FIELD];
    return Point(x: x, y: y);
  }

  Map<String, dynamic> toJson() {
    return {X_FIELD: x, Y_FIELD: y};
  }

  bool isValid() {
    return true;
  }
}

class Font {
  static const FAMILY_FIELD = 'family';
  static const SIZE_FIELD = 'size';

  String family;
  int size;

  Font({required this.family, required this.size});

  Font copy() {
    return Font(family: family, size: size);
  }

  factory Font.fromJson(Map<String, dynamic> json) {
    final family = json[FAMILY_FIELD];
    final size = json[SIZE_FIELD];
    return Font(family: family, size: size);
  }

  Map<String, dynamic> toJson() {
    return {FAMILY_FIELD: family, SIZE_FIELD: size};
  }
}

class OverlayUrlType {
  static const int _URL = 0;
  static const int _WPE = 1;
  static const int _CEF = 2;

  final int _value;

  const OverlayUrlType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _URL) {
      return 'URL';
    } else if (_value == _WPE) {
      return 'WPE';
    }
    assert(_value == _CEF);
    return 'CEF';
  }

  factory OverlayUrlType.fromInt(int type) {
    if (type == _URL) {
      return OverlayUrlType.URL;
    } else if (type == _WPE) {
      return OverlayUrlType.WPE;
    } else if (type == _CEF) {
      return OverlayUrlType.CEF;
    }

    throw 'Unknown background type: $type';
  }

  static List<OverlayUrlType> get values => [URL, WPE, CEF];

  static const OverlayUrlType URL = OverlayUrlType._(_URL);
  static const OverlayUrlType WPE = OverlayUrlType._(_WPE);
  static const OverlayUrlType CEF = OverlayUrlType._(_CEF);
}

abstract class IOverlayUrl {
  static const URL_FIELD = 'url';
  static const TYPE_FIELD = 'type';

  String url;
  OverlayUrlType type;

  IOverlayUrl copy();

  bool isValid() {
    final length = url.length;
    return length >= StreamUrl.MIN_LENGTH && length < StreamUrl.MAX_LENGTH;
  }

  IOverlayUrl({required this.url, required this.type});

  factory IOverlayUrl.fromJson(Map<String, dynamic> json) {
    final type = OverlayUrlType.fromInt(json[TYPE_FIELD]);

    if (type == OverlayUrlType.URL) {
      return OverlayUrlUrl.fromJson(json);
    } else if (type == OverlayUrlType.WPE) {
      return OverlayUrlWpe.fromJson(json);
    } else if (type == OverlayUrlType.CEF) {
      return OverlayUrlCef.fromJson(json);
    }

    throw 'Unknown overlay url type: $type';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {URL_FIELD: url, TYPE_FIELD: type.toInt()};
    return result;
  }
}

class OverlayUrlUrl extends IOverlayUrl {
  static const DEFAULT_URL_URL = 'https://fastocloud.com/hls/master.m3u8';

  OverlayUrlUrl({required String url}) : super(url: url, type: OverlayUrlType.URL);

  OverlayUrlUrl.createDefault() : super(url: DEFAULT_URL_URL, type: OverlayUrlType.URL);

  @override
  IOverlayUrl copy() {
    return OverlayUrlUrl(url: url);
  }

  factory OverlayUrlUrl.fromJson(Map<String, dynamic> json) {
    return OverlayUrlUrl(url: json[IOverlayUrl.URL_FIELD]);
  }
}

class OverlayUrlWpe extends IOverlayUrl {
  static const WPE_FIELD = 'wpe';
  static const DEFAULT_WPE_URL = 'https://fastocloud.com';

  Wpe wpe;

  OverlayUrlWpe({required String url, required this.wpe})
      : super(url: url, type: OverlayUrlType.WPE);

  OverlayUrlWpe.createDefault()
      : wpe = Wpe.createDefault(),
        super(url: DEFAULT_WPE_URL, type: OverlayUrlType.WPE);

  @override
  IOverlayUrl copy() {
    return OverlayUrlWpe(url: url, wpe: wpe);
  }

  factory OverlayUrlWpe.fromJson(Map<String, dynamic> json) {
    return OverlayUrlWpe(url: json[IOverlayUrl.URL_FIELD], wpe: Wpe.fromJson(json[WPE_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[WPE_FIELD] = wpe.toJson();
    return result;
  }
}

class OverlayUrlCef extends IOverlayUrl {
  static const CEF_FIELD = 'cef';
  static const DEFAULT_CEF_URL = 'https://fastocloud.com';

  Cef cef;

  OverlayUrlCef({required String url, required this.cef})
      : super(url: url, type: OverlayUrlType.CEF);

  OverlayUrlCef.createDefault()
      : cef = Cef.createDefault(),
        super(url: DEFAULT_CEF_URL, type: OverlayUrlType.CEF);

  @override
  IOverlayUrl copy() {
    return OverlayUrlCef(url: url, cef: cef);
  }

  factory OverlayUrlCef.fromJson(Map<String, dynamic> json) {
    return OverlayUrlCef(url: json[IOverlayUrl.URL_FIELD], cef: Cef.fromJson(json[CEF_FIELD]));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[CEF_FIELD] = cef.toJson();
    return result;
  }
}

class StreamOverlay {
  static const URL_FIELD = 'url';

  static const BACKGROUND_FIELD = 'background';
  static const METHOD_FIELD = 'method';
  static const SIZE_FIELD = 'size';

  IOverlayUrl url;
  int? background;
  AlphaMethod? method;
  Size? size;

  StreamOverlay({required this.url, this.background, this.method, this.size});

  StreamOverlay.createDefault() : url = OverlayUrlUrl.createDefault();

  StreamOverlay copy() {
    return StreamOverlay(url: url.copy(), background: background, method: method, size: size);
  }

  bool isValidStreamUrl() {
    return url.isValid();
  }

  factory StreamOverlay.fromJson(Map<String, dynamic> json) {
    final url = IOverlayUrl.fromJson(json[URL_FIELD]);
    int? background;
    if (json.containsKey(BACKGROUND_FIELD)) {
      background = json[BACKGROUND_FIELD];
    }
    AlphaMethod? method;
    if (json.containsKey(METHOD_FIELD)) {
      method = AlphaMethod.fromJson(json[METHOD_FIELD]);
    }
    Size? size;
    if (json.containsKey(SIZE_FIELD)) {
      size = Size.fromJson(json[SIZE_FIELD]);
    }
    return StreamOverlay(url: url, background: background, method: method, size: size);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {URL_FIELD: url.toJson()};
    if (background != null) {
      result[BACKGROUND_FIELD] = background;
    }
    if (method != null) {
      result[METHOD_FIELD] = method!.toJson();
    }
    if (size != null) {
      result[SIZE_FIELD] = size!.toJson();
    }
    return result;
  }
}

class TextOverlay {
  static const TEXT_FIELD = 'text';
  static const FONT_FIELD = 'font';
  static const X_ABSOLUTE_FIELD = 'x_absolute';
  static const Y_ABSOLUTE_FIELD = 'y_absolute';

  static const DEFAULT_X_ABSOLUTE = 0.5;
  static const DEFAULT_Y_ABSOLUTE = 0.5;

  String text;
  double xAbsolute;
  double yAbsolute;

  Font? font;

  TextOverlay({required this.text, required this.xAbsolute, required this.yAbsolute, this.font});

  TextOverlay.createDefault()
      : text = 'FastoCloud',
        xAbsolute = DEFAULT_X_ABSOLUTE,
        yAbsolute = DEFAULT_Y_ABSOLUTE;

  TextOverlay copy() {
    return TextOverlay(text: text, xAbsolute: xAbsolute, yAbsolute: yAbsolute, font: font);
  }

  factory TextOverlay.fromJson(Map<String, dynamic> json) {
    final text = json[TEXT_FIELD];
    final xAbs = json[X_ABSOLUTE_FIELD];
    final yAbs = json[Y_ABSOLUTE_FIELD];
    Font? font;
    if (json.containsKey(FONT_FIELD)) {
      font = Font.fromJson(json[FONT_FIELD]);
    }
    return TextOverlay(text: text, xAbsolute: xAbs, yAbsolute: yAbs, font: font);
  }

  Map<String, dynamic> toJson() {
    final result = {TEXT_FIELD: text, X_ABSOLUTE_FIELD: xAbsolute, Y_ABSOLUTE_FIELD: yAbsolute};
    if (font != null) {
      result[FONT_FIELD] = font!;
    }
    return result;
  }

  bool isValid() {
    return text.isNotEmpty &&
        (xAbsolute >= 0 && xAbsolute <= 1.0) &&
        (yAbsolute >= 0 && yAbsolute <= 1.0);
  }
}

class Logo {
  static const PATH_FIELD = 'path';
  static const POSITION_FIELD = 'position';
  static const ALPHA_FIELD = 'alpha';
  static const SIZE_FIELD = 'size';

  String path;
  Point position;
  double alpha;
  Size size;

  Logo({required this.path, required this.position, required this.alpha, required this.size});

  Logo.createDefault()
      : path = 'https://fastocloud.com/logo.png',
        position = Point.createStart(),
        size = Size.create240x120(),
        alpha = Alpha.DEFAULT;

  Logo copy() {
    return Logo(path: path, position: position.copy(), alpha: alpha, size: size.copy());
  }

  factory Logo.fromJson(Map<String, dynamic> json) {
    final path = json[PATH_FIELD];
    final position = Point.fromJson(json[POSITION_FIELD]);
    final alpha = json[ALPHA_FIELD];
    final size = Size.fromJson(json[SIZE_FIELD]);
    return Logo(path: path, position: position, alpha: alpha, size: size);
  }

  Map<String, dynamic> toJson() {
    return {
      PATH_FIELD: path,
      POSITION_FIELD: position.toJson(),
      ALPHA_FIELD: alpha,
      SIZE_FIELD: size.toJson()
    };
  }

  bool isValid() {
    return (path.isValidIconUrl() || path.isValidFilePath()) &&
        position.isValid() &&
        alpha.isValidAlpha() &&
        size.isValid();
  }
}

class RsvgLogo {
  static const PATH_FIELD = 'path';
  static const POSITION_FIELD = 'position';
  static const SIZE_FIELD = 'size';

  String path;
  Point position;
  Size size;

  RsvgLogo({required this.path, required this.position, required this.size});

  RsvgLogo.createDefault()
      : path = 'https://fastocloud.com/logo.svg',
        position = Point.createStart(),
        size = Size.create240x120();

  RsvgLogo copy() {
    return RsvgLogo(path: path, position: position.copy(), size: size.copy());
  }

  factory RsvgLogo.fromJson(Map<String, dynamic> json) {
    final path = json[PATH_FIELD];
    final position = Point.fromJson(json[POSITION_FIELD]);
    final size = Size.fromJson(json[SIZE_FIELD]);
    return RsvgLogo(path: path, position: position, size: size);
  }

  Map<String, dynamic> toJson() {
    return {PATH_FIELD: path, POSITION_FIELD: position.toJson(), SIZE_FIELD: size.toJson()};
  }

  bool isValid() {
    return (path.isValidIconUrl() || path.isValidFilePath()) &&
        position.isValid() &&
        size.isValid();
  }
}

class HostAndPort {
  static const HOST_FIELD = 'host';
  static const PORT_FIELD = 'port';

  static const String DEFAULT_LOCAL_HOST_IPV4 = '127.0.0.1';
  static const String DEFAULT_LOCAL_HOST_IPV6 = '::1';
  static const String DEFAULT_ROUTE_HOST_IPV4 = '0.0.0.0';
  static const String DEFAULT_ROUTE_HOST_IPV6 = '::';

  String host;
  int port;

  HostAndPort({required this.host, required this.port});

  HostAndPort.createLocalHostV4({required this.port}) : host = DEFAULT_LOCAL_HOST_IPV4;

  HostAndPort.createLocalHostV6({required this.port}) : host = DEFAULT_LOCAL_HOST_IPV4;

  HostAndPort.createDefaultRouteHostV4({required this.port}) : host = DEFAULT_ROUTE_HOST_IPV4;

  HostAndPort.createDefaultRouteHostV6({required this.port}) : host = DEFAULT_ROUTE_HOST_IPV6;

  HostAndPort copy() {
    return HostAndPort(host: host, port: port);
  }

  factory HostAndPort.fromString(String data) {
    final split = data.split(':');
    return HostAndPort(host: split[0], port: int.parse(split[1]));
  }

  factory HostAndPort.fromJson(Map<String, dynamic> json) {
    return HostAndPort(host: json[HOST_FIELD], port: json[PORT_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {HOST_FIELD: host, PORT_FIELD: port};
  }

  static bool isValidHost(String host) {
    return isValidIPV4(host) || isValidIPV6(host) || isValidDomain(host);
  }

  bool isValid() {
    return isValidHost(host);
  }

  @override
  String toString() {
    return '$host:$port';
  }
}

HostAndPort? makeHostAndPort(String data) {
  final List<String> parts = data.split(":");
  if (parts.length != 2) {
    return null;
  }

  final port = int.tryParse(parts[1]);
  if (port == null) {
    return null;
  }

  final host = parts[0];
  return HostAndPort(host: host, port: port);
}

class ServerProvider {
  static const ID_FIELD = 'id';
  static const ROLE_FIELD = 'role';
  static const EMAIL_FIELD = 'email';

  String? id;
  ProviderRole role;
  String? email;

  ServerProvider({required this.id, required this.email, required this.role});

  ServerProvider copy() {
    return ServerProvider(id: id, email: email, role: role);
  }

  factory ServerProvider.fromJson(Map<String, dynamic> json) {
    return ServerProvider(
        id: json[ID_FIELD], email: json[EMAIL_FIELD], role: ProviderRole.fromInt(json[ROLE_FIELD]));
  }

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, EMAIL_FIELD: email, ROLE_FIELD: role.toInt()};
  }
}

class ChannelStats {
  static const ID_FIELD = 'id';
  static const BPS_FIELD = 'bps';

  final int id;
  final int bps;

  ChannelStats({required this.id, required this.bps});

  ChannelStats copy() {
    return ChannelStats(id: id, bps: bps);
  }

  factory ChannelStats.fromJson(Map<String, dynamic> json) {
    return ChannelStats(id: json[ID_FIELD], bps: json[BPS_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, BPS_FIELD: bps};
  }
}

class MlModel {
  static const PATH_FIELD = 'path';

  String path;

  MlModel({required this.path});

  MlModel.createDefault()
      : path =
            '/opt/nvidia/deepstream/deepstream-6.1/sources/objectDetector_Yolo/config_infer_primary_yoloV3.txt';

  MlModel copy() {
    return MlModel(path: path);
  }

  factory MlModel.fromJson(Map<String, dynamic> json) {
    return MlModel(path: json[PATH_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {PATH_FIELD: path};
  }
}

class MachineLearning {
  static const BACKEND_FIELD = 'backend';
  static const MODELS_FIELD = 'models';
  static const OVERLAY_FIELD = 'overlay';
  static const TRACKING_FIELD = 'tracking';

  MlBackend backend;
  List<MlModel> models;
  bool overlay;
  bool tracking;

  MachineLearning(
      {required this.backend, required this.models, required this.tracking, required this.overlay});

  MachineLearning copy() {
    return MachineLearning(backend: backend, models: models, tracking: tracking, overlay: overlay);
  }

  MachineLearning.createDefault()
      : backend = MlBackend.NVIDIA,
        models = [MlModel.createDefault()],
        tracking = false,
        overlay = false;

  bool isValid() {
    return models.isNotEmpty;
  }

  factory MachineLearning.fromJson(Map<String, dynamic> json) {
    final MlBackend backend = MlBackend.fromInt(json[MachineLearning.BACKEND_FIELD]);
    final modelsJson = json[MachineLearning.MODELS_FIELD];
    final List<MlModel> models = [];
    modelsJson.forEach((element) {
      models.add(MlModel.fromJson(element));
    });
    final bool tracking = json[MachineLearning.TRACKING_FIELD];
    final bool overlay = json[MachineLearning.OVERLAY_FIELD];
    return MachineLearning(backend: backend, models: models, tracking: tracking, overlay: overlay);
  }

  Map<String, dynamic> toJson() {
    final List<Map<String, dynamic>> _models = [];
    for (final out in models) {
      _models.add(out.toJson());
    }

    return {
      MachineLearning.BACKEND_FIELD: backend.toInt(),
      MachineLearning.MODELS_FIELD: _models,
      MachineLearning.TRACKING_FIELD: tracking,
      MachineLearning.OVERLAY_FIELD: overlay
    };
  }
}

class PhoneNumber {
  static const DIAL_CODE_FIELD = 'dial_code';
  static const PHONE_NUMBER_FIELD = 'phone_number';
  static const ISO_CODE_FILED = 'iso_code';

  String dialCode;
  String phoneNumber;
  String isoCode;

  PhoneNumber({required this.dialCode, required this.phoneNumber, required this.isoCode});

  PhoneNumber.createUSADefault()
      : dialCode = '+1',
        phoneNumber = '123456789',
        isoCode = 'US';

  PhoneNumber copy() {
    return PhoneNumber(dialCode: dialCode, phoneNumber: phoneNumber, isoCode: isoCode);
  }

  factory PhoneNumber.fromJson(Map<String, dynamic> json) {
    final code = json[DIAL_CODE_FIELD];
    final number = json[PHONE_NUMBER_FIELD];
    final iso = json[ISO_CODE_FILED];
    return PhoneNumber(dialCode: code, phoneNumber: number, isoCode: iso);
  }

  Map<String, dynamic> toJson() {
    return {DIAL_CODE_FIELD: dialCode, PHONE_NUMBER_FIELD: phoneNumber, ISO_CODE_FILED: isoCode};
  }

  bool isValid() {
    return dialCode.isNotEmpty && phoneNumber.isNotEmpty && isoCode.isNotEmpty;
  }
}

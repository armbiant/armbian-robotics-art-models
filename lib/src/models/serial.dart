import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/serial_info.dart';

class ServerSerial extends Serial {
  // #FIXME: CreateDate should be optional like in streams (server fill it)
  static const _VISIBLE_FIELD = 'visible';

  bool visible = true;

  ServerSerial(
      {String? id,
      String name = StreamName.SERIAL,
      required String icon,
      String backgroundUrl = '',
      List<String>? groups,
      int iarc = IARC.DEFAULT,
      this.visible = true,
      String description = '',
      List<String>? seasons,
      int primeDate = 0,
      double price = Price.DEFAULT,
      int viewCount = 0,
      int createdDate = 0,
      double userScore = 0.0,
      String country = Country.DEFAULT,
      List<String> directors = const [],
      List<String> cast = const [],
      List<String> production = const [],
      List<String> genres = const [],
      String? pid})
      : super(
            id: id,
            name: name,
            background: backgroundUrl,
            icon: icon,
            groups: groups ?? [],
            iarc: iarc,
            description: description,
            seasons: seasons ?? [],
            viewCount: viewCount,
            primeDate: primeDate,
            price: price,
            createdDate: createdDate,
            userScore: userScore,
            country: country,
            directors: directors,
            cast: cast,
            production: production,
            genres: genres,
            pid: pid);

  @override
  ServerSerial copy() {
    final ServerSerial serial = ServerSerial(
        id: id,
        name: name,
        icon: icon,
        backgroundUrl: background,
        groups: groups,
        iarc: iarc,
        visible: visible,
        description: description,
        seasons: seasons,
        primeDate: primeDate,
        price: price,
        viewCount: viewCount,
        createdDate: createdDate,
        userScore: userScore,
        country: country,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres,
        pid: pid);
    return serial;
  }

  ServerSerial copyWith({required String? id, required String name}) {
    final other = copy();
    other.id = id;
    other.name = name;
    return other;
  }

  bool isValid() {
    return name.isNotEmpty && icon.isValidIconUrl();
  }

  factory ServerSerial.fromJson(Map<String, dynamic> json) {
    final Serial base = Serial.fromJson(json);
    final visible = json[_VISIBLE_FIELD];
    final ServerSerial serial = ServerSerial(
        id: base.id,
        name: base.name,
        icon: base.icon,
        backgroundUrl: base.background,
        groups: base.groups,
        iarc: base.iarc,
        visible: visible,
        description: base.description,
        seasons: base.seasons,
        primeDate: base.primeDate,
        price: base.price,
        viewCount: base.viewCount,
        createdDate: base.createdDate,
        pid: base.pid,
        userScore: base.userScore,
        country: base.country,
        genres: base.genres,
        production: base.production,
        cast: base.cast,
        directors: base.directors);
    return serial;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[_VISIBLE_FIELD] = visible;
    return result;
  }
}

class ProviderRole {
  static const int _READ_CONSTANT = 0;
  static const int _WRITE_CONSTANT = 1;
  static const int _SUPPORT_CONSTANT = 2;
  static const int _ADMIN_CONSTANT = 3;

  final int _value;

  const ProviderRole._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _READ_CONSTANT) {
      return 'Read';
    } else if (_value == _WRITE_CONSTANT) {
      return 'Write';
    } else if (_value == _SUPPORT_CONSTANT) {
      return 'Support';
    }

    assert(_value == _ADMIN_CONSTANT);
    return 'Admin';
  }

  factory ProviderRole.fromInt(int role) {
    if (role == _READ_CONSTANT) {
      return ProviderRole.READ;
    } else if (role == _WRITE_CONSTANT) {
      return ProviderRole.WRITE;
    } else if (role == _SUPPORT_CONSTANT) {
      return ProviderRole.SUPPORT;
    } else if (role == _ADMIN_CONSTANT) {
      return ProviderRole.ADMIN;
    }

    throw 'Unknown provider role: $role';
  }

  static List<ProviderRole> get values => [READ, WRITE, SUPPORT, ADMIN];

  static const ProviderRole READ = ProviderRole._(_READ_CONSTANT); // + start/stop streams
  // + play output, play
  // + playlist
  // + refresh epg
  static const ProviderRole WRITE = ProviderRole._(_WRITE_CONSTANT); // READ
  // + edit/copy/add/remove streams, epg
  static const ProviderRole SUPPORT = ProviderRole._(_SUPPORT_CONSTANT); // WRITE
  // + logs, pipeline
  // + connect/disconnect/activate
  static const ProviderRole ADMIN = ProviderRole._(_ADMIN_CONSTANT); // WRITE
// + add/remove providers
// + add/edit/remove server
// + connect/disconnect/activate
// + send online sub message
}

import 'package:fastotv_dart/commands_info.dart';
import 'package:fastotv_dart/commands_info/client_info.dart';

class DeviceStatus {
  static const int _NOT_ACTIVE_CONSTANT = 0;
  static const int _ACTIVE_CONSTANT = 1;
  static const int _BANNED_CONSTANT = 2;

  final int _value;

  const DeviceStatus._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NOT_ACTIVE_CONSTANT) {
      return 'NOT ACTIVE';
    } else if (_value == _ACTIVE_CONSTANT) {
      return 'ACTIVE';
    }

    assert(_value == _BANNED_CONSTANT);
    return 'BANNED';
  }

  factory DeviceStatus.fromInt(int type) {
    if (type == _NOT_ACTIVE_CONSTANT) {
      return DeviceStatus.NOT_ACTIVE;
    } else if (type == _ACTIVE_CONSTANT) {
      return DeviceStatus.ACTIVE;
    } else if (type == _BANNED_CONSTANT) {
      return DeviceStatus.BANNED;
    }

    throw 'Unknown device type: $type';
  }

  static List<DeviceStatus> get values => [NOT_ACTIVE, ACTIVE, BANNED];

  static const DeviceStatus NOT_ACTIVE = DeviceStatus._(_NOT_ACTIVE_CONSTANT);
  static const DeviceStatus ACTIVE = DeviceStatus._(_ACTIVE_CONSTANT);
  static const DeviceStatus BANNED = DeviceStatus._(_BANNED_CONSTANT);
}

class Device {
  static const _ID_FIELD = 'id';
  static const _NAME_FIELD = 'name';
  static const _STATUS_FIELD = 'status';
  static const _CREATED_DATE_FIELD = 'created_date';

  static const MIN_NAME_LENGTH = 1;
  static const MAX_NAME_LENGTH = 32;

  final String? id;
  final int? createdDate;

  String name;
  DeviceStatus status;

  Device({this.id, required this.name, required this.status, this.createdDate});

  Device.createDefault()
      : id = null,
        name = 'Test',
        status = DeviceStatus.NOT_ACTIVE,
        createdDate = null;

  Device copy() {
    return Device(id: id, name: name, status: status, createdDate: createdDate);
  }

  bool isValid() {
    return name.length >= MIN_NAME_LENGTH && name.length < MAX_NAME_LENGTH && createdDate != 0;
  }

  factory Device.fromJson(Map<String, dynamic> json) {
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    int? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    return Device(
        id: id,
        name: json[_NAME_FIELD],
        status: DeviceStatus.fromInt(json[_STATUS_FIELD]),
        createdDate: createdDate);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {_NAME_FIELD: name, _STATUS_FIELD: status.toInt()};
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    return result;
  }
}

class DeviceLoginInfo {
  static const _IP_FIELD = 'ip';
  static const _TIMESTAMP_FIELD = 'timestamp';
  static const _LOC_FIELD = 'loc';
  static const _CITY_FIELD = 'city';
  static const _COUNTRY_FIELD = 'country';

  final String ip;
  final int timestamp;
  final String location;
  final String city;
  final String country;

  DeviceLoginInfo(
      {required this.ip,
      required this.timestamp,
      required this.location,
      required this.city,
      required this.country});

  DeviceLoginInfo copy() {
    return DeviceLoginInfo(
        ip: ip, timestamp: timestamp, location: location, city: city, country: country);
  }

  factory DeviceLoginInfo.fromJson(Map<String, dynamic> json) {
    return DeviceLoginInfo(
        ip: json[_IP_FIELD],
        timestamp: json[_TIMESTAMP_FIELD],
        location: json[_LOC_FIELD],
        city: json[_CITY_FIELD],
        country: json[_COUNTRY_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {
      _IP_FIELD: ip,
      _TIMESTAMP_FIELD: timestamp,
      _LOC_FIELD: location,
      _CITY_FIELD: city,
      _COUNTRY_FIELD: country
    };
  }
}

class DeviceLoginFront extends Device {
  static const _OS_FIELD = 'os';
  static const _PROJECT_FIELD = 'project';
  static const _CPU_BRAND_FIELD = 'cpu_brand';
  static const _LOGINS_FIELD = 'logins';
  static const _ACTIVE_DURATION = 'active_duration';
  static const _LAST_ACTIVE = 'last_active';

  final OperationSystemInfo? deviceOS;
  final Project? deviceProject;
  final String? deviceBrand;
  final List<DeviceLoginInfo>? logins;
  final int? activeDuration; // millisec
  final int? lastActive; // millisec

  int get loginsCount {
    if (logins == null) {
      return 0;
    }

    return logins!.length;
  }

  DeviceLoginFront(
      {String? id,
      required String name,
      required DeviceStatus status,
      int? createdDate,
      this.deviceOS,
      this.deviceBrand,
      this.deviceProject,
      this.logins,
      this.activeDuration,
      this.lastActive})
      : super(id: id, name: name, status: status, createdDate: createdDate);

  @override
  DeviceLoginFront copy() {
    return DeviceLoginFront(
        id: id,
        name: name,
        status: status,
        createdDate: createdDate,
        deviceOS: deviceOS,
        deviceBrand: deviceBrand,
        deviceProject: deviceProject,
        logins: logins,
        activeDuration: activeDuration,
        lastActive: lastActive);
  }

  factory DeviceLoginFront.fromJson(Map<String, dynamic> json) {
    final device = Device.fromJson(json);
    OperationSystemInfo? deviceOS;
    Project? deviceProject;
    String? deviceBrand;
    List<DeviceLoginInfo>? logins;
    int? activeDuration;
    int? lastActive;
    if (json.containsKey(_OS_FIELD)) {
      deviceOS = OperationSystemInfo.fromJson(json[_OS_FIELD]);
    }
    if (json.containsKey(_PROJECT_FIELD)) {
      deviceProject = Project.fromJson(json[_PROJECT_FIELD]);
    }
    if (json.containsKey(_CPU_BRAND_FIELD)) {
      deviceBrand = json[_CPU_BRAND_FIELD];
    }
    if (json.containsKey(_LOGINS_FIELD)) {
      final List<DeviceLoginInfo> res = [];
      json[_LOGINS_FIELD].forEach((b) {
        res.add(DeviceLoginInfo.fromJson(b));
      });
      logins = res;
    }
    if (json.containsKey(_ACTIVE_DURATION)) {
      activeDuration = json[_ACTIVE_DURATION];
    }
    if (json.containsKey(_LAST_ACTIVE)) {
      lastActive = json[_LAST_ACTIVE];
    }
    return DeviceLoginFront(
        id: device.id,
        name: device.name,
        status: device.status,
        createdDate: device.createdDate,
        deviceOS: deviceOS,
        deviceProject: deviceProject,
        deviceBrand: deviceBrand,
        logins: logins,
        activeDuration: activeDuration,
        lastActive: lastActive);
  }
}

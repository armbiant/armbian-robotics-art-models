import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/platform.dart';
import 'package:fastotv_dart/profile.dart';

class SubscriberStatus {
  static const int _NOT_ACTIVE_CONSTANT = 0;
  static const int _ACTIVE_CONSTANT = 1;
  static const int _DELETED_CONSTANT = 2;

  final int _value;

  const SubscriberStatus._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NOT_ACTIVE_CONSTANT) {
      return 'NOT ACTIVE';
    } else if (_value == _ACTIVE_CONSTANT) {
      return 'ACTIVE';
    }

    assert(_value == _DELETED_CONSTANT);
    return 'DELETED';
  }

  factory SubscriberStatus.fromInt(int status) {
    if (status == _NOT_ACTIVE_CONSTANT) {
      return SubscriberStatus.NOT_ACTIVE;
    } else if (status == _ACTIVE_CONSTANT) {
      return SubscriberStatus.ACTIVE;
    } else if (status == _DELETED_CONSTANT) {
      return SubscriberStatus.DELETED;
    }

    throw 'Unknown subscriber status: $status';
  }

  static List<SubscriberStatus> get values => [NOT_ACTIVE, ACTIVE, DELETED];

  static const SubscriberStatus NOT_ACTIVE = SubscriberStatus._(_NOT_ACTIVE_CONSTANT);
  static const SubscriberStatus ACTIVE = SubscriberStatus._(_ACTIVE_CONSTANT);
  static const SubscriberStatus DELETED = SubscriberStatus._(_DELETED_CONSTANT);
}

class Subscriber extends SubProfile {
  static const _CREATED_DATE_FIELD = 'created_date';
  static const _STATUS_FIELD = 'status';
  static const _MAX_DEVICE_COUNT_FIELD = 'max_devices_count';
  static const _DEVICES_COUNT_FIELD = 'devices_count';
  static const _LANGUAGE_FIELD = 'language';
  static const _COUNTRY_FIELD = 'country';
  static const _SERVERS_FIELD = 'servers';

  final int? createdDate;
  SubscriberStatus status;
  int maxDevicesCount;
  final int devicesCount;
  String? language;
  String? country;
  List<String> servers;

  static const MAX_NAME_LENGTH = 64;

  Subscriber(
      {String? id,
      required String email,
      String? password,
      required String firstName,
      required String lastName,
      this.createdDate,
      required int expDate,
      required this.status,
      required this.maxDevicesCount,
      required this.language,
      required this.country,
      required this.servers,
      required this.devicesCount})
      : super(
            id: id,
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            expDate: expDate);

  Subscriber.create(
      {required String email,
      String? password,
      required String firstName,
      required String lastName,
      required int expDate,
      required this.status,
      required this.maxDevicesCount,
      required this.language,
      required this.country,
      required this.servers,
      required this.devicesCount})
      : createdDate = null,
        super.create(
            email: email,
            password: password,
            firstName: firstName,
            lastName: lastName,
            expDate: expDate);

  Subscriber.createDefault()
      : createdDate = null,
        status = SubscriberStatus.ACTIVE,
        maxDevicesCount = DevicesCount.DEFAULT,
        devicesCount = 0,
        servers = [],
        super.createDefault();

  @override
  Subscriber copy() {
    return Subscriber(
        id: id,
        email: email,
        firstName: firstName,
        password: password,
        lastName: lastName,
        createdDate: createdDate,
        expDate: expDate,
        status: status,
        language: language,
        country: country,
        maxDevicesCount: maxDevicesCount,
        devicesCount: devicesCount,
        servers: servers);
  }

  bool isValid() {
    if (language == null || country == null) {
      return false;
    }

    final bool fields = email.isNotEmpty &&
        isValidName(firstName) &&
        isValidName(lastName) &&
        expDate != 0 &&
        language!.isNotEmpty &&
        country!.isNotEmpty;

    if (id == null) {
      return fields && (password?.isNotEmpty ?? false);
    }
    return fields && createdDate != 0;
  }

  // password field not exists in json
  factory Subscriber.fromJson(Map<String, dynamic> json) {
    final profile = SubProfile.fromJson(json);
    final status = SubscriberStatus.fromInt(json[_STATUS_FIELD]);
    final maxDeviceCount = json[_MAX_DEVICE_COUNT_FIELD];
    final devicesCount = json[_DEVICES_COUNT_FIELD];
    final language = json[_LANGUAGE_FIELD];
    final country = json[_COUNTRY_FIELD];
    final List<String> servers = json[_SERVERS_FIELD].cast<String>();

    int? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    return Subscriber(
        id: profile.id,
        email: profile.email,
        password: profile.password,
        firstName: profile.firstName,
        lastName: profile.lastName,
        createdDate: createdDate,
        expDate: profile.expDate,
        status: status,
        maxDevicesCount: maxDeviceCount,
        language: language,
        country: country,
        servers: servers,
        devicesCount: devicesCount);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();

    result[_STATUS_FIELD] = status.toInt();
    result[_MAX_DEVICE_COUNT_FIELD] = maxDevicesCount;
    result[_LANGUAGE_FIELD] = language;
    result[_COUNTRY_FIELD] = country;
    result[_SERVERS_FIELD] = servers;
    result[_DEVICES_COUNT_FIELD] = devicesCount;

    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    return result;
  }

  static bool isValidName(String name) {
    return name.isNotEmpty && name.length < MAX_NAME_LENGTH;
  }
}

class OnlineSubscriber {
  static const _ID_FIELD = 'id';
  static const _EMAIL_FIELD = 'login'; //
  static const _EXP_DATE_FIELD = 'exp_date';
  static const _DEVICE_ID_FIELD = 'device_id';

  final String id; // not changed
  final String email;
  final int expDate;
  final String deviceId;

  OnlineSubscriber(
      {required this.id, required this.email, required this.expDate, required this.deviceId});

  OnlineSubscriber copy() {
    return OnlineSubscriber(id: id, email: email, expDate: expDate, deviceId: deviceId);
  }

  factory OnlineSubscriber.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final email = json[_EMAIL_FIELD];
    final exp = json[_EXP_DATE_FIELD];
    final did = json[_DEVICE_ID_FIELD];
    return OnlineSubscriber(id: id, email: email, expDate: exp, deviceId: did);
  }

  Map<String, dynamic> toJson() {
    return {
      _ID_FIELD: id,
      _EMAIL_FIELD: email,
      _EXP_DATE_FIELD: expDate,
      _DEVICE_ID_FIELD: deviceId
    };
  }
}

class OnlineSubscriberEx extends OnlineSubscriber {
  static const _ACTION_DATE_FIELD = 'action_date';
  static const _OS_FIELD = 'os';
  static const _DEVICE_NAME_FIELD = 'device_name';

  final int actDate;
  final PlatformType os;
  final String deviceName;

  OnlineSubscriberEx({
    required String id,
    required String email,
    required int expDate,
    required String deviceId,
    required this.actDate,
    required this.os,
    required this.deviceName,
  }) : super(id: id, email: email, expDate: expDate, deviceId: deviceId);

  @override
  OnlineSubscriberEx copy() {
    return OnlineSubscriberEx(
        id: id,
        email: email,
        expDate: expDate,
        deviceId: deviceId,
        actDate: actDate,
        os: os,
        deviceName: deviceName);
  }

  factory OnlineSubscriberEx.fromJson(Map<String, dynamic> json) {
    final base = OnlineSubscriber.fromJson(json);
    final act = json[_ACTION_DATE_FIELD];
    final os = json[_OS_FIELD];
    final deviceName = json[_DEVICE_NAME_FIELD];
    return OnlineSubscriberEx(
      id: base.id,
      email: base.email,
      expDate: base.expDate,
      deviceId: base.deviceId,
      actDate: act,
      os: PlatformType.fromString(os),
      deviceName: deviceName,
    );
  }

  @override
  Map<String, dynamic> toJson() {
    final result = super.toJson();
    result[_ACTION_DATE_FIELD] = actDate;
    result[_OS_FIELD] = os.toString();
    result[_DEVICE_NAME_FIELD] = deviceName;
    return result;
  }
}

class DictStream {
  static const _TVG_ID_FIELD = 'tvg-id';
  static const _TVG_NAME_FIELD = 'tvg-name';
  static const _TVG_LOGO_FIELD = 'tvg-logo';
  static const _GROUPS_FIELD = 'groups';
  static const _URL_FIELD = 'url';

  String epgId;
  String name;
  String icon;
  List<String> groups;
  String url;

  DictStream(
      {required this.epgId,
      required this.name,
      required this.icon,
      required this.groups,
      required this.url});

  DictStream copy() {
    return DictStream(epgId: epgId, name: name, icon: icon, groups: groups, url: url);
  }

  factory DictStream.fromJson(Map<String, dynamic> json) {
    final id = json[DictStream._TVG_ID_FIELD];
    final name = json[DictStream._TVG_NAME_FIELD];
    final icon = json[DictStream._TVG_LOGO_FIELD];
    final List<String> groups = json[DictStream._GROUPS_FIELD].cast<String>();
    final url = json[DictStream._URL_FIELD];
    return DictStream(epgId: id, name: name, icon: icon, groups: groups, url: url);
  }

  Map<String, dynamic> toJson() {
    return {
      _TVG_ID_FIELD: epgId,
      _TVG_NAME_FIELD: name,
      _TVG_LOGO_FIELD: icon,
      _GROUPS_FIELD: groups,
      _URL_FIELD: url
    };
  }
}

import 'package:fastocloud_dart_models/src/models/base.dart';

class AlgoType {
  final int _value;

  const AlgoType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == 48) {
      return 'HDD';
    }
    return 'MachineID';
  }

  factory AlgoType.fromInt(int? value) {
    if (value == 48) {
      return AlgoType.HDD;
    }
    return AlgoType.MACHINE_ID;
  }

  static List<AlgoType> get values => [HDD, MACHINE_ID];

  static const AlgoType HDD = AlgoType._(48);
  static const AlgoType MACHINE_ID = AlgoType._(49);
}

class NodeAlgoInfo {
  static const HOST_FIELD = 'host';
  static const ALGO_FIELD = 'algo';

  HostAndPort host;
  AlgoType algo;

  NodeAlgoInfo(this.host, this.algo);

  NodeAlgoInfo copy() {
    return NodeAlgoInfo(host, algo);
  }

  factory NodeAlgoInfo.fromJson(Map<String, dynamic> json) {
    final host = json[HOST_FIELD];
    final algo = AlgoType.fromInt(json[ALGO_FIELD]);
    return NodeAlgoInfo(host, algo);
  }

  Map<String, dynamic> toJson() {
    return {HOST_FIELD: host.toString(), ALGO_FIELD: algo.toInt()};
  }
}

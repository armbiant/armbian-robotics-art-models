import 'package:fastocloud_dart_models/src/models/base.dart';
import 'package:fastocloud_dart_models/src/models/input_urls.dart';
import 'package:fastocloud_dart_models/src/models/machine.dart';
import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/server_base.dart';
import 'package:fastocloud_dart_models/src/models/server_provider.dart';
import 'package:fastocloud_dart_models/src/models/stream.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/client_info.dart';

class ServerOnlineUsers {
  static const _DAEMON_FIELD = 'daemon';
  static const _HTTP_FIELD = 'http';
  static const _VODS_FIELD = 'vods';
  static const _CODS_FIELD = 'cods';

  final int daemon;
  final int http;
  final int vods;
  final int cods;

  ServerOnlineUsers(
      {required this.daemon, required this.http, required this.vods, required this.cods});

  ServerOnlineUsers copy() {
    return ServerOnlineUsers(daemon: daemon, http: http, vods: vods, cods: cods);
  }

  factory ServerOnlineUsers.fromJson(Map<String, dynamic> json) {
    final daemon = json[_DAEMON_FIELD];
    final http = json[_HTTP_FIELD];
    final vods = json[_VODS_FIELD];
    final cods = json[_CODS_FIELD];
    return ServerOnlineUsers(daemon: daemon, http: http, vods: vods, cods: cods);
  }

  Map<String, dynamic> toJson() {
    return {_DAEMON_FIELD: daemon, _HTTP_FIELD: http, _VODS_FIELD: vods, _CODS_FIELD: cods};
  }
}

class MediaServerInfo extends Machine {
  static const _ONLINE_USERS_FIELDS = 'online_users';

  ServerOnlineUsers? onlineUsers;

  MediaServerInfo(
      {required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      required int totalBytesIn,
      required int totalBytesOut,
      int? syncTime,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      this.onlineUsers})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            syncTime: syncTime,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os);

  @override
  MediaServerInfo copy() {
    return MediaServerInfo(
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        timestamp: timestamp,
        version: version,
        project: project,
        os: os,
        expirationTime: expirationTime,
        syncTime: syncTime,
        status: status,
        onlineUsers: onlineUsers);
  }

  factory MediaServerInfo.fromJson(Map<String, dynamic> json) {
    final Machine mach = Machine.fromJson(json);
    ServerOnlineUsers? onlineUsers;
    if (json.containsKey(_ONLINE_USERS_FIELDS)) {
      onlineUsers = ServerOnlineUsers.fromJson(json[_ONLINE_USERS_FIELDS]);
    }
    return MediaServerInfo(
        cpu: mach.cpu,
        gpu: mach.gpu,
        loadAverage: mach.loadAverage,
        memoryTotal: mach.memoryTotal,
        memoryFree: mach.memoryFree,
        hddTotal: mach.hddTotal,
        hddFree: mach.hddFree,
        bandwidthIn: mach.bandwidthIn,
        bandwidthOut: mach.bandwidthOut,
        uptime: mach.uptime,
        timestamp: mach.timestamp,
        totalBytesIn: mach.totalBytesIn,
        totalBytesOut: mach.totalBytesOut,
        syncTime: mach.syncTime,
        version: mach.version,
        project: mach.project,
        status: mach.status,
        expirationTime: mach.expirationTime,
        os: mach.os,
        onlineUsers: onlineUsers);
  }

  MediaServerInfo.createInit()
      : onlineUsers = null,
        super.createInit();

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[_ONLINE_USERS_FIELDS] = onlineUsers?.toJson();
    return data;
  }
}

class LiveServer extends MediaServerInfo {
  static const HLS_HOST_FIELD = 'hls_host';
  static const VODS_HOST_FIELD = 'vods_host';
  static const CODS_HOST_FIELD = 'cods_host';

  static const HLS_DIRECTORY_FIELD = 'hls_directory';
  static const VODS_DIRECTORY_FIELD = 'vods_directory';
  static const CODS_DIRECTORY_FIELD = 'cods_directory';
  static const PROXY_DIRECTORY_FIELD = 'proxy_directory';

  static const String DEFAULT_PROXY_DIR = '~/streamer/proxy';
  static const String DEFAULT_HLS_DIR = '~/streamer/hls';
  static const String DEFAULT_VODS_DIR = '~/streamer/vods';
  static const String DEFAULT_CODS_DIR = '~/streamer/cods';

  Uri hlsHost;
  Uri vodsHost;
  Uri codsHost;

  Uri get proxyHost {
    return hlsHost;
  }

  String hlsDirectory;
  String vodsDirectory;
  String codsDirectory;
  String proxyDirectory;

  LiveServer(
      {required this.hlsHost,
      required this.vodsHost,
      required this.codsHost,
      required this.hlsDirectory,
      required this.vodsDirectory,
      required this.codsDirectory,
      required this.proxyDirectory,
      required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      required int totalBytesIn,
      required int totalBytesOut,
      int? syncTime,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      ServerOnlineUsers? onlineUsers})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            syncTime: syncTime,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os,
            onlineUsers: onlineUsers);

  LiveServer copyWith(MediaServerInfo media) {
    return LiveServer(
        hlsHost: hlsHost,
        vodsHost: vodsHost,
        codsHost: codsHost,
        hlsDirectory: hlsDirectory,
        vodsDirectory: vodsDirectory,
        codsDirectory: codsDirectory,
        proxyDirectory: proxyDirectory,
        cpu: media.cpu,
        gpu: media.gpu,
        loadAverage: media.loadAverage,
        memoryTotal: media.memoryTotal,
        memoryFree: media.memoryFree,
        hddTotal: media.hddTotal,
        hddFree: media.hddFree,
        bandwidthIn: media.bandwidthIn,
        bandwidthOut: media.bandwidthOut,
        uptime: media.uptime,
        totalBytesIn: media.totalBytesIn,
        totalBytesOut: media.totalBytesOut,
        timestamp: media.timestamp,
        version: media.version,
        project: media.project,
        os: media.os,
        expirationTime: media.expirationTime,
        syncTime: media.syncTime,
        status: media.status,
        onlineUsers: media.onlineUsers);
  }

  HttpOutputUrl genTemplateHLSHttpOutputUrl(int id) {
    return HttpOutputUrl(
        id: id,
        uri: '${hlsHost.toString()}/master.m3u8',
        httpRoot: hlsDirectory,
        hlsType: HlsType.HLS_PULL,
        hlsSinkType: HlsSinkType.HLSSINK2,
        token: false);
  }

  HttpOutputUrl genTemplateCodsHttpOutputUrl(int id) {
    return HttpOutputUrl(
        id: id,
        uri: '${codsHost.toString()}/master.m3u8',
        httpRoot: codsDirectory,
        hlsType: HlsType.HLS_PULL,
        hlsSinkType: HlsSinkType.HLSSINK2,
        token: false);
  }

  HttpOutputUrl genTemplateVodsHttpOutputUrl(int id) {
    return HttpOutputUrl(
        id: id,
        uri: '${vodsHost.toString()}/master.m3u8',
        httpRoot: vodsDirectory,
        hlsType: HlsType.HLS_PULL,
        hlsSinkType: HlsSinkType.HLSSINK2,
        token: false);
  }

  LiveServer.createInit()
      : hlsHost = Uri(scheme: "http", host: "0.0.0.0", port: 8000),
        vodsHost = Uri(scheme: "http", host: "0.0.0.0", port: 7000),
        codsHost = Uri(scheme: "http", host: "0.0.0.0", port: 6000),
        vodsDirectory = DEFAULT_VODS_DIR,
        codsDirectory = DEFAULT_CODS_DIR,
        hlsDirectory = DEFAULT_HLS_DIR,
        proxyDirectory = DEFAULT_PROXY_DIR,
        super.createInit();

  LiveServer.createDefault()
      : hlsHost = Uri(scheme: "http", host: "0.0.0.0", port: 8000),
        vodsHost = Uri(scheme: "http", host: "0.0.0.0", port: 7000),
        codsHost = Uri(scheme: "http", host: "0.0.0.0", port: 6000),
        vodsDirectory = DEFAULT_VODS_DIR,
        codsDirectory = DEFAULT_CODS_DIR,
        hlsDirectory = DEFAULT_HLS_DIR,
        proxyDirectory = DEFAULT_PROXY_DIR,
        super.createInit();

  factory LiveServer.fromJson(Map<String, dynamic> json) {
    final MediaServerInfo media = MediaServerInfo.fromJson(json);

    return LiveServer(
        hlsHost: Uri.parse(json[HLS_HOST_FIELD]),
        vodsHost: Uri.parse(json[VODS_HOST_FIELD]),
        codsHost: Uri.parse(json[CODS_HOST_FIELD]),
        hlsDirectory: json[HLS_DIRECTORY_FIELD],
        vodsDirectory: json[VODS_DIRECTORY_FIELD],
        codsDirectory: json[CODS_DIRECTORY_FIELD],
        proxyDirectory: json[PROXY_DIRECTORY_FIELD],
        cpu: media.cpu,
        gpu: media.gpu,
        loadAverage: media.loadAverage,
        memoryTotal: media.memoryTotal,
        memoryFree: media.memoryFree,
        hddTotal: media.hddTotal,
        hddFree: media.hddFree,
        bandwidthIn: media.bandwidthIn,
        bandwidthOut: media.bandwidthOut,
        uptime: media.uptime,
        timestamp: media.timestamp,
        totalBytesIn: media.totalBytesIn,
        totalBytesOut: media.totalBytesOut,
        syncTime: media.syncTime,
        version: media.version,
        project: media.project,
        status: media.status,
        expirationTime: media.expirationTime,
        os: media.os,
        onlineUsers: media.onlineUsers);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[HLS_HOST_FIELD] = hlsHost.toString();
    result[VODS_HOST_FIELD] = vodsHost.toString();
    result[CODS_HOST_FIELD] = codsHost.toString();
    result[HLS_DIRECTORY_FIELD] = hlsDirectory;
    result[VODS_DIRECTORY_FIELD] = vodsDirectory;
    result[CODS_DIRECTORY_FIELD] = codsDirectory;
    result[PROXY_DIRECTORY_FIELD] = proxyDirectory;
    return result;
  }

  Uri? generateHttpProxyUrl(String filePath,
      {String protocol = 'http', String root = 'fastocloud'}) {
    final List<String> parts = proxyDirectory.split('/');
    if (parts.isEmpty) {
      return null;
    }

    final int replaced = filePath.indexOf(parts.last);
    if (replaced == -1) {
      return null;
    }

    final stabled = filePath.substring(replaced).split('/');
    final List<String> segments = [root] + stabled;
    return Uri(
        scheme: protocol, host: proxyHost.host, port: proxyHost.port, pathSegments: segments);
  }

  IStream createDefault(StreamType type, String streamLogoIcon,
      {String? backgroundUrl, String? vodTrailerUrl, bool isSerial = false}) {
    if (isVodStreamType(type)) {
      assert(backgroundUrl != null, vodTrailerUrl != null);
    }

    switch (type) {
      case StreamType.PROXY:
        return ProxyStream(icon: streamLogoIcon, output: [OutputUrl.createInvalid(id: 0)]);
      case StreamType.VOD_PROXY:
        return VodProxyStream(
            primeDate: 0,
            name: isSerial ? 'Serial' : 'Vod',
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            output: [OutputUrl.createInvalid(id: 0)]);
      case StreamType.RELAY:
        return RelayStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.ENCODE:
        return EncodeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.TIMESHIFT_RECORDER:
        return TimeshiftRecorderStream(
            icon: streamLogoIcon, input: [InputUrl.createInvalid(id: 0)], output: []);
      case StreamType.TIMESHIFT_PLAYER:
        return TimeshiftPlayerStream(
            timeshiftDir: '~',
            timeshiftDelay: 60,
            icon: streamLogoIcon,
            input: [InputUrl.createRecord(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.CATCHUP:
        return CatchupStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.TEST_LIFE:
        return TestLifeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [OutputUrl.createInvalid(id: 0)]);
      case StreamType.VOD_RELAY:
        return VodRelayStream(
            primeDate: 0,
            name: isSerial ? 'Serial' : 'Vod',
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateVodsHttpOutputUrl(0)]);
      case StreamType.VOD_ENCODE:
        return VodEncodeStream(
            primeDate: 0,
            name: isSerial ? 'Serial' : 'Vod',
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateVodsHttpOutputUrl(0)]);
      case StreamType.COD_RELAY:
        return CodRelayStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateCodsHttpOutputUrl(0)]);
      case StreamType.COD_ENCODE:
        return CodEncodeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateCodsHttpOutputUrl(0)]);
      case StreamType.EVENT:
        return EventStream(
            primeDate: 0,
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateVodsHttpOutputUrl(0)]);
      case StreamType.CV_DATA:
        return CvDataStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [UnknownOutputUrl.createFake(id: 0)]);
      case StreamType.CHANGER_RELAY:
        return ChangerRelayStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.CHANGER_ENCODE:
        return ChangerEncodeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      default:
        throw 'Unknown type';
    }
  }
}

class Server extends LiveServer {
  static const ID_FIELD = 'id';
  static const NAME_FIELD = 'name';

  static const HOST_FIELD = 'host';

  static const RTMP_HOST_FILED = 'rtmp_host';
  static const NGINX_HOST_FIELD = 'nginx_host';
  static const FEEDBACK_DIRECOTRY_FIELD = 'feedback_directory';
  static const TIMESHIFTS_DIRECTORY_FIELD = 'timeshifts_directory';
  static const DATA_DIRECTORY_FIELD = 'data_directory';
  static const PROVIDERS_FIELD = 'providers';
  static const AUTO_START_FIELD = 'auto_start';
  static const VISIBLE_FIELD = 'visible';
  static const ACTIVATION_KEY_FIELD = 'activation_key';
  static const MONITORING_FIELD = 'monitoring';
  static const PRICE_PACKAGE_FIELD = 'price_package';
  static const DESCRIPTION_FIELD = 'description';

  static const String DEFAULT_SERVER_NAME = ServiceName.DEFAULT;
  static const String DEFAULT_FEEDBACK_DIR = '~/streamer/feedback';
  static const String DEFAULT_TIMESHIFTS_DIR = '~/streamer/timeshifts';
  static const String DEFAULT_DATA_DIR = '~/streamer/data';

  static const MIN_NAME_LENGTH = ServiceName.MIN_LENGTH;
  static const MAX_NAME_LENGTH = ServiceName.MAX_LENGTH;

  final String? id;
  String name;
  HostAndPort host;

  HostAndPort rtmpHost;
  HostAndPort nginxHost;
  String feedbackDirectory;
  String timeshiftsDirectory;

  String dataDirectory;
  bool monitoring;
  bool autoStart;
  bool visible;
  List<ServerProvider> providers;
  double pricePackage;

  // optional
  String? description;
  String? activationKey;

  Server(
      {required this.id,
      required this.name,
      required this.host,
      required Uri httpHost,
      required Uri vodsHost,
      required Uri codsHost,
      required this.nginxHost,
      required this.rtmpHost,
      required this.feedbackDirectory,
      required this.timeshiftsDirectory,
      required String hlsDirectory,
      required String vodsDirectory,
      required String codsDirectory,
      required String proxyDirectory,
      required this.dataDirectory,
      required this.providers,
      required this.monitoring,
      required this.autoStart,
      required this.visible,
      required this.pricePackage,
      this.activationKey,
      this.description,
      required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      required int totalBytesIn,
      required int totalBytesOut,
      int? syncTime,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      ServerOnlineUsers? onlineUsers})
      : super(
            hlsHost: httpHost,
            vodsHost: vodsHost,
            codsHost: codsHost,
            vodsDirectory: vodsDirectory,
            codsDirectory: codsDirectory,
            hlsDirectory: hlsDirectory,
            proxyDirectory: proxyDirectory,
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            syncTime: syncTime,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os,
            onlineUsers: onlineUsers);

  Server.createDefault()
      : id = null,
        name = DEFAULT_SERVER_NAME,
        host = HostAndPort.createLocalHostV4(port: 6317),
        nginxHost = HostAndPort.createDefaultRouteHostV4(port: 81),
        rtmpHost = HostAndPort.createDefaultRouteHostV4(port: 1935),
        feedbackDirectory = DEFAULT_FEEDBACK_DIR,
        timeshiftsDirectory = DEFAULT_TIMESHIFTS_DIR,
        dataDirectory = DEFAULT_DATA_DIR,
        monitoring = false,
        autoStart = false,
        visible = true,
        pricePackage = 0.0,
        description = '',
        providers = [],
        activationKey = null,
        super.createInit();

  @override
  Server copy() {
    return Server(
        id: id,
        name: name,
        host: host.copy(),
        httpHost: hlsHost,
        vodsHost: vodsHost,
        codsHost: codsHost,
        nginxHost: nginxHost.copy(),
        rtmpHost: rtmpHost.copy(),
        feedbackDirectory: feedbackDirectory,
        timeshiftsDirectory: timeshiftsDirectory,
        hlsDirectory: hlsDirectory,
        vodsDirectory: vodsDirectory,
        codsDirectory: codsDirectory,
        proxyDirectory: proxyDirectory,
        dataDirectory: dataDirectory,
        monitoring: monitoring,
        autoStart: autoStart,
        visible: visible,
        providers: providers,
        activationKey: activationKey,
        description: description,
        pricePackage: pricePackage,
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        timestamp: timestamp,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        syncTime: syncTime,
        version: version,
        project: project,
        status: status,
        expirationTime: expirationTime,
        os: os,
        onlineUsers: onlineUsers);
  }

  bool isValid() {
    bool req = name.isValidServiceName() &&
        host.isValid() &&
        nginxHost.isValid() &&
        rtmpHost.isValid() &&
        feedbackDirectory.isNotEmpty &&
        timeshiftsDirectory.isNotEmpty &&
        hlsDirectory.isNotEmpty &&
        vodsDirectory.isNotEmpty &&
        codsDirectory.isNotEmpty &&
        proxyDirectory.isNotEmpty &&
        dataDirectory.isNotEmpty &&
        pricePackage.isValidPrice();

    if (req && activationKey != null) {
      req &= activationKey!.isValidActivationKey();
    }
    return req;
  }

  factory Server.fromJson(Map<String, dynamic> json) {
    final List<ServerProvider> _providers = [];
    json[PROVIDERS_FIELD].forEach((element) {
      _providers.add(ServerProvider.fromJson(element));
    });

    final String id = json[ID_FIELD];
    final LiveServer media = LiveServer.fromJson(json);

    final String? activationKey = json[ACTIVATION_KEY_FIELD];
    final String? description = json[DESCRIPTION_FIELD];

    return Server(
        id: id,
        name: json[NAME_FIELD],
        host: HostAndPort.fromJson(json[HOST_FIELD]),
        httpHost: media.hlsHost,
        vodsHost: media.vodsHost,
        codsHost: media.codsHost,
        nginxHost: HostAndPort.fromJson(json[NGINX_HOST_FIELD]),
        rtmpHost: HostAndPort.fromJson(json[RTMP_HOST_FILED]),
        feedbackDirectory: json[FEEDBACK_DIRECOTRY_FIELD],
        timeshiftsDirectory: json[TIMESHIFTS_DIRECTORY_FIELD],
        hlsDirectory: media.hlsDirectory,
        vodsDirectory: media.vodsDirectory,
        codsDirectory: media.codsDirectory,
        proxyDirectory: media.proxyDirectory,
        dataDirectory: json[DATA_DIRECTORY_FIELD],
        providers: _providers,
        monitoring: json[MONITORING_FIELD],
        autoStart: json[AUTO_START_FIELD],
        visible: json[VISIBLE_FIELD],
        pricePackage: json[PRICE_PACKAGE_FIELD],
        activationKey: activationKey,
        description: description,
        cpu: media.cpu,
        gpu: media.gpu,
        loadAverage: media.loadAverage,
        memoryTotal: media.memoryTotal,
        memoryFree: media.memoryFree,
        hddTotal: media.hddTotal,
        hddFree: media.hddFree,
        bandwidthIn: media.bandwidthIn,
        bandwidthOut: media.bandwidthOut,
        uptime: media.uptime,
        timestamp: media.timestamp,
        totalBytesIn: media.totalBytesIn,
        totalBytesOut: media.totalBytesOut,
        syncTime: media.syncTime,
        version: media.version,
        project: media.project,
        status: media.status,
        expirationTime: media.expirationTime,
        os: media.os,
        onlineUsers: media.onlineUsers);
  }

  @override
  Map<String, dynamic> toJson() {
    final _providersJson = [];
    for (final element in providers) {
      _providersJson.add(element.toJson());
    }

    final Map<String, dynamic> result = super.toJson();
    result[ID_FIELD] = id;
    result[NAME_FIELD] = name;
    result[HOST_FIELD] = host.toJson();
    result[PROVIDERS_FIELD] = providers;
    result[HOST_FIELD] = host.toJson();
    result[RTMP_HOST_FILED] = rtmpHost.toJson();
    result[FEEDBACK_DIRECOTRY_FIELD] = feedbackDirectory;
    result[TIMESHIFTS_DIRECTORY_FIELD] = timeshiftsDirectory;
    result[DATA_DIRECTORY_FIELD] = dataDirectory;
    result[MONITORING_FIELD] = monitoring;
    result[AUTO_START_FIELD] = autoStart;
    result[VISIBLE_FIELD] = visible;
    result[PRICE_PACKAGE_FIELD] = pricePackage;
    result[DESCRIPTION_FIELD] = description;
    if (activationKey != null) {
      result[ACTIVATION_KEY_FIELD] = activationKey;
    }
    return result;
  }

  ProviderRole getProviderRoleById(String pid) {
    for (final ServerProvider provider in providers) {
      if (provider.id == pid) {
        return provider.role;
      }
    }
    return ProviderRole.READ;
  }

  Uri? generateNginxUrl(String httpUrl, {String protocol = 'http', String root = 'fastocloud'}) {
    final Uri? original = Uri.tryParse(httpUrl);
    if (original == null) {
      return null;
    }

    List<String>? parts;
    if (original.port == hlsHost.port) {
      parts = hlsDirectory.split('/');
    } else if (original.port == vodsHost.port) {
      parts = vodsDirectory.split('/');
    } else if (original.port == codsHost.port) {
      parts = codsDirectory.split('/');
    }

    if (parts == null || parts.isEmpty) {
      return null;
    }

    final List<String> segments = [root] + [parts.last] + original.pathSegments;
    return Uri(
        scheme: protocol, host: nginxHost.host, port: nginxHost.port, pathSegments: segments);
  }

  Uri generateRtmpUrl(String streamName, {String protocol = 'rtmp', String root = 'live'}) {
    final List<String> segments = [root, streamName];
    return Uri(scheme: protocol, host: rtmpHost.host, port: rtmpHost.port, pathSegments: segments);
  }

  Uri? generateHttpDataUrl(String filePath,
      {String protocol = 'http', String root = 'fastocloud'}) {
    final List<String> parts = dataDirectory.split('/');
    if (parts.isEmpty) {
      return null;
    }

    final int replaced = filePath.indexOf(parts.last);
    if (replaced == -1) {
      return null;
    }

    final stabled = filePath.substring(replaced).split('/');
    final List<String> segments = [root] + stabled;
    return Uri(
        scheme: protocol, host: nginxHost.host, port: nginxHost.port, pathSegments: segments);
  }

  @override
  IStream createDefault(StreamType type, String streamLogoIcon,
      {String? backgroundUrl, String? vodTrailerUrl, bool isSerial = false}) {
    if (isVodStreamType(type)) {
      assert(backgroundUrl != null, vodTrailerUrl != null);
    }

    switch (type) {
      case StreamType.PROXY:
        return ProxyStream(icon: streamLogoIcon, output: [OutputUrl.createInvalid(id: 0)]);
      case StreamType.VOD_PROXY:
        return VodProxyStream(
            primeDate: 0,
            name: isSerial ? 'Serial' : 'Vod',
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            output: [OutputUrl.createInvalid(id: 0)]);
      case StreamType.RELAY:
        return RelayStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.ENCODE:
        return EncodeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.TIMESHIFT_RECORDER:
        return TimeshiftRecorderStream(
            icon: streamLogoIcon, input: [InputUrl.createInvalid(id: 0)], output: []);
      case StreamType.TIMESHIFT_PLAYER:
        return TimeshiftPlayerStream(
            timeshiftDelay: 60,
            timeshiftDir: '~',
            icon: streamLogoIcon,
            input: [InputUrl.createRecord(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.CATCHUP:
        return CatchupStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.TEST_LIFE:
        return TestLifeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [OutputUrl.createInvalid(id: 0)]);
      case StreamType.VOD_RELAY:
        return VodRelayStream(
            primeDate: 0,
            name: isSerial ? 'Serial' : 'Vod',
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateVodsHttpOutputUrl(0)]);
      case StreamType.VOD_ENCODE:
        return VodEncodeStream(
            primeDate: 0,
            name: isSerial ? 'Serial' : 'Vod',
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateVodsHttpOutputUrl(0)]);
      case StreamType.COD_RELAY:
        return CodRelayStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateCodsHttpOutputUrl(0)]);
      case StreamType.COD_ENCODE:
        return CodEncodeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateCodsHttpOutputUrl(0)]);
      case StreamType.EVENT:
        return EventStream(
            primeDate: 0,
            icon: streamLogoIcon,
            backgroundUrl: backgroundUrl!,
            trailerUrl: vodTrailerUrl!,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateVodsHttpOutputUrl(0)]);
      case StreamType.CV_DATA:
        return CvDataStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [UnknownOutputUrl.createFake(id: 0)]);
      case StreamType.CHANGER_RELAY:
        return ChangerRelayStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      case StreamType.CHANGER_ENCODE:
        return ChangerEncodeStream(
            icon: streamLogoIcon,
            input: [InputUrl.createInvalid(id: 0)],
            output: [genTemplateHLSHttpOutputUrl(0)]);
      default:
        throw 'Unknown type';
    }
  }
}

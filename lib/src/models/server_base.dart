class ServerStatus {
  static const int _INIT_CONSTANT = 0;
  static const int _CONNECTED_CONSTANT = 1;
  static const int _ACTIVE_CONSTANT = 2;

  final int _value;

  const ServerStatus._(this._value);

  int toInt() {
    return _value;
  }

  @override
  String toString() {
    if (_value == _INIT_CONSTANT) {
      return 'Init';
    } else if (_value == _CONNECTED_CONSTANT) {
      return 'Connected';
    }

    assert(_value == _ACTIVE_CONSTANT);
    return 'Active';
  }

  factory ServerStatus.fromInt(int status) {
    if (status == _INIT_CONSTANT) {
      return INIT;
    } else if (status == _CONNECTED_CONSTANT) {
      return CONNECTED;
    } else if (status == _ACTIVE_CONSTANT) {
      return ACTIVE;
    }

    throw 'Unknown server status: $status';
  }

  static List<ServerStatus> get values => [INIT, CONNECTED, ACTIVE];

  static const ServerStatus INIT = ServerStatus._(_INIT_CONSTANT);
  static const ServerStatus CONNECTED = ServerStatus._(_CONNECTED_CONSTANT);
  static const ServerStatus ACTIVE = ServerStatus._(_ACTIVE_CONSTANT);
}

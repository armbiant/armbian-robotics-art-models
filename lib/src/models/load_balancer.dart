import 'package:fastocloud_dart_models/src/models/base.dart';
import 'package:fastocloud_dart_models/src/models/machine.dart';
import 'package:fastocloud_dart_models/src/models/server_base.dart';
import 'package:fastocloud_dart_models/src/models/server_provider.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/client_info.dart';

class LBOnlineUsers {
  static const _DAEMON_FIELD = 'daemon';
  static const _SUBSCRIBERS_FIELD = 'subscribers';

  final int daemon;
  final int subscribers;

  LBOnlineUsers({required this.daemon, required this.subscribers});

  LBOnlineUsers copy() {
    return LBOnlineUsers(daemon: daemon, subscribers: subscribers);
  }

  factory LBOnlineUsers.fromJson(Map<String, dynamic> json) {
    final daemon = json[_DAEMON_FIELD];
    final subscribers = json[_SUBSCRIBERS_FIELD];
    return LBOnlineUsers(daemon: daemon, subscribers: subscribers);
  }

  Map<String, dynamic> toJson() {
    return {_DAEMON_FIELD: daemon, _SUBSCRIBERS_FIELD: subscribers};
  }
}

class LoadBalanceServerInfo extends Machine {
  static const _ONLINE_USERS_FIELDS = 'online_users';

  LBOnlineUsers? onlineUsers;

  LoadBalanceServerInfo(
      {required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      int? syncTime,
      required int totalBytesIn,
      required int totalBytesOut,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      this.onlineUsers})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            syncTime: syncTime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os);

  @override
  LoadBalanceServerInfo copy() {
    return LoadBalanceServerInfo(
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        timestamp: timestamp,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut);
  }

  LoadBalanceServerInfo.createInit()
      : onlineUsers = null,
        super.createInit();

  factory LoadBalanceServerInfo.fromJson(Map<String, dynamic> json) {
    final Machine mach = Machine.fromJson(json);
    LBOnlineUsers? onlineUsers;
    if (json.containsKey(_ONLINE_USERS_FIELDS)) {
      onlineUsers = LBOnlineUsers.fromJson(json[_ONLINE_USERS_FIELDS]);
    }
    return LoadBalanceServerInfo(
        cpu: mach.cpu,
        gpu: mach.gpu,
        loadAverage: mach.loadAverage,
        memoryTotal: mach.memoryTotal,
        memoryFree: mach.memoryFree,
        hddTotal: mach.hddTotal,
        hddFree: mach.hddFree,
        bandwidthIn: mach.bandwidthIn,
        bandwidthOut: mach.bandwidthOut,
        uptime: mach.uptime,
        timestamp: mach.timestamp,
        totalBytesIn: mach.totalBytesIn,
        totalBytesOut: mach.totalBytesOut,
        syncTime: mach.syncTime,
        version: mach.version,
        project: mach.project,
        status: mach.status,
        expirationTime: mach.expirationTime,
        os: mach.os,
        onlineUsers: onlineUsers);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[_ONLINE_USERS_FIELDS] = onlineUsers?.toJson();
    return data;
  }
}

class LoadBalancer extends LoadBalanceServerInfo {
  static const ID_FIELD = 'id';
  static const NAME_FIELD = 'name';
  static const HOST_FIELD = 'host';
  static const CLIENTS_HOST = 'clients_host';
  static const CATCHUPS_HOST_FIELD = 'catchups_host';
  static const CATCHUPS_HTTP_ROOT_FIELD = 'catchups_http_root';
  static const MONITORING_FIELD = 'monitoring';
  static const ACTIVATION_KEY_FIELD = 'activation_key';
  static const AUTO_START_FIELD = 'auto_start';
  static const PROVIDERS_FIELD = 'providers';

  static const String DEFAULT_SERVER_NAME = 'Load Balancer';
  static const MIN_NAME_LENGTH = ServiceName.MIN_LENGTH;
  static const MAX_NAME_LENGTH = ServiceName.MAX_LENGTH;
  static const String DEFAULT_CATHUPS_HTTP_DIR = '~/streamer/hls';

  final String? id;
  String name;
  HostAndPort host;
  HostAndPort clientsHost;
  HostAndPort catchupsHost;
  String catchupsHttpRoot;
  bool monitoring;
  bool autoStart;
  List<ServerProvider> providers;

  // optional
  String? activationKey;

  LoadBalancer(
      {required this.id,
      required this.name,
      required this.host,
      required this.clientsHost,
      required this.catchupsHost,
      required this.catchupsHttpRoot,
      required this.monitoring,
      required this.providers,
      required this.autoStart,
      this.activationKey,
      required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int uptime,
      required int timestamp,
      required int totalBytesIn,
      required int totalBytesOut,
      int? syncTime,
      String? version,
      String? project,
      ServerStatus? status,
      int? expirationTime,
      OperationSystem? os,
      LBOnlineUsers? onlineUsers})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut,
            syncTime: syncTime,
            version: version,
            project: project,
            status: status,
            expirationTime: expirationTime,
            os: os,
            onlineUsers: onlineUsers);

  LoadBalancer.createDefault()
      : id = null,
        name = DEFAULT_SERVER_NAME,
        host = HostAndPort.createLocalHostV4(port: 5317),
        clientsHost = HostAndPort.createDefaultRouteHostV4(port: 6000),
        catchupsHost = HostAndPort.createDefaultRouteHostV4(port: 8000),
        catchupsHttpRoot = DEFAULT_CATHUPS_HTTP_DIR,
        monitoring = false,
        autoStart = false,
        providers = [],
        activationKey = null,
        super.createInit();

  @override
  LoadBalancer copy() {
    return LoadBalancer(
        id: id,
        name: name,
        host: host.copy(),
        clientsHost: clientsHost.copy(),
        catchupsHost: catchupsHost.copy(),
        catchupsHttpRoot: catchupsHttpRoot,
        providers: providers,
        autoStart: autoStart,
        monitoring: monitoring,
        activationKey: activationKey,
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        timestamp: timestamp,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        syncTime: syncTime,
        version: version,
        project: project,
        status: status,
        expirationTime: expirationTime,
        os: os,
        onlineUsers: onlineUsers);
  }

  @override
  bool isActive() {
    return status == ServerStatus.ACTIVE;
  }

  bool isValid() {
    bool req = name.isValidServiceName() &&
        host.isValid() &&
        clientsHost.isValid() &&
        catchupsHost.isValid() &&
        catchupsHttpRoot.isNotEmpty;
    if (req && activationKey != null) {
      req &= activationKey!.isValidActivationKey();
    }
    return req;
  }

  factory LoadBalancer.fromJson(Map<String, dynamic> json) {
    final id = json[ID_FIELD];
    final _json = json[PROVIDERS_FIELD];
    final List<ServerProvider> _providers = [];
    _json.forEach((element) => _providers.add(ServerProvider.fromJson(element)));
    final String? activationKey = json[ACTIVATION_KEY_FIELD];

    final LoadBalanceServerInfo load = LoadBalanceServerInfo.fromJson(json);
    return LoadBalancer(
        id: id,
        name: json[NAME_FIELD],
        host: HostAndPort.fromJson(json[HOST_FIELD]),
        clientsHost: HostAndPort.fromJson(json[CLIENTS_HOST]),
        catchupsHost: HostAndPort.fromJson(json[CATCHUPS_HOST_FIELD]),
        catchupsHttpRoot: json[CATCHUPS_HTTP_ROOT_FIELD],
        providers: _providers,
        monitoring: json[MONITORING_FIELD],
        autoStart: json[AUTO_START_FIELD],
        activationKey: activationKey,
        cpu: load.cpu,
        gpu: load.gpu,
        loadAverage: load.loadAverage,
        memoryTotal: load.memoryTotal,
        memoryFree: load.memoryFree,
        hddTotal: load.hddTotal,
        hddFree: load.hddFree,
        bandwidthIn: load.bandwidthIn,
        bandwidthOut: load.bandwidthOut,
        version: load.version,
        project: load.project,
        uptime: load.uptime,
        totalBytesIn: load.totalBytesIn,
        totalBytesOut: load.totalBytesOut,
        syncTime: load.syncTime,
        timestamp: load.timestamp,
        status: load.status,
        onlineUsers: load.onlineUsers,
        os: load.os,
        expirationTime: load.expirationTime);
  }

  @override
  Map<String, dynamic> toJson() {
    final _providersJson = [];
    for (final element in providers) {
      _providersJson.add(element.toJson());
    }

    final Map<String, dynamic> result = super.toJson();
    result[ID_FIELD] = id;
    result[NAME_FIELD] = name;
    result[HOST_FIELD] = host.toJson();
    result[PROVIDERS_FIELD] = providers;
    result[CLIENTS_HOST] = clientsHost.toJson();
    result[CATCHUPS_HOST_FIELD] = catchupsHost.toJson();
    result[CATCHUPS_HTTP_ROOT_FIELD] = catchupsHttpRoot;
    result[MONITORING_FIELD] = monitoring;
    result[AUTO_START_FIELD] = autoStart;
    if (activationKey != null) {
      result[ACTIVATION_KEY_FIELD] = activationKey;
    }
    return result;
  }

  ProviderRole getProviderRoleById(String pid) {
    for (final ServerProvider provider in providers) {
      if (provider.id == pid) {
        return provider.role;
      }
    }
    return ProviderRole.READ;
  }
}

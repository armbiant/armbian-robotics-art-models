import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';

// enums
class UserAgent {
  static const _GSTREAMER_CONSTANT = 0;
  static const _VLC_CONSTANT = 1;
  static const _FFMPEG_CONSTANT = 2;
  static const _WINK_CONSTANT = 3;
  static const _CHROME_CONSTANT = 4;
  static const _MOZILLA_CONSTANT = 5;
  static const _SAFARI_CONSTANT = 6;

  final int _value;

  const UserAgent._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _GSTREAMER_CONSTANT) {
      return 'GStreamer';
    } else if (_value == _VLC_CONSTANT) {
      return 'VLC';
    } else if (_value == _FFMPEG_CONSTANT) {
      return 'FFMPEG';
    } else if (_value == _WINK_CONSTANT) {
      return 'Wink';
    } else if (_value == _CHROME_CONSTANT) {
      return 'Chrome';
    } else if (_value == _MOZILLA_CONSTANT) {
      return 'Mozilla';
    }
    return 'Safari';
  }

  factory UserAgent.fromInt(int value) {
    if (value == _GSTREAMER_CONSTANT) {
      return UserAgent.GSTREAMER;
    } else if (value == _VLC_CONSTANT) {
      return UserAgent.VLC;
    } else if (value == _FFMPEG_CONSTANT) {
      return UserAgent.FFMPEG;
    } else if (value == _WINK_CONSTANT) {
      return UserAgent.WINK;
    } else if (value == _CHROME_CONSTANT) {
      return UserAgent.CHROME;
    } else if (value == _MOZILLA_CONSTANT) {
      return UserAgent.MOZILLA;
    }
    return UserAgent.SAFARI;
  }

  static List<UserAgent> get values => [GSTREAMER, VLC, FFMPEG, WINK, CHROME, MOZILLA, SAFARI];

  static const UserAgent GSTREAMER = UserAgent._(_GSTREAMER_CONSTANT);
  static const UserAgent VLC = UserAgent._(_VLC_CONSTANT);
  static const UserAgent FFMPEG = UserAgent._(_FFMPEG_CONSTANT);
  static const UserAgent WINK = UserAgent._(_WINK_CONSTANT);
  static const UserAgent CHROME = UserAgent._(_CHROME_CONSTANT);
  static const UserAgent MOZILLA = UserAgent._(_MOZILLA_CONSTANT);
  static const UserAgent SAFARI = UserAgent._(_SAFARI_CONSTANT);
}

class QualityPrefer {
  static const _AUDIO_CONSTANT = 0;
  static const _VIDEO_CONSTANT = 1;
  static const _BOTH_CONSTANT = 2;

  final int _value;

  const QualityPrefer._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _AUDIO_CONSTANT) {
      return 'Audio';
    } else if (_value == _VIDEO_CONSTANT) {
      return 'Video';
    }
    return 'Both';
  }

  factory QualityPrefer.fromInt(int value) {
    if (value == _AUDIO_CONSTANT) {
      return QualityPrefer.AUDIO;
    } else if (value == _VIDEO_CONSTANT) {
      return QualityPrefer.VIDEO;
    }
    return QualityPrefer.BOTH;
  }

  static List<QualityPrefer> get values => [AUDIO, VIDEO, BOTH];

  static const QualityPrefer AUDIO = QualityPrefer._(_AUDIO_CONSTANT);
  static const QualityPrefer VIDEO = QualityPrefer._(_VIDEO_CONSTANT);
  static const QualityPrefer BOTH = QualityPrefer._(_BOTH_CONSTANT);
}

// models
class PyFastoStream {
  static const _HTTP_PROXY_FIELD = 'http_proxy';
  static const _HTTPS_PROXY_FIELD = 'https_proxy';
  static const _PREFER_FIELD = 'prefer';

  String? httpProxy;
  String? httpsProxy;
  QualityPrefer prefer = QualityPrefer.BOTH;

  PyFastoStream({this.httpProxy, this.httpsProxy, this.prefer = QualityPrefer.BOTH});

  PyFastoStream copy() {
    return PyFastoStream(httpProxy: httpProxy, httpsProxy: httpsProxy);
  }

  factory PyFastoStream.fromJson(Map<String, dynamic> json) {
    String? http;
    if (json.containsKey(_HTTP_PROXY_FIELD)) {
      http = json[_HTTP_PROXY_FIELD];
    }
    String? https;
    if (json.containsKey(_HTTPS_PROXY_FIELD)) {
      https = json[_HTTPS_PROXY_FIELD];
    }
    final QualityPrefer prefer = QualityPrefer.fromInt(json[_PREFER_FIELD]);
    return PyFastoStream(httpProxy: http, httpsProxy: https, prefer: prefer);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    if (httpProxy != null) {
      result[_HTTP_PROXY_FIELD] = httpProxy;
    }
    if (httpsProxy != null) {
      result[_HTTPS_PROXY_FIELD] = httpsProxy;
    }
    result[_PREFER_FIELD] = prefer.toInt();
    return result;
  }
}

class RtmpSrcType {
  static const int _RTMP_SRC_CONSTANT = 0;
  static const int _RTMP2_SRC_CONSTANT = 1;

  final int _value;

  const RtmpSrcType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _RTMP_SRC_CONSTANT) {
      return 'RTMPSRC';
    }
    return 'RTMP2SRC';
  }

  factory RtmpSrcType.fromInt(int value) {
    if (value == _RTMP_SRC_CONSTANT) {
      return RtmpSrcType.RTMPSRC;
    }
    return RtmpSrcType.RTMP2SRC;
  }

  static List<RtmpSrcType> get values => [RTMPSRC, RTMP2SRC];

  static const RtmpSrcType RTMPSRC = RtmpSrcType._(_RTMP_SRC_CONSTANT);
  static const RtmpSrcType RTMP2SRC = RtmpSrcType._(_RTMP2_SRC_CONSTANT);
}

class InputUrl {
  static const ID_FIELD = 'id';
  static const URI_FIELD = 'uri';

  static const WEBRTC_URL = 'unknown://webrtc';
  static const RECORD_URL = 'unknown://record';

  int id;
  String uri;

  InputUrl({required this.id, required this.uri});

  InputUrl.createWebRTC({required this.id}) : uri = WEBRTC_URL;

  InputUrl.createRecord({required this.id}) : uri = RECORD_URL;

  InputUrl.createInvalid({required this.id}) : uri = '';

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, URI_FIELD: uri};
  }

  bool isValid() {
    if (uri.isEmpty) {
      return false;
    }
    final parsed = Uri.tryParse(uri);
    return parsed != null;
  }

  InputUrl copy() {
    return InputUrl(id: id, uri: uri);
  }

  factory InputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[ID_FIELD];
    final uri = json[URI_FIELD];
    return InputUrl(id: id, uri: uri);
  }
}

class RtmpInputUrl extends InputUrl {
  static const RTMPSRC_TYPE_FIELD = 'rtmpsrc_type';

  static const List<Protocol> protocols = [
    Protocol.RTMP,
    Protocol.RTMPS,
    Protocol.RTMPT,
    Protocol.RTMPE,
    Protocol.RTMFP,
    Protocol.RTMPTE,
    Protocol.RTMPTS
  ];

  RtmpSrcType? rtmpSrcType;

  RtmpInputUrl({required int id, required String uri, this.rtmpSrcType}) : super(id: id, uri: uri);

  @override
  RtmpInputUrl copy() {
    return RtmpInputUrl(id: id, uri: uri, rtmpSrcType: rtmpSrcType);
  }

  factory RtmpInputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[OutputUrl.ID_FIELD];
    final uri = json[OutputUrl.URI_FIELD];
    final result = RtmpInputUrl(id: id, uri: uri);
    if (json.containsKey(RtmpInputUrl.RTMPSRC_TYPE_FIELD)) {
      result.rtmpSrcType = RtmpSrcType.fromInt(json[RtmpInputUrl.RTMPSRC_TYPE_FIELD]);
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (rtmpSrcType != null) {
      result[RTMPSRC_TYPE_FIELD] = rtmpSrcType!.toInt();
    }
    return result;
  }
}

class HttpInputUrl extends InputUrl {
  static const _STREAM_LINK_FIELD = 'stream_link';
  static const _USER_AGENT_FIELD = 'user_agent';
  static const _PROXY_FIELD = 'proxy';
  static const _WPE_FIELD = 'wpe';

  static const List<Protocol> protocols = [Protocol.HTTP, Protocol.HTTPS];

  PyFastoStream? streamLink;
  UserAgent? userAgent;
  String? proxy;
  Wpe? wpe;

  HttpInputUrl(
      {required int id, required String uri, this.streamLink, this.userAgent, this.proxy, this.wpe})
      : super(id: id, uri: uri);

  @override
  HttpInputUrl copy() {
    final PyFastoStream? streamLinkCopy = streamLink?.copy();
    return HttpInputUrl(
        id: id, uri: uri, streamLink: streamLinkCopy, userAgent: userAgent, proxy: proxy, wpe: wpe);
  }

  factory HttpInputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[InputUrl.ID_FIELD];
    final uri = json[InputUrl.URI_FIELD];
    final HttpInputUrl result = HttpInputUrl(id: id, uri: uri);

    if (json.containsKey(_STREAM_LINK_FIELD)) {
      result.streamLink = PyFastoStream.fromJson(json[_STREAM_LINK_FIELD]);
    }
    if (json.containsKey(_USER_AGENT_FIELD)) {
      result.userAgent = UserAgent.fromInt(json[_USER_AGENT_FIELD]);
    }
    if (json.containsKey(_PROXY_FIELD)) {
      result.proxy = json[_PROXY_FIELD];
    }
    if (json.containsKey(_WPE_FIELD)) {
      result.wpe = Wpe.fromJson(json[_WPE_FIELD]);
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (streamLink != null) {
      result[_STREAM_LINK_FIELD] = streamLink!.toJson();
    }
    if (userAgent != null) {
      result[_USER_AGENT_FIELD] = userAgent!.toInt();
    }
    if (proxy != null) {
      result[_PROXY_FIELD] = proxy;
    }
    if (wpe != null) {
      result[_WPE_FIELD] = wpe!.toJson();
    }
    return result;
  }
}

class UdpInputUrl extends InputUrl {
  static const PROGRAM_NUMBER_FIELD = 'program_number';
  static const MULTICAST_IFACE_FIELD = 'multicast_iface';

  static const protocol = Protocol.UDP;

  int? programNumber;
  String? multicastIface;

  UdpInputUrl({required int id, required String uri, this.programNumber, this.multicastIface})
      : super(id: id, uri: uri);

  @override
  UdpInputUrl copy() {
    return UdpInputUrl(
        id: id, uri: uri, programNumber: programNumber, multicastIface: multicastIface);
  }

  factory UdpInputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[InputUrl.ID_FIELD];
    final uri = json[InputUrl.URI_FIELD];
    final UdpInputUrl result = UdpInputUrl(id: id, uri: uri);

    if (json.containsKey(PROGRAM_NUMBER_FIELD)) {
      result.programNumber = json[PROGRAM_NUMBER_FIELD];
    }
    if (json.containsKey(MULTICAST_IFACE_FIELD)) {
      result.multicastIface = json[MULTICAST_IFACE_FIELD];
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (programNumber != null) {
      result[PROGRAM_NUMBER_FIELD] = programNumber;
    }
    if (multicastIface != null) {
      result[MULTICAST_IFACE_FIELD] = multicastIface;
    }
    return result;
  }
}

class SrtInputUrl extends InputUrl {
  static const SRT_KEY_FIELD = 'srt_key';
  static const SRT_MODE_FIELD = 'srt_mode';

  static const protocol = Protocol.SRT;

  SrtMode? mode;
  SrtKey? srtKey;

  SrtInputUrl({required int id, required String uri, this.mode, this.srtKey})
      : super(id: id, uri: uri);

  @override
  SrtInputUrl copy() {
    final SrtKey? key = srtKey?.copy();
    return SrtInputUrl(id: id, uri: uri, mode: mode, srtKey: key);
  }

  factory SrtInputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[InputUrl.ID_FIELD];
    final uri = json[InputUrl.URI_FIELD];
    final SrtInputUrl result = SrtInputUrl(id: id, uri: uri);
    if (json.containsKey(SrtInputUrl.SRT_MODE_FIELD)) {
      result.mode = SrtMode.fromInt(json[SrtInputUrl.SRT_MODE_FIELD]);
    }
    if (json.containsKey(SrtInputUrl.SRT_KEY_FIELD)) {
      result.srtKey = SrtKey.fromJson(json[SrtInputUrl.SRT_KEY_FIELD]);
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (mode != null) {
      result[SrtInputUrl.SRT_MODE_FIELD] = mode!.toInt();
    }
    if (srtKey != null) {
      result[SrtInputUrl.SRT_KEY_FIELD] = srtKey!.toJson();
    }
    return result;
  }
}

class FileInputUrl extends InputUrl {
  static const protocol = Protocol.FILE;

  FileInputUrl({required int id, required String uri}) : super(id: id, uri: uri);

  @override
  FileInputUrl copy() {
    return FileInputUrl(id: id, uri: uri);
  }

  factory FileInputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[InputUrl.ID_FIELD];
    final uri = json[InputUrl.URI_FIELD];
    final FileInputUrl result = FileInputUrl(id: id, uri: uri);
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    return result;
  }
}

InputUrl makeInputUrl(Map<String, dynamic> json) {
  final id = json[InputUrl.ID_FIELD];
  final uri = json[InputUrl.URI_FIELD];
  final Uri parsed = Uri.parse(uri);
  final proto = Protocol.fromString(parsed.scheme);
  if (HttpInputUrl.protocols.contains(proto)) {
    return HttpInputUrl.fromJson(json);
  } else if (RtmpInputUrl.protocols.contains(proto)) {
    return RtmpInputUrl.fromJson(json);
  } else if (proto == UdpInputUrl.protocol) {
    return UdpInputUrl.fromJson(json);
  } else if (proto == FileInputUrl.protocol) {
    return FileInputUrl.fromJson(json);
  } else if (proto == SrtInputUrl.protocol) {
    return SrtInputUrl.fromJson(json);
  }
  return InputUrl(id: id, uri: uri);
}

extension InputUrls on List<InputUrl> {
  bool isValidInputUrls() {
    if (isEmpty) {
      return false;
    }

    for (final InputUrl url in this) {
      if (!url.isValid()) {
        return false;
      }
    }
    return true;
  }
}

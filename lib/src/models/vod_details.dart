import 'dart:core';

import 'package:fastocloud_dart_models/src/models/base.dart';
import 'package:fastocloud_dart_models/src/models/stream/base.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';

class VodFields {
  static const PRIME_DATE_FIELD = 'prime_date';
  static const ID = 'id';
  static const VOD_TYPE_FIELD = 'vod_type';
  static const BACKGROUND_URL_FIELD = 'background_url';
  static const TRAILER_URL_FIELD = 'trailer_url';
  static const USER_SCORE_FIELD = 'user_score';
  static const COUNTRY_FIELD = 'country';
  static const DURATION_FIELD = 'duration';
  static const DIRECTORS_FIELD = 'directors';
  static const CAST_FIELD = 'cast';
  static const PRODUCTION_FIELD = 'production';
  static const GENRES_FIELD = 'genres';

  int primeDate;
  VodType vodType;
  double userScore;
  String country;
  int duration;
  List<String> directors; // ['Travolta', 'Mike']
  List<String> cast; // ['Jhonny Dep', 'Arnold']
  List<String> production; // ['WB', 'Netflix']
  List<String> genres; // ['Comedy', 'Drama']

  String backgroundUrl;
  String trailerUrl;

  VodFields(
      {required this.primeDate,
      this.vodType = VodType.VOD,
      this.userScore = UserScore.DEFAULT,
      this.country = Country.DEFAULT,
      this.duration = VodDuration.DEFAULT,
      this.directors = const [],
      this.cast = const [],
      this.production = const [],
      this.genres = const [],
      this.backgroundUrl = DEFAULT_BACKGROUND_URL,
      this.trailerUrl = INVALID_TRAILER_URL});

  VodFields copy() {
    return VodFields(
        primeDate: primeDate,
        vodType: vodType,
        userScore: userScore,
        country: country,
        duration: duration,
        cast: cast,
        production: production,
        genres: genres,
        directors: directors,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl);
  }

  factory VodFields.fromJson(Map<String, dynamic> json) {
    final int primeDate = json[PRIME_DATE_FIELD];
    final VodType vodType = VodType.fromInt(json[VOD_TYPE_FIELD]);
    final double userScore = json[USER_SCORE_FIELD];
    final String country = json[COUNTRY_FIELD];
    final int duration = json[DURATION_FIELD];
    final directors = json[DIRECTORS_FIELD].cast<String>();
    final cast = json[CAST_FIELD].cast<String>();
    final production = json[PRODUCTION_FIELD].cast<String>();
    final genres = json[GENRES_FIELD].cast<String>();

    final String backgroundUrl = json[BACKGROUND_URL_FIELD];
    final String trailerUrl = json[TRAILER_URL_FIELD];

    return VodFields(
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      PRIME_DATE_FIELD: primeDate,
      VOD_TYPE_FIELD: vodType.toInt(),
      USER_SCORE_FIELD: userScore,
      COUNTRY_FIELD: country,
      DURATION_FIELD: duration,
      BACKGROUND_URL_FIELD: backgroundUrl,
      TRAILER_URL_FIELD: trailerUrl,
      DIRECTORS_FIELD: directors,
      CAST_FIELD: cast,
      PRODUCTION_FIELD: production,
      GENRES_FIELD: genres
    };

    return result;
  }
}

class VodDetails {
  String? id;
  String? name;
  String? icon;
  String? backgroundLogoUrl;
  int? primeDate;
  String? description;
  double? userScore;
  String? country;
  int? duration;

  List<String>? directors; // ['Travolta', 'Mike']
  List<String>? cast; // ['Jony Dep', 'Arnold']
  List<String>? production; // ['WB', 'Netflix']
  List<String>? genres; // ['Comedy', 'Drama']

  VodDetails(
      {required this.id,
      required this.name,
      required this.icon,
      required this.backgroundLogoUrl,
      required this.primeDate,
      required this.description,
      required this.country,
      this.userScore = UserScore.DEFAULT,
      this.duration = VodDuration.DEFAULT,
      this.directors = const [],
      this.cast = const [],
      this.production = const [],
      this.genres = const []});

  VodDetails copy() {
    return VodDetails(
        id: id,
        name: name,
        icon: icon,
        backgroundLogoUrl: backgroundLogoUrl,
        primeDate: primeDate,
        description: description,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  factory VodDetails.fromJson(Map<String, dynamic> json) {
    final String? id = json[VodFields.ID];
    final String? name = json[IStream.NAME_FIELD];
    final String? backgroundLogoUrl = json[VodFields.BACKGROUND_URL_FIELD];
    final String? icon = json[IStream.ICON_FIELD];
    final int? primeDate = json[VodFields.PRIME_DATE_FIELD];
    final String? description = json[IStream.DESCRIPTION_FIELD];
    final double? userScore = json[VodFields.USER_SCORE_FIELD];
    final String? country = json[VodFields.COUNTRY_FIELD];
    final int? duration = json[VodFields.DURATION_FIELD];
    List<String>? directors;
    if (json.containsKey(VodFields.DIRECTORS_FIELD)) {
      directors = json[VodFields.DIRECTORS_FIELD].cast<String>();
    }
    List<String>? cast;
    if (json.containsKey(VodFields.CAST_FIELD)) {
      cast = json[VodFields.CAST_FIELD].cast<String>();
    }
    List<String>? production;
    if (json.containsKey(VodFields.PRODUCTION_FIELD)) {
      production = json[VodFields.PRODUCTION_FIELD].cast<String>();
    }
    List<String>? genres;
    if (json.containsKey(VodFields.GENRES_FIELD)) {
      genres = json[VodFields.GENRES_FIELD].cast<String>();
    }

    return VodDetails(
        id: id,
        name: name,
        icon: icon,
        backgroundLogoUrl: backgroundLogoUrl,
        primeDate: primeDate,
        description: description,
        userScore: userScore,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  Map<String, dynamic> toJson() {
    return {
      VodFields.ID: id,
      IStream.NAME_FIELD: name,
      IStream.ICON_FIELD: icon,
      VodFields.BACKGROUND_URL_FIELD: backgroundLogoUrl,
      VodFields.PRIME_DATE_FIELD: primeDate,
      IStream.DESCRIPTION_FIELD: description,
      VodFields.USER_SCORE_FIELD: userScore,
      VodFields.COUNTRY_FIELD: country,
      VodFields.DURATION_FIELD: duration,
      VodFields.DIRECTORS_FIELD: directors,
      VodFields.CAST_FIELD: cast,
      VodFields.PRODUCTION_FIELD: production,
      VodFields.GENRES_FIELD: genres
    };
  }
}

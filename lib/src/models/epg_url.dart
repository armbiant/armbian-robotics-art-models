class EpgUrl {
  static const _ID_FIELD = 'id';
  static const _URL_FIELD = 'url';

  final String? id;
  String url;

  EpgUrl({required this.id, required this.url});

  EpgUrl.create({required this.url}) : id = null;

  EpgUrl.createDefault()
      : id = null,
        url = 'http://0.0.0.0/epg.xml';

  EpgUrl copy() {
    return EpgUrl(id: id, url: url);
  }

  bool isValid() {
    if (url.isEmpty) {
      return false;
    }
    final parsed = Uri.tryParse(url);
    return parsed != null;
  }

  factory EpgUrl.fromJson(Map<String, dynamic> json) {
    final id = json[_ID_FIELD];
    final url = json[_URL_FIELD];
    return EpgUrl(id: id, url: url);
  }

  Map<String, dynamic> toJson() {
    return {_ID_FIELD: id, _URL_FIELD: url};
  }
}

enum GpuModel {
  NVIDIA_A100,
  NVIDIA_A10,
  NVIDIA_T4,
  NVIDIA_V100,
}

extension GpuModelExt on GpuModel {
  static const Map<GpuModel, String> _text = {
    GpuModel.NVIDIA_A10: 'Nvidia A10',
    GpuModel.NVIDIA_A100: 'Nvidia A100',
    GpuModel.NVIDIA_T4: 'Nvidia T4',
    GpuModel.NVIDIA_V100: 'Nvidia V100',
  };

  String toHumanReadable() => _text[this]!;
}

enum AudioEffectType {
  DENOISER,
  DEREVERB,
  DEREVERB_DENOISER,
}

extension AudioEffectTypeExt on AudioEffectType {
  static const Map<AudioEffectType, String> _text = {
    AudioEffectType.DENOISER: 'Denoiser',
    AudioEffectType.DEREVERB: 'Dereverb',
    AudioEffectType.DEREVERB_DENOISER: 'Dereverb & denoiser',
  };

  String toHumanReadable() => _text[this]!;
}

class AudioStabilization {
  static const String MODEL_FIELD = "gpu_model";
  static const String TYPE_FILED = "type";

  final GpuModel model;
  final AudioEffectType type;

  const AudioStabilization({required this.model, required this.type});

  factory AudioStabilization.fromJson(Map<String, dynamic> json) {
    final modelIndex = json[MODEL_FIELD];
    final typeIndex = json[TYPE_FILED];

    return AudioStabilization(
      model: GpuModel.values[modelIndex],
      type: AudioEffectType.values[typeIndex],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      MODEL_FIELD: model.index,
      TYPE_FILED: type.index,
    };
  }

  AudioStabilization copyWith({
    GpuModel? model,
    AudioEffectType? type,
  }) {
    return AudioStabilization(
      model: model ?? this.model,
      type: type ?? this.type,
    );
  }
}

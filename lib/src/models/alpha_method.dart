class AlphaMethodType {
  static const int _SET = 0;
  static const int _GREEN = 1;
  static const int _BLUE = 2;
  static const int _CUSTOM = 3;

  final int _value;

  const AlphaMethodType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _SET) {
      return 'Set';
    } else if (_value == _GREEN) {
      return 'Green';
    } else if (_value == _BLUE) {
      return 'Blue';
    }

    assert(_value == _CUSTOM);
    return 'Custom';
  }

  factory AlphaMethodType.fromInt(int type) {
    if (type == _SET) {
      return AlphaMethodType.SET;
    } else if (type == _GREEN) {
      return AlphaMethodType.GREEN;
    } else if (type == _BLUE) {
      return AlphaMethodType.BLUE;
    } else if (type == _CUSTOM) {
      return AlphaMethodType.CUSTOM;
    }

    throw 'Unknown background type: $type';
  }

  static List<AlphaMethodType> get values => [SET, GREEN, BLUE, CUSTOM];

  static const AlphaMethodType SET = AlphaMethodType._(_SET);
  static const AlphaMethodType GREEN = AlphaMethodType._(_GREEN);
  static const AlphaMethodType BLUE = AlphaMethodType._(_BLUE);
  static const AlphaMethodType CUSTOM = AlphaMethodType._(_CUSTOM);
}

abstract class AlphaMethod {
  static const String ALPHA_FIELD = 'alpha';
  static const String METHOD_FIELD = 'method';

  final AlphaMethodType method;
  double? alpha;

  AlphaMethod(this.method, this.alpha);

  bool isValid();

  factory AlphaMethod.fromJson(Map<String, dynamic> json) {
    final AlphaMethodType method = AlphaMethodType.fromInt(json[METHOD_FIELD]);
    if (method == AlphaMethodType.CUSTOM) {
      return CustomAlphaMethod.fromJson(json);
    }

    return StableAlphaMethod.fromJson(method, json);
  }

  Map<String, dynamic> toJson() {
    return {METHOD_FIELD: method.toInt()};
  }
}

class StableAlphaMethod extends AlphaMethod {
  StableAlphaMethod(AlphaMethodType method, double? alpha) : super(method, alpha);

  StableAlphaMethod.createDefault() : super(AlphaMethodType.GREEN, 1.0);

  @override
  bool isValid() {
    return true;
  }

  factory StableAlphaMethod.fromJson(AlphaMethodType method, Map<String, dynamic> json) {
    double? alpha;
    if (json.containsKey(AlphaMethod.ALPHA_FIELD)) {
      alpha = json[AlphaMethod.ALPHA_FIELD];
    }
    return StableAlphaMethod(method, alpha);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (alpha != null) {
      result[AlphaMethod.ALPHA_FIELD] = alpha;
    }
    return result;
  }
}

class CustomAlphaMethod extends AlphaMethod {
  static const String COLOR_FIELD = 'color';

  CustomAlphaMethod([this.hex = 0xFF00FF00, alpha]) : super(AlphaMethodType.CUSTOM, alpha);

  int hex;

  @override
  bool isValid() {
    return true;
  }

  factory CustomAlphaMethod.fromJson(Map<String, dynamic> json) {
    return CustomAlphaMethod(json[COLOR_FIELD], json[AlphaMethod.ALPHA_FIELD]);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[COLOR_FIELD] = hex;
    if (alpha != null) {
      result[AlphaMethod.ALPHA_FIELD] = alpha;
    }
    return result;
  }
}

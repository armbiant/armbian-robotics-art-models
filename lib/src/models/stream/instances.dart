import 'package:fastocloud_dart_models/src/models/audio_stabilizaton.dart';
import 'package:fastocloud_dart_models/src/models/base.dart';
import 'package:fastocloud_dart_models/src/models/input_urls.dart';
import 'package:fastocloud_dart_models/src/models/ml.dart';
import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/stream/base.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastocloud_dart_models/src/models/vod_details.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/types.dart';

// proxy
class ProxyStream extends IStream {
  ProxyStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive);

  @override
  ProxyStream copy() {
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return ProxyStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        output: out);
  }

  factory ProxyStream.copyFrom(IStream stream) {
    final List<OutputUrl> out = [];
    for (final OutputUrl url in stream.output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in stream.meta) {
      copyMeta.add(met.copy());
    }
    return ProxyStream(
        id: stream.id,
        createdDate: stream.createdDate,
        name: stream.name,
        icon: stream.icon,
        epgId: stream.epgId,
        groups: stream.groups,
        price: stream.price,
        visible: stream.visible,
        iarc: stream.iarc,
        views: stream.views,
        meta: copyMeta,
        description: stream.description,
        location: stream.location,
        archive: stream.archive,
        output: out);
  }

  @override
  StreamType type() {
    return StreamType.PROXY;
  }

  @override
  bool isValid() {
    final _name = name.isValidStreamName();
    final _icon = icon?.isValidIconUrl() ?? true;
    final _iarc = iarc.isValidIARC();
    final _price = price.isValidPrice();
    final _output = output.isValidOutputUrls();
    return _name && _price && _icon && _output && _iarc;
  }
}

class VodProxyStream extends ProxyStream implements VodStream {
  final VodFields _vod;

  @override
  int get primeDate => _vod.primeDate;

  @override
  set primeDate(int date) {
    _vod.primeDate = date;
  }

  @override
  VodType get vodType => _vod.vodType;

  @override
  set vodType(VodType type) {
    _vod.vodType = type;
  }

  @override
  String get backgroundUrl => _vod.backgroundUrl;

  @override
  set backgroundUrl(String url) {
    _vod.backgroundUrl = url;
  }

  @override
  String get trailerUrl => _vod.trailerUrl;

  @override
  set trailerUrl(String url) {
    _vod.trailerUrl = url;
  }

  @override
  double get userScore => _vod.userScore;

  @override
  set userScore(double score) {
    _vod.userScore = score;
  }

  @override
  String get country => _vod.country;

  @override
  set country(String country) {
    _vod.country = country;
  }

  @override
  int get duration => _vod.duration;

  @override
  set duration(int duration) {
    _vod.duration = duration;
  }

  @override
  List<String> get directors => _vod.directors;

  @override
  set directors(List<String> directors) {
    _vod.directors = directors;
  }

  @override
  List<String> get cast => _vod.cast;

  @override
  set cast(List<String> cast) {
    _vod.cast = cast;
  }

  @override
  List<String> get production => _vod.production;

  @override
  set production(List<String> production) {
    _vod.production = production;
  }

  @override
  List<String> get genres => _vod.genres;

  @override
  set genres(List<String> genres) {
    _vod.genres = genres;
  }

  VodProxyStream(
      {String? id,
      int? createdDate,
      String name = StreamName.VOD,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      required int primeDate,
      VodType vodType = VodType.VOD,
      String backgroundUrl = DEFAULT_BACKGROUND_URL,
      String trailerUrl = INVALID_TRAILER_URL,
      double userScore = UserScore.DEFAULT,
      String country = Country.DEFAULT,
      int duration = VodDuration.DEFAULT,
      List<String> directors = const [],
      List<String> cast = const [],
      List<String> production = const [],
      List<String> genres = const []})
      : _vod = VodFields(
            primeDate: primeDate,
            vodType: vodType,
            backgroundUrl: backgroundUrl,
            trailerUrl: trailerUrl,
            userScore: userScore,
            country: country,
            duration: duration,
            directors: directors,
            cast: cast,
            production: production,
            genres: genres),
        super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive);

  @override
  bool isSerial() {
    return vodType == VodType.SERIAL;
  }

  @override
  VodProxyStream copy() {
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return VodProxyStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: output,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        production: production,
        genres: genres,
        cast: cast,
        directors: directors);
  }

  factory VodProxyStream.copyFrom(IStream stream, VodStream vod) {
    final List<OutputUrl> out = [];
    for (final OutputUrl url in stream.output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in stream.meta) {
      copyMeta.add(met.copy());
    }
    return VodProxyStream(
        id: stream.id,
        createdDate: stream.createdDate,
        name: stream.name,
        icon: stream.icon,
        epgId: stream.epgId,
        groups: stream.groups,
        price: stream.price,
        visible: stream.visible,
        iarc: stream.iarc,
        views: stream.views,
        meta: copyMeta,
        description: stream.description,
        location: stream.location,
        archive: stream.archive,
        output: out,
        primeDate: vod.primeDate,
        vodType: vod.vodType,
        backgroundUrl: vod.backgroundUrl,
        trailerUrl: vod.trailerUrl,
        userScore: vod.userScore,
        country: vod.country,
        duration: vod.duration,
        production: vod.production,
        genres: vod.genres,
        cast: vod.cast,
        directors: vod.directors);
  }

  @override
  StreamType type() {
    return StreamType.VOD_PROXY;
  }

  @override
  bool isValid() {
    return super.isValid() && duration.isValidVodDuration() && userScore.isValidUserScore();
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data.addAll(_vod.toJson());
    return data;
  }
}

// relay
class RelayStream extends HardwareStream {
  static const DEFAULT_VIDEO_PARSER = VideoParser.H264;
  static const DEFAULT_AUDIO_PARSER = AudioParser.AAC;

  static const VIDEO_PARSER_FIELD = 'video_parser';
  static const AUDIO_PARSER_FIELD = 'audio_parser';

  VideoParser? videoParser;
  AudioParser? audioParser;

  RelayStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      this.videoParser = VideoParser.H264,
      this.audioParser = AudioParser.AAC})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts);

  @override
  RelayStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return RelayStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser);
  }

  @override
  StreamType type() {
    return StreamType.RELAY;
  }

  @override
  bool isValid() {
    bool req = super.isValid() && input.isValidInputUrls();
    if (videoParser != null) {
      req &= VideoParser.values.contains(videoParser);
    }
    if (req && audioParser != null) {
      req &= AudioParser.values.contains(audioParser);
    }
    return req;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    if (videoParser != null) {
      data[VIDEO_PARSER_FIELD] = videoParser.toString();
    }
    if (audioParser != null) {
      data[AUDIO_PARSER_FIELD] = audioParser.toString();
    }
    return data;
  }
}

class ChangerRelayStream extends RelayStream {
  ChangerRelayStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  ChangerRelayStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return ChangerRelayStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser);
  }

  @override
  StreamType type() {
    return StreamType.CHANGER_RELAY;
  }
}

class CodRelayStream extends RelayStream {
  CodRelayStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  CodRelayStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return CodRelayStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser);
  }

  @override
  StreamType type() {
    return StreamType.COD_RELAY;
  }
}

class TestLifeStream extends RelayStream {
  TestLifeStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = false,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  TestLifeStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return TestLifeStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser);
  }

  @override
  StreamType type() {
    return StreamType.TEST_LIFE;
  }
}

class TimeshiftRecorderStream extends RelayStream {
  TimeshiftRecorderStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = false,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  TimeshiftRecorderStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return TimeshiftRecorderStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser);
  }

  @override
  StreamType type() {
    return StreamType.TIMESHIFT_RECORDER;
  }

  @override
  bool isValid() {
    final _name = name.isValidStreamName();
    final _icon = icon?.isValidIconUrl() ?? true;
    final _iarc = iarc.isValidIARC();
    final _price = price.isValidPrice();
    final _input = input.isValidInputUrls();
    final _rest = restartAttempts.isValidRestartAttempts();
    final _audioTracksCount = audioTracksCount.isValidAudioChannelsCount();
    bool req = _name && _price && _icon && _input && _iarc && _rest && _audioTracksCount;
    if (req && audioSelect != null) {
      req &= audioSelect!.isValidAudioSelect();
    }
    if (req && autoExit != null) {
      req &= autoExit!.ttl.isValidAutoExitTime();
    }
    if (videoParser != null) {
      req &= VideoParser.values.contains(videoParser);
    }
    if (req && audioParser != null) {
      req &= AudioParser.values.contains(audioParser);
    }
    return req;
  }
}

class TimeshiftPlayerStream extends RelayStream {
  static const TIMESHIFT_DIR_FIELD = 'timeshift_dir';
  static const TIMESHIFT_DELAY_FIELD = 'timeshift_delay';

  String timeshiftDir;
  int timeshiftDelay;

  TimeshiftPlayerStream(
      {String? id,
      required this.timeshiftDir,
      required this.timeshiftDelay,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  TimeshiftPlayerStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return TimeshiftPlayerStream(
        id: id,
        createdDate: createdDate,
        timeshiftDir: timeshiftDir,
        timeshiftDelay: timeshiftDelay,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser);
  }

  @override
  bool isValid() {
    final _output = output.isValidOutputUrls();
    final _input = input.isValidInputUrls();
    final _name = name.isValidStreamName();
    final _icon = icon?.isValidIconUrl() ?? true;
    final _iarc = iarc.isValidIARC();
    final _price = price.isValidPrice();
    final _rest = restartAttempts.isValidRestartAttempts();
    final _audioTracksCount = audioTracksCount.isValidAudioChannelsCount();
    bool req = _name &&
        _price &&
        _icon &&
        _iarc &&
        _rest &&
        _audioTracksCount &&
        _input &&
        _output &&
        timeshiftDir.isNotEmpty &&
        timeshiftDelay >= 0;
    if (req && audioSelect != null) {
      req &= audioSelect!.isValidAudioSelect();
    }
    if (req && autoExit != null) {
      req &= autoExit!.ttl.isValidAutoExitTime();
    }
    if (videoParser != null) {
      req &= VideoParser.values.contains(videoParser);
    }
    if (req && audioParser != null) {
      req &= AudioParser.values.contains(audioParser);
    }
    return req;
  }

  @override
  StreamType type() {
    return StreamType.TIMESHIFT_PLAYER;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[TIMESHIFT_DIR_FIELD] = timeshiftDir;
    data[TIMESHIFT_DELAY_FIELD] = timeshiftDelay;
    return data;
  }
}

class VodRelayStream extends RelayStream implements VodStream {
  final VodFields _vod;

  @override
  int get primeDate => _vod.primeDate;

  @override
  set primeDate(int date) {
    _vod.primeDate = date;
  }

  @override
  VodType get vodType => _vod.vodType;

  @override
  set vodType(VodType type) {
    _vod.vodType = type;
  }

  @override
  String get backgroundUrl => _vod.backgroundUrl;

  @override
  set backgroundUrl(String url) {
    _vod.backgroundUrl = url;
  }

  @override
  String get trailerUrl => _vod.trailerUrl;

  @override
  set trailerUrl(String url) {
    _vod.trailerUrl = url;
  }

  @override
  double get userScore => _vod.userScore;

  @override
  set userScore(double score) {
    _vod.userScore = score;
  }

  @override
  String get country => _vod.country;

  @override
  set country(String country) {
    _vod.country = country;
  }

  @override
  int get duration => _vod.duration;

  @override
  set duration(int duration) {
    _vod.duration = duration;
  }

  @override
  List<String> get directors => _vod.directors;

  @override
  set directors(List<String> directors) {
    _vod.directors = directors;
  }

  @override
  List<String> get cast => _vod.cast;

  @override
  set cast(List<String> cast) {
    _vod.cast = cast;
  }

  @override
  List<String> get production => _vod.production;

  @override
  set production(List<String> production) {
    _vod.production = production;
  }

  @override
  List<String> get genres => _vod.genres;

  @override
  set genres(List<String> genres) {
    _vod.genres = genres;
  }

  VodRelayStream(
      {String? id,
      int? createdDate,
      String name = StreamName.VOD,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC,
      required int primeDate,
      VodType vodType = VodType.VOD,
      String backgroundUrl = DEFAULT_BACKGROUND_URL,
      String trailerUrl = INVALID_TRAILER_URL,
      double userScore = UserScore.DEFAULT,
      String country = Country.DEFAULT,
      int duration = VodDuration.DEFAULT,
      List<String> directors = const [],
      List<String> cast = const [],
      List<String> production = const [],
      List<String> genres = const []})
      : _vod = VodFields(
            primeDate: primeDate,
            vodType: vodType,
            backgroundUrl: backgroundUrl,
            trailerUrl: trailerUrl,
            userScore: userScore,
            country: country,
            duration: duration,
            directors: directors,
            cast: cast,
            production: production,
            genres: genres),
        super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  VodRelayStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return VodRelayStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser,
        primeDate: primeDate,
        vodType: vodType,
        trailerUrl: trailerUrl,
        backgroundUrl: backgroundUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  @override
  StreamType type() {
    return StreamType.VOD_RELAY;
  }

  @override
  bool isSerial() {
    return vodType == VodType.SERIAL;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data.addAll(_vod.toJson());
    return data;
  }
}

class CatchupStream extends TimeshiftRecorderStream {
  static const START_FIELD = 'start';
  static const STOP_FIELD = 'stop';

  int? start;
  int? stop;

  CatchupStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = false,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      VideoParser? videoParser = VideoParser.H264,
      AudioParser? audioParser = AudioParser.AAC,
      this.start,
      this.stop})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            videoParser: videoParser,
            audioParser: audioParser);

  @override
  CatchupStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return CatchupStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        videoParser: videoParser,
        audioParser: audioParser,
        start: start,
        stop: stop);
  }

  @override
  StreamType type() {
    return StreamType.CATCHUP;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[START_FIELD] = start;
    data[STOP_FIELD] = stop;
    return data;
  }
}

// encode
class EncodeStream extends HardwareStream {
  static const RELAY_AUDIO_FIELD = 'relay_audio';
  static const RELAY_VIDEO_FIELD = 'relay_video';
  static const DEINTERLACE_FIELD = 'deinterlace';
  static const RESAMPLE_FIELD = 'resample';
  static const VOLUME_FIELD = 'volume';
  static const VIDEO_CODEC_FIELD = 'video_codec';
  static const AUDIO_CODEC_FIELD = 'audio_codec';

  // optional
  static const AUDIO_CHANNELS_COUNT_FIELD = 'audio_channels_count';
  static const FRAME_RATE_FIELD = 'frame_rate';
  static const SIZE_FIELD = 'size';
  static const MACHINE_LEARNING_FIELD = 'machine_learning';
  static const VIDEO_BITRATE_FIELD = 'video_bitrate';
  static const AUDIO_BITRATE_FIELD = 'audio_bitrate';
  static const ASPECT_RATIO_FIELD = 'aspect_ratio';
  static const LOGO_FIELD = 'logo';
  static const RSVG_LOGO_FIELD = 'rsvg_logo';
  static const BACKGROUND_EFFECT_FIELD = 'background_effect';
  static const TEXT_OVERLAY_FIELD = 'text_overlay';
  static const VIDEO_FLIP_FIELD = 'video_flip';
  static const STREAM_OVERLAY_FIELD = 'stream_overlay';
  static const AUDIO_STABILIZATION = 'audio_stabilization';

  bool relayVideo;
  bool relayAudio;
  VideoCodec videoCodec;
  AudioCodec audioCodec;

  bool? deinterlace;
  bool? resample;
  double? volume;
  int? audioChannelsCount;
  Rational? frameRate;
  Size? size;
  MachineLearning? machineLearning;
  int? videoBitRate;
  int? audioBitRate;
  Rational? aspectRatio;
  Logo? logo;
  RsvgLogo? rsvgLogo;
  BackgroundEffect? backgroundEffect;
  TextOverlay? textOverlay;
  int? videoFlip;
  StreamOverlay? streamOverlay;
  AudioStabilization? audioStabilization;

  EncodeStream({
    String? id,
    int? createdDate,
    String name = StreamName.DEFAULT,
    String? icon,
    String? epgId,
    List<String>? groups,
    double price = Price.DEFAULT,
    bool visible = true,
    bool archive = false,
    int iarc = IARC.DEFAULT,
    int views = 0,
    List<OutputUrl>? output,
    List<MetaUrl>? meta,
    String? description,
    LatLng? location,
    List<InputUrl>? input,
    StreamLogLevel logLevel = StreamLogLevel.INFO,
    String? feedbackDirectory,
    bool haveVideo = true,
    bool haveAudio = true,
    bool loop = false,
    int selectedInput = 0,
    int restartAttempts = RestartAttempts.DEFAULT,
    bool autoStart = false,
    int audioTracksCount = 1,
    StreamTTL? autoExit,
    ExtraConfig? extraConfig,
    int? audioSelect,
    List<NotificationStreamContact>? notificationContacts,
    this.deinterlace,
    this.resample,
    this.relayVideo = false,
    this.relayAudio = false,
    this.videoCodec = VideoCodec.DEFAULT,
    this.audioCodec = AudioCodec.DEFAULT,
    this.volume,
    this.audioChannelsCount,
    this.frameRate,
    this.size,
    this.machineLearning,
    this.videoBitRate,
    this.audioBitRate,
    this.aspectRatio,
    this.logo,
    this.rsvgLogo,
    this.backgroundEffect,
    this.textOverlay,
    this.videoFlip,
    this.streamOverlay,
    this.audioStabilization,
  }) : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts);

  @override
  EncodeStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return EncodeStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: out,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: inp,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  }

  @override
  StreamType type() {
    return StreamType.ENCODE;
  }

  @override
  bool isValid() {
    bool req = super.isValid();
    if (!req) {
      return false;
    }

    if (volume != null) {
      req &= volume!.isValidVolume();
    }
    if (audioChannelsCount != null) {
      req &= audioChannelsCount!.isValidAudioChannelsCount();
    }
    if (frameRate != null) {
      req &= frameRate!.isValid();
    }
    if (size != null) {
      req &= size!.isValid();
    }
    if (machineLearning != null) {
      req &= machineLearning!.isValid();
    }
    if (videoBitRate != null) {
      req &= videoBitRate!.isValidBitRate();
    }
    if (audioBitRate != null) {
      req &= audioBitRate!.isValidBitRate();
    }
    if (aspectRatio != null) {
      req &= aspectRatio!.isValid();
    }
    if (logo != null) {
      req &= logo!.isValid();
    }
    if (rsvgLogo != null) {
      req &= rsvgLogo!.isValid();
    }
    if (backgroundEffect != null) {
      req &= backgroundEffect!.isValid();
    }
    if (textOverlay != null) {
      req &= textOverlay!.isValid();
    }
    if (videoFlip != null) {
      req &= videoFlip!.isValidVideoFlip();
    }
    if (streamOverlay != null) {
      req &= streamOverlay!.isValidStreamUrl();
    }
    return req;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[RELAY_VIDEO_FIELD] = relayVideo;
    data[RELAY_AUDIO_FIELD] = relayAudio;
    data[VIDEO_CODEC_FIELD] = videoCodec.toString();
    data[AUDIO_CODEC_FIELD] = audioCodec.toString();

    if (deinterlace != null) {
      data[DEINTERLACE_FIELD] = deinterlace;
    }
    if (resample != null) {
      data[RESAMPLE_FIELD] = resample;
    }
    if (volume != null) {
      data[VOLUME_FIELD] = volume;
    }
    if (frameRate != null) {
      data[FRAME_RATE_FIELD] = frameRate;
    }
    if (audioChannelsCount != null) {
      data[AUDIO_CHANNELS_COUNT_FIELD] = audioChannelsCount;
    }
    if (size != null) {
      data[SIZE_FIELD] = size!.toJson();
    }
    if (machineLearning != null) {
      data[MACHINE_LEARNING_FIELD] = machineLearning!.toJson();
    }
    if (videoBitRate != null) {
      data[VIDEO_BITRATE_FIELD] = videoBitRate;
    }
    if (audioBitRate != null) {
      data[AUDIO_BITRATE_FIELD] = audioBitRate;
    }
    if (aspectRatio != null) {
      data[ASPECT_RATIO_FIELD] = aspectRatio!.toJson();
    }
    if (logo != null) {
      data[LOGO_FIELD] = logo!.toJson();
    }
    if (rsvgLogo != null) {
      data[RSVG_LOGO_FIELD] = rsvgLogo!.toJson();
    }
    if (backgroundEffect != null) {
      data[BACKGROUND_EFFECT_FIELD] = backgroundEffect!.toJson();
    }
    if (textOverlay != null) {
      data[TEXT_OVERLAY_FIELD] = textOverlay!.toJson();
    }
    if (videoFlip != null) {
      data[VIDEO_FLIP_FIELD] = videoFlip;
    }
    if (streamOverlay != null) {
      data[STREAM_OVERLAY_FIELD] = streamOverlay;
    }
    if (audioStabilization != null) {
      data[AUDIO_STABILIZATION] = audioStabilization!.toJson();
    }
    return data;
  }
}

class ChangerEncodeStream extends EncodeStream {
  ChangerEncodeStream({
    String? id,
    int? createdDate,
    String name = StreamName.DEFAULT,
    String? icon,
    String? epgId,
    List<String>? groups,
    double price = Price.DEFAULT,
    bool visible = true,
    bool archive = false,
    int iarc = IARC.DEFAULT,
    int views = 0,
    List<OutputUrl>? output,
    List<MetaUrl>? meta,
    String? description,
    LatLng? location,
    List<InputUrl>? input,
    StreamLogLevel logLevel = StreamLogLevel.INFO,
    String? feedbackDirectory,
    bool haveVideo = true,
    bool haveAudio = true,
    bool loop = false,
    int selectedInput = 0,
    int restartAttempts = RestartAttempts.DEFAULT,
    bool autoStart = false,
    int audioTracksCount = 1,
    bool relayVideo = false,
    bool relayAudio = false,
    StreamTTL? autoExit,
    ExtraConfig? extraConfig,
    int? audioSelect,
    List<NotificationStreamContact>? notificationContacts,
    bool? deinterlace,
    bool? resample,
    double? volume,
    VideoCodec videoCodec = VideoCodec.DEFAULT,
    AudioCodec audioCodec = AudioCodec.DEFAULT,
    int? audioChannelsCount,
    Rational? frameRate,
    Size? size,
    MachineLearning? machineLearning,
    int? videoBitRate,
    int? audioBitRate,
    Rational? aspectRatio,
    Logo? logo,
    RsvgLogo? rsvgLogo,
    BackgroundEffect? backgroundEffect,
    TextOverlay? textOverlay,
    int? videoFlip,
    StreamOverlay? streamOverlay,
    AudioStabilization? audioStabilization,
  }) : super(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: meta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          relayVideo: relayVideo,
          relayAudio: relayAudio,
          deinterlace: deinterlace,
          resample: resample,
          volume: volume,
          videoCodec: videoCodec,
          audioCodec: audioCodec,
          audioChannelsCount: audioChannelsCount,
          frameRate: frameRate,
          size: size,
          machineLearning: machineLearning,
          videoBitRate: videoBitRate,
          audioBitRate: audioBitRate,
          aspectRatio: aspectRatio,
          logo: logo,
          rsvgLogo: rsvgLogo,
          backgroundEffect: backgroundEffect,
          textOverlay: textOverlay,
          videoFlip: videoFlip,
          streamOverlay: streamOverlay,
          audioStabilization: audioStabilization,
        );

  @override
  ChangerEncodeStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return ChangerEncodeStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: out,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: inp,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  }

  @override
  StreamType type() {
    return StreamType.CHANGER_ENCODE;
  }
}

class CodEncodeStream extends EncodeStream {
  CodEncodeStream({
    String? id,
    int? createdDate,
    String name = StreamName.DEFAULT,
    String? icon,
    String? epgId,
    List<String>? groups,
    double price = Price.DEFAULT,
    bool visible = true,
    bool archive = false,
    int iarc = IARC.DEFAULT,
    int views = 0,
    List<OutputUrl>? output,
    List<MetaUrl>? meta,
    String? description,
    LatLng? location,
    List<InputUrl>? input,
    StreamLogLevel logLevel = StreamLogLevel.INFO,
    String? feedbackDirectory,
    bool haveVideo = true,
    bool haveAudio = true,
    bool loop = false,
    int selectedInput = 0,
    int restartAttempts = RestartAttempts.DEFAULT,
    bool autoStart = false,
    int audioTracksCount = 1,
    StreamTTL? autoExit,
    ExtraConfig? extraConfig,
    int? audioSelect,
    List<NotificationStreamContact>? notificationContacts,
    bool? deinterlace,
    bool? resample,
    bool relayVideo = false,
    bool relayAudio = false,
    double? volume,
    VideoCodec videoCodec = VideoCodec.DEFAULT,
    AudioCodec audioCodec = AudioCodec.DEFAULT,
    int? audioChannelsCount,
    Rational? frameRate,
    Size? size,
    MachineLearning? machineLearning,
    int? videoBitRate,
    int? audioBitRate,
    Rational? aspectRatio,
    Logo? logo,
    RsvgLogo? rsvgLogo,
    BackgroundEffect? backgroundEffect,
    TextOverlay? textOverlay,
    int? videoFlip,
    StreamOverlay? streamOverlay,
    AudioStabilization? audioStabilization,
  }) : super(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: meta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          relayVideo: relayVideo,
          relayAudio: relayAudio,
          deinterlace: deinterlace,
          resample: resample,
          volume: volume,
          videoCodec: videoCodec,
          audioCodec: audioCodec,
          audioChannelsCount: audioChannelsCount,
          frameRate: frameRate,
          size: size,
          machineLearning: machineLearning,
          videoBitRate: videoBitRate,
          audioBitRate: audioBitRate,
          aspectRatio: aspectRatio,
          logo: logo,
          rsvgLogo: rsvgLogo,
          backgroundEffect: backgroundEffect,
          textOverlay: textOverlay,
          videoFlip: videoFlip,
          streamOverlay: streamOverlay,
          audioStabilization: audioStabilization,
        );

  @override
  CodEncodeStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return CodEncodeStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: out,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: inp,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  }

  @override
  StreamType type() {
    return StreamType.COD_ENCODE;
  }
}

class CvDataStream extends EncodeStream {
  CvDataStream({
    String? id,
    int? createdDate,
    String name = StreamName.DEFAULT,
    String? icon,
    String? epgId,
    List<String>? groups,
    double price = Price.DEFAULT,
    bool visible = true,
    bool archive = false,
    int iarc = IARC.DEFAULT,
    int views = 0,
    List<OutputUrl>? output,
    List<MetaUrl>? meta,
    String? description,
    LatLng? location,
    List<InputUrl>? input,
    StreamLogLevel logLevel = StreamLogLevel.INFO,
    String? feedbackDirectory,
    bool haveVideo = true,
    bool haveAudio = true,
    bool loop = false,
    int selectedInput = 0,
    int restartAttempts = RestartAttempts.DEFAULT,
    bool autoStart = false,
    int audioTracksCount = 1,
    bool relayVideo = false,
    bool relayAudio = false,
    StreamTTL? autoExit,
    ExtraConfig? extraConfig,
    int? audioSelect,
    List<NotificationStreamContact>? notificationContacts,
    bool? deinterlace,
    bool? resample,
    double? volume,
    VideoCodec videoCodec = VideoCodec.DEFAULT,
    AudioCodec audioCodec = AudioCodec.DEFAULT,
    int? audioChannelsCount,
    Rational? frameRate,
    Size? size,
    MachineLearning? machineLearning,
    int? videoBitRate,
    int? audioBitRate,
    Rational? aspectRatio,
    Logo? logo,
    RsvgLogo? rsvgLogo,
    BackgroundEffect? backgroundEffect,
    TextOverlay? textOverlay,
    int? videoFlip,
    StreamOverlay? streamOverlay,
    AudioStabilization? audioStabilization,
  }) : super(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: meta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          relayVideo: relayVideo,
          relayAudio: relayAudio,
          deinterlace: deinterlace,
          resample: resample,
          volume: volume,
          videoCodec: videoCodec,
          audioCodec: audioCodec,
          audioChannelsCount: audioChannelsCount,
          frameRate: frameRate,
          size: size,
          machineLearning: machineLearning,
          videoBitRate: videoBitRate,
          audioBitRate: audioBitRate,
          aspectRatio: aspectRatio,
          logo: logo,
          rsvgLogo: rsvgLogo,
          backgroundEffect: backgroundEffect,
          textOverlay: textOverlay,
          videoFlip: videoFlip,
          streamOverlay: streamOverlay,
          audioStabilization: audioStabilization,
        );

  @override
  CvDataStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return CvDataStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: out,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: inp,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  }

  @override
  StreamType type() {
    return StreamType.CV_DATA;
  }
}

class VodEncodeStream extends EncodeStream implements VodStream {
  final VodFields _vod;

  @override
  int get primeDate => _vod.primeDate;

  @override
  set primeDate(int date) {
    _vod.primeDate = date;
  }

  @override
  VodType get vodType => _vod.vodType;

  @override
  set vodType(VodType type) {
    _vod.vodType = type;
  }

  @override
  String get backgroundUrl => _vod.backgroundUrl;

  @override
  set backgroundUrl(String url) {
    _vod.backgroundUrl = url;
  }

  @override
  String get trailerUrl => _vod.trailerUrl;

  @override
  set trailerUrl(String url) {
    _vod.trailerUrl = url;
  }

  @override
  double get userScore => _vod.userScore;

  @override
  set userScore(double score) {
    _vod.userScore = score;
  }

  @override
  String get country => _vod.country;

  @override
  set country(String country) {
    _vod.country = country;
  }

  @override
  int get duration => _vod.duration;

  @override
  set duration(int duration) {
    _vod.duration = duration;
  }

  @override
  List<String> get directors => _vod.directors;

  @override
  set directors(List<String> directors) {
    _vod.directors = directors;
  }

  @override
  List<String> get cast => _vod.cast;

  @override
  set cast(List<String> cast) {
    _vod.cast = cast;
  }

  @override
  List<String> get production => _vod.production;

  @override
  set production(List<String> production) {
    _vod.production = production;
  }

  @override
  List<String> get genres => _vod.genres;

  @override
  set genres(List<String> genres) {
    _vod.genres = genres;
  }

  VodEncodeStream(
      {String? id,
      int? createdDate,
      String name = StreamName.VOD,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      bool relayVideo = false,
      bool relayAudio = false,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      bool? deinterlace,
      bool? resample,
      double? volume,
      VideoCodec videoCodec = VideoCodec.DEFAULT,
      AudioCodec audioCodec = AudioCodec.DEFAULT,
      int? audioChannelsCount,
      Rational? frameRate,
      Size? size,
      MachineLearning? machineLearning,
      int? videoBitRate,
      int? audioBitRate,
      Rational? aspectRatio,
      Logo? logo,
      RsvgLogo? rsvgLogo,
      BackgroundEffect? backgroundEffect,
      TextOverlay? textOverlay,
      int? videoFlip,
      StreamOverlay? streamOverlay,
      AudioStabilization? audioStabilization,
      required int primeDate,
      VodType vodType = VodType.VOD,
      String backgroundUrl = DEFAULT_BACKGROUND_URL,
      String trailerUrl = INVALID_TRAILER_URL,
      double userScore = UserScore.DEFAULT,
      String country = Country.DEFAULT,
      int duration = VodDuration.DEFAULT,
      List<String> directors = const [],
      List<String> cast = const [],
      List<String> production = const [],
      List<String> genres = const []})
      : _vod = VodFields(
            primeDate: primeDate,
            vodType: vodType,
            backgroundUrl: backgroundUrl,
            trailerUrl: trailerUrl,
            userScore: userScore,
            country: country,
            duration: duration,
            directors: directors,
            cast: cast,
            production: production,
            genres: genres),
        super(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: meta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          relayVideo: relayVideo,
          relayAudio: relayAudio,
          deinterlace: deinterlace,
          resample: resample,
          volume: volume,
          videoCodec: videoCodec,
          audioCodec: audioCodec,
          audioChannelsCount: audioChannelsCount,
          frameRate: frameRate,
          size: size,
          machineLearning: machineLearning,
          videoBitRate: videoBitRate,
          audioBitRate: audioBitRate,
          aspectRatio: aspectRatio,
          logo: logo,
          rsvgLogo: rsvgLogo,
          backgroundEffect: backgroundEffect,
          textOverlay: textOverlay,
          videoFlip: videoFlip,
          streamOverlay: streamOverlay,
          audioStabilization: audioStabilization,
        );

  @override
  bool isSerial() {
    return vodType == VodType.SERIAL;
  }

  @override
  VodEncodeStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return VodEncodeStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        relayVideo: relayVideo,
        relayAudio: relayAudio,
        deinterlace: deinterlace,
        resample: resample,
        volume: volume,
        videoCodec: videoCodec,
        audioCodec: audioCodec,
        audioChannelsCount: audioChannelsCount,
        frameRate: frameRate,
        size: size,
        machineLearning: machineLearning,
        videoBitRate: videoBitRate,
        audioBitRate: audioBitRate,
        aspectRatio: aspectRatio,
        logo: logo,
        rsvgLogo: rsvgLogo,
        backgroundEffect: backgroundEffect,
        textOverlay: textOverlay,
        videoFlip: videoFlip,
        streamOverlay: streamOverlay,
        audioStabilization: audioStabilization,
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  @override
  StreamType type() {
    return StreamType.VOD_ENCODE;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data.addAll(_vod.toJson());
    return data;
  }
}

class EventStream extends VodEncodeStream {
  EventStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      StreamLogLevel logLevel = StreamLogLevel.INFO,
      String? feedbackDirectory,
      bool haveVideo = true,
      bool haveAudio = true,
      bool loop = false,
      int selectedInput = 0,
      int restartAttempts = RestartAttempts.DEFAULT,
      bool autoStart = false,
      int audioTracksCount = 1,
      bool relayVideo = false,
      bool relayAudio = false,
      StreamTTL? autoExit,
      ExtraConfig? extraConfig,
      int? audioSelect,
      List<NotificationStreamContact>? notificationContacts,
      bool? deinterlace,
      bool? resample,
      double? volume,
      VideoCodec videoCodec = VideoCodec.DEFAULT,
      AudioCodec audioCodec = AudioCodec.DEFAULT,
      int? audioChannelsCount,
      Rational? frameRate,
      Size? size,
      MachineLearning? machineLearning,
      int? videoBitRate,
      int? audioBitRate,
      Rational? aspectRatio,
      Logo? logo,
      RsvgLogo? rsvgLogo,
      BackgroundEffect? backgroundEffect,
      TextOverlay? textOverlay,
      StreamOverlay? streamOverlay,
      AudioStabilization? audioStabilization,
      int? videoFlip,
      required int primeDate,
      VodType vodType = VodType.VOD,
      String trailerUrl = INVALID_TRAILER_URL,
      String backgroundUrl = DEFAULT_BACKGROUND_URL,
      double userScore = UserScore.DEFAULT,
      String country = Country.DEFAULT,
      int duration = VodDuration.DEFAULT,
      List<String> directors = const [],
      List<String> cast = const [],
      List<String> production = const [],
      List<String> genres = const []})
      : super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive,
            input: input,
            logLevel: logLevel,
            feedbackDirectory: feedbackDirectory,
            haveVideo: haveVideo,
            haveAudio: haveAudio,
            loop: loop,
            selectedInput: selectedInput,
            restartAttempts: restartAttempts,
            autoStart: autoStart,
            audioTracksCount: audioTracksCount,
            autoExit: autoExit,
            extraConfig: extraConfig,
            audioSelect: audioSelect,
            notificationContacts: notificationContacts,
            relayVideo: relayVideo,
            relayAudio: relayAudio,
            deinterlace: deinterlace,
            resample: resample,
            volume: volume,
            videoCodec: videoCodec,
            audioCodec: audioCodec,
            audioChannelsCount: audioChannelsCount,
            frameRate: frameRate,
            size: size,
            machineLearning: machineLearning,
            videoBitRate: videoBitRate,
            audioBitRate: audioBitRate,
            aspectRatio: aspectRatio,
            logo: logo,
            rsvgLogo: rsvgLogo,
            backgroundEffect: backgroundEffect,
            textOverlay: textOverlay,
            videoFlip: videoFlip,
            streamOverlay: streamOverlay,
            audioStabilization: audioStabilization,
            primeDate: primeDate,
            vodType: vodType,
            backgroundUrl: backgroundUrl,
            trailerUrl: trailerUrl,
            userScore: userScore,
            country: country,
            duration: duration,
            directors: directors,
            production: production,
            cast: cast,
            genres: genres);

  @override
  EventStream copy() {
    final List<InputUrl> inp = [];
    for (final InputUrl url in input) {
      inp.add(url.copy());
    }
    final List<OutputUrl> out = [];
    for (final OutputUrl url in output) {
      out.add(url.copy());
    }
    final List<MetaUrl> copyMeta = [];
    for (final MetaUrl met in meta) {
      copyMeta.add(met.copy());
    }
    return EventStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: out,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: inp,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        relayVideo: relayVideo,
        relayAudio: relayAudio,
        deinterlace: deinterlace,
        resample: resample,
        volume: volume,
        videoCodec: videoCodec,
        audioCodec: audioCodec,
        audioChannelsCount: audioChannelsCount,
        frameRate: frameRate,
        size: size,
        machineLearning: machineLearning,
        videoBitRate: videoBitRate,
        audioBitRate: audioBitRate,
        aspectRatio: aspectRatio,
        logo: logo,
        rsvgLogo: rsvgLogo,
        backgroundEffect: backgroundEffect,
        textOverlay: textOverlay,
        streamOverlay: streamOverlay,
        audioStabilization: audioStabilization,
        videoFlip: videoFlip,
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  @override
  StreamType type() {
    return StreamType.EVENT;
  }
}

import 'package:fastocloud_dart_models/src/models/base.dart';
import 'package:fastocloud_dart_models/src/models/input_urls.dart';
import 'package:fastocloud_dart_models/src/models/output_urls.dart';
import 'package:fastocloud_dart_models/src/models/stream/stream_stats.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastocloud_dart_models/src/utils/fix_num.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/types.dart';

abstract class IStream {
  static const NAME_FIELD = 'name';
  static const ID_FIELD = 'id';
  static const TYPE_FIELD = 'type';
  static const ICON_FIELD = 'tvg_logo';
  static const GROUPS_FIELD = 'groups';
  static const PRICE_FIELD = 'price';
  static const VISIBLE_FIELD = 'visible';
  static const IARC_FIELD = 'iarc';
  static const VIEW_COUNT_FIELD = 'view_count';
  static const OUTPUT_FIELD = 'output';
  static const EPG_ID_FIELD = 'tvg_id';
  static const META_FIELD = 'meta';
  static const DESCRIPTION_FIELD = 'description';
  static const ARCHIVE_FIELD = 'archive';

  static const LOCATION_FIELD = 'location';
  static const CREATED_DATE_FIELD = 'created_date';

  // TVG_NAME_FIELD = 'tvg_name'

  // server
  String? id;
  final int? createdDate;

  // required
  String name;
  double price;
  bool visible;
  bool archive;
  int iarc;
  int views;
  List<OutputUrl> output;

  // blank
  List<String> groups;
  List<MetaUrl> meta;

  // optional
  String? epgId;
  String? icon;
  String? description;
  LatLng? location;

  IStream(
      {this.id,
      this.createdDate,
      this.name = StreamName.DEFAULT,
      this.icon,
      this.epgId,
      List<String>? groups,
      this.price = Price.DEFAULT,
      this.visible = true,
      this.archive = false,
      this.iarc = IARC.DEFAULT,
      this.views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      this.description,
      this.location})
      : groups = groups ?? [],
        meta = meta ?? [],
        output = output ?? [];

  IStream copy();

  IStream copyWith({required String? id, required String name}) {
    final other = copy();
    other.id = id;
    other.name = name;
    return other;
  }

  bool isSerial() {
    return false;
  }

  StreamType type();

  bool isVod() {
    final tp = type();
    return isVodStreamType(tp);
  }

  bool isValid();

  Map<String, dynamic> toJson() {
    final List<Map<String, dynamic>> _output = [];
    for (final out in output) {
      _output.add(out.toJson());
    }
    final List<Map<String, dynamic>> _meta = [];
    for (final out in meta) {
      _meta.add(out.toJson());
    }
    final Map<String, dynamic> result = {
      NAME_FIELD: name,
      ICON_FIELD: icon,
      GROUPS_FIELD: groups,
      TYPE_FIELD: type().toInt(),
      PRICE_FIELD: price,
      VIEW_COUNT_FIELD: views,
      OUTPUT_FIELD: _output,
      VISIBLE_FIELD: visible,
      IARC_FIELD: iarc,
      EPG_ID_FIELD: epgId,
      META_FIELD: _meta,
      DESCRIPTION_FIELD: description,
      ARCHIVE_FIELD: archive
    };
    if (id != null) {
      result[ID_FIELD] = id;
    }
    if (createdDate != null) {
      result[CREATED_DATE_FIELD] = createdDate;
    }
    if (location != null) {
      result[LOCATION_FIELD] = location!.toJson();
    }
    return result;
  }
}

abstract class HardwareStream extends IStream {
  static const INPUT_FIELD = 'input';
  static const LOG_LEVEL_FIELD = 'log_level';
  static const HAVE_VIDEO_FIELD = 'have_video';
  static const HAVE_AUDIO_FIELD = 'have_audio';
  static const LOOP_FIELD = 'loop';
  static const SELECTED_INPUT_FILED = 'selected_input';
  static const RESTART_ATTEMPTS_FIELD = 'restart_attempts';
  static const AUTO_START_FIELD = 'auto_start';
  static const AUDIO_TRACKS_COUNT_FIELD = 'audio_tracks_count';

  // optional
  static const AUDIO_SELECT_FIELD = 'audio_select';
  static const AUTO_EXIT_TIME_FIELD = 'auto_exit_time';
  static const EXTRA_CONFIG_FIELD = 'extra_config';
  static const NOTIFICATION_FIELD = 'notification';

  // dynamic
  static const FEEDBACK_DIR_FIELD = 'feedback_directory';
  static const RUNTIME_FIELD = 'runtime';

  List<InputUrl> input;
  StreamLogLevel logLevel;
  String? feedbackDirectory;
  bool haveVideo;
  bool haveAudio;
  bool loop;
  int selectedInput;
  int restartAttempts;
  bool autoStart;
  int audioTracksCount;

  StreamTTL? autoExit;
  int? audioSelect;
  ExtraConfig? extraConfig;

  // dynamic optional
  StreamRuntimeStats? stats;
  List<NotificationStreamContact>? notificationContacts;

  HardwareStream(
      {String? id,
      int? createdDate,
      String name = StreamName.DEFAULT,
      String? icon,
      String? epgId,
      List<String>? groups,
      double price = Price.DEFAULT,
      bool visible = true,
      bool archive = false,
      int iarc = IARC.DEFAULT,
      int views = 0,
      List<OutputUrl>? output,
      List<MetaUrl>? meta,
      String? description,
      LatLng? location,
      List<InputUrl>? input,
      this.logLevel = StreamLogLevel.INFO,
      this.feedbackDirectory = '',
      this.haveVideo = true,
      this.haveAudio = true,
      this.loop = false,
      this.selectedInput = 0,
      this.restartAttempts = RestartAttempts.DEFAULT,
      this.autoStart = false,
      this.audioTracksCount = 1,
      this.autoExit,
      this.extraConfig,
      this.notificationContacts,
      this.audioSelect})
      : input = input ?? [],
        super(
            id: id,
            createdDate: createdDate,
            name: name,
            icon: icon,
            epgId: epgId,
            groups: groups,
            price: price,
            visible: visible,
            iarc: iarc,
            views: views,
            output: output,
            meta: meta,
            description: description,
            location: location,
            archive: archive);

  double rssInMegabytes() {
    if (stats == null) {
      return 0.0;
    }

    final double inMegabytes = stats!.rss / (1024 * 1024);
    return fixedDouble(inMegabytes);
  }

  double fixedCpu() {
    if (stats == null) {
      return 0.0;
    }

    return fixedDouble(stats!.cpu);
  }

  double fixedQuality() {
    if (stats == null) {
      return 0.0;
    }

    return fixedDouble(stats!.quality);
  }

  double fixedStartTime() {
    if (stats == null) {
      return 0.0;
    }

    return fixedDouble((stats!.timestamp - stats!.startTime) / 1000);
  }

  double fixedLoopTime() {
    if (stats == null) {
      return 0.0;
    }

    return fixedDouble((stats!.timestamp - stats!.loopStartTime) / 1000);
  }

  double fixedInputMBitsPerSecond() {
    final double inMegabits = 8 * inputBitsPerSecond() / (1024 * 1024);
    return fixedDouble(inMegabits);
  }

  int inputBitsPerSecond() {
    if (stats == null) {
      return 0;
    }

    int result = 0;
    for (final ChannelStats stat in stats!.inputStreams) {
      result += stat.bps;
    }
    return result;
  }

  double fixedOutputMBitsPerSecond() {
    final double outMegabits = 8 * outputBitsPerSecond() / (1024 * 1024);
    return fixedDouble(outMegabits);
  }

  int outputBitsPerSecond() {
    if (stats == null) {
      return 0;
    }

    int result = 0;
    for (final ChannelStats stat in stats!.outputStreams) {
      result += stat.bps;
    }
    return result;
  }

  void setRuntime({required StreamRuntimeStats stats}) {
    this.stats = stats;
  }

  @override
  bool isValid() {
    final _name = name.isValidStreamName();
    final _icon = icon?.isValidIconUrl() ?? true;
    final _iarc = iarc.isValidIARC();
    final _price = price.isValidPrice();
    final _input = input.isValidInputUrls();
    final _output = output.isValidOutputUrls();
    final _rest = restartAttempts.isValidRestartAttempts();
    final _audioTracksCount = audioTracksCount.isValidAudioChannelsCount();
    bool req = _name && _price && _icon && _input && _output && _iarc && _rest && _audioTracksCount;
    if (req && audioSelect != null) {
      req &= audioSelect!.isValidAudioSelect();
    }
    if (req && autoExit != null) {
      req &= autoExit!.ttl.isValidAutoExitTime();
    }
    return req;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    final List<Map<String, dynamic>> _input = [];
    for (final inp in input) {
      _input.add(inp.toJson());
    }

    data[INPUT_FIELD] = _input;
    data[LOG_LEVEL_FIELD] = logLevel.toInt();
    data[FEEDBACK_DIR_FIELD] = feedbackDirectory;
    data[HAVE_VIDEO_FIELD] = haveVideo;
    data[HAVE_AUDIO_FIELD] = haveAudio;
    data[AUDIO_TRACKS_COUNT_FIELD] = audioTracksCount;
    if (audioSelect != null) {
      data[AUDIO_SELECT_FIELD] = audioSelect;
    }
    if (autoExit != null) {
      data[AUTO_EXIT_TIME_FIELD] = autoExit!.toJson();
    }
    if (extraConfig != null) {
      data[EXTRA_CONFIG_FIELD] = extraConfig;
    }
    data[LOOP_FIELD] = loop;
    data[SELECTED_INPUT_FILED] = selectedInput;
    data[RESTART_ATTEMPTS_FIELD] = restartAttempts;
    data[AUTO_START_FIELD] = autoStart;

    if (feedbackDirectory != null) {
      data[FEEDBACK_DIR_FIELD] = feedbackDirectory;
    }
    if (stats != null) {
      data[RUNTIME_FIELD] = stats!.toJson();
    }
    if (notificationContacts != null) {
      final List<Map<String, dynamic>> _notificationContacts = [];
      for (final notify in notificationContacts!) {
        _notificationContacts.add(notify.toJson());
      }
      data[NOTIFICATION_FIELD] = _notificationContacts;
    }

    return data;
  }
}

abstract class VodStream {
  const VodStream();

  String get name;

  int get primeDate;

  set primeDate(int date);

  VodType get vodType;

  set vodType(VodType type);

  String get backgroundUrl;

  set backgroundUrl(String url);

  String get trailerUrl;

  set trailerUrl(String url);

  double get userScore;

  set userScore(double score);

  String get country;

  set country(String country);

  int get duration;

  set duration(int duration);

  List<String> get directors;

  set directors(List<String> directors);

  List<String> get cast;

  set cast(List<String> actors);

  List<String> get production;

  set production(List<String> production);

  List<String> get genres;

  set genres(List<String> genres);
}

import 'package:fastocloud_dart_models/src/models/base.dart';

class StreamRuntimeStats {
  static const _STATUS_FIELD = 'status';
  static const _CPU_FIELD = 'cpu';
  static const _TIMESTAMP_FIELD = 'timestamp';
  static const _IDLE_TIME_FIELD = 'idle_time';
  static const _LOOP_START_TIME_FIELD = 'loop_start_time';
  static const _RSS_FIELD = 'rss';
  static const _RESTARTS_FIELD = 'restarts';
  static const _START_TIME_FIELD = 'start_time';
  static const _INPUT_STREAMS_FIELD = 'input_streams';
  static const _OUTPUT_STREAMS_FIELD = 'output_streams';
  static const _QUALITY_FIELD = 'quality';

  StreamStatus status = StreamStatus.NEW;
  double cpu = 0.0;
  int timestamp = 0;
  int idleTime = 0;
  int rss = 0;
  int loopStartTime = 0;
  int restarts = 0;
  int startTime = 0;
  List<ChannelStats> inputStreams = [];
  List<ChannelStats> outputStreams = [];
  double quality = 100.0;

  StreamRuntimeStats();

  factory StreamRuntimeStats.fromJson(Map<String, dynamic> json) {
    final StreamRuntimeStats result = StreamRuntimeStats();
    result.status = StreamStatus.fromInt(json[_STATUS_FIELD]);
    result.cpu = (json[_CPU_FIELD] as num).toDouble();
    result.timestamp = json[_TIMESTAMP_FIELD];
    result.idleTime = json[_IDLE_TIME_FIELD];
    result.rss = json[_RSS_FIELD];
    result.loopStartTime = json[_LOOP_START_TIME_FIELD];
    result.restarts = json[_RESTARTS_FIELD];
    result.startTime = json[_START_TIME_FIELD];
    json[_INPUT_STREAMS_FIELD].forEach((s) {
      result.inputStreams.add(ChannelStats.fromJson(s));
    });
    json[_OUTPUT_STREAMS_FIELD].forEach((s) {
      result.outputStreams.add(ChannelStats.fromJson(s));
    });
    result.quality = (json[_QUALITY_FIELD] as num).toDouble();
    return result;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data[_STATUS_FIELD] = status.toInt();
    data[_CPU_FIELD] = cpu;
    data[_TIMESTAMP_FIELD] = timestamp;
    data[_IDLE_TIME_FIELD] = idleTime;
    data[_RSS_FIELD] = rss;
    data[_LOOP_START_TIME_FIELD] = loopStartTime;
    data[_RESTARTS_FIELD] = restarts;
    data[_START_TIME_FIELD] = startTime;
    data[_INPUT_STREAMS_FIELD] = inputStreams;
    data[_OUTPUT_STREAMS_FIELD] = outputStreams;
    data[_QUALITY_FIELD] = quality;
    return data;
  }
}

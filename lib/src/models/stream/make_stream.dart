import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/meta_url.dart';
import 'package:fastotv_dart/commands_info/types.dart';

IStream? makeStream(Map<String, dynamic> json) {
  final IStream? stream = _makeStream(json);
  if (stream == null) {
    return null;
  }

  if (stream is HardwareStream && json.containsKey(HardwareStream.RUNTIME_FIELD)) {
    final stats = StreamRuntimeStats.fromJson(json[HardwareStream.RUNTIME_FIELD]);
    stream.setRuntime(stats: stats);
  }
  return stream;
}

IStream? _makeStream(Map<String, dynamic> json) {
  final id = json[IStream.ID_FIELD];
  final name = json[IStream.NAME_FIELD];
  final icon = json[IStream.ICON_FIELD];
  final List<String> groups = json[IStream.GROUPS_FIELD].cast<String>();
  final double price = (json[IStream.PRICE_FIELD] as num).toDouble();
  final visible = json[IStream.VISIBLE_FIELD];
  final iarc = json[IStream.IARC_FIELD];
  final views = json[IStream.VIEW_COUNT_FIELD];
  final epgId = json[IStream.EPG_ID_FIELD];
  final List<OutputUrl> output = [];
  json[IStream.OUTPUT_FIELD].forEach((element) => output.add(makeOutputUrl(element)));
  final List<MetaUrl> copyMeta = [];
  json[IStream.META_FIELD].forEach((element) => copyMeta.add(MetaUrl.fromJson(element)));
  final description = json[IStream.DESCRIPTION_FIELD];
  final archive = json[IStream.ARCHIVE_FIELD];
  int? createdDate;
  if (json.containsKey(IStream.CREATED_DATE_FIELD)) {
    createdDate = json[IStream.CREATED_DATE_FIELD];
  }
  LatLng? location;
  if (json.containsKey(IStream.LOCATION_FIELD)) {
    location = LatLng.fromJson(json[IStream.LOCATION_FIELD]);
  }

  late int primeDate;
  late VodType vodType;
  late String trailerUrl;
  late String backgroundUrl;
  late double userScore;
  late String country;
  late int duration;
  late List<String> directors;
  late List<String> cast;
  late List<String> production;
  late List<String> genres;

  final type = StreamType.fromInt(json[IStream.TYPE_FIELD]);

  if (type == StreamType.VOD_PROXY ||
      type == StreamType.VOD_RELAY ||
      type == StreamType.VOD_ENCODE ||
      type == StreamType.EVENT) {
    primeDate = json[VodFields.PRIME_DATE_FIELD];
    vodType = VodType.fromInt(json[VodFields.VOD_TYPE_FIELD]);
    backgroundUrl = json[VodFields.BACKGROUND_URL_FIELD];
    trailerUrl = json[VodFields.TRAILER_URL_FIELD];
    userScore = (json[VodFields.USER_SCORE_FIELD] as num).toDouble();
    country = json[VodFields.COUNTRY_FIELD];
    duration = json[VodFields.DURATION_FIELD];
    directors = json[VodFields.DIRECTORS_FIELD].cast<String>();
    cast = json[VodFields.CAST_FIELD].cast<String>();
    production = json[VodFields.PRODUCTION_FIELD].cast<String>();
    genres = json[VodFields.GENRES_FIELD].cast<String>();
  }

  if (type == StreamType.PROXY) {
    return ProxyStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        output: output);
  } else if (type == StreamType.VOD_PROXY) {
    return VodProxyStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: output,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  final List<InputUrl> input = [];
  final List<dynamic> result = json[HardwareStream.INPUT_FIELD];
  for (final element in result) {
    input.add(makeInputUrl(element));
  }
  final StreamLogLevel logLevel = StreamLogLevel.fromInt(json[HardwareStream.LOG_LEVEL_FIELD]);
  final String? feedbackDirectory = json[HardwareStream.FEEDBACK_DIR_FIELD];
  final bool haveVideo = json[HardwareStream.HAVE_VIDEO_FIELD];
  final bool haveAudio = json[HardwareStream.HAVE_AUDIO_FIELD];
  final bool loop = json[HardwareStream.LOOP_FIELD];
  final int selectedInput = json[HardwareStream.SELECTED_INPUT_FILED];
  final int restartAttempts = json[HardwareStream.RESTART_ATTEMPTS_FIELD];
  final bool autoStart = json[HardwareStream.AUTO_START_FIELD];
  final int audioTracksCount = json[HardwareStream.AUDIO_TRACKS_COUNT_FIELD];
  int? audioSelect;
  if (json.containsKey(HardwareStream.AUDIO_SELECT_FIELD)) {
    audioSelect = json[HardwareStream.AUDIO_SELECT_FIELD];
  }

  StreamTTL? autoExit;
  if (json.containsKey(HardwareStream.AUTO_EXIT_TIME_FIELD)) {
    autoExit = StreamTTL.fromJson(json[HardwareStream.AUTO_EXIT_TIME_FIELD]);
  }

  ExtraConfig? extraConfig;
  if (json.containsKey(HardwareStream.EXTRA_CONFIG_FIELD)) {
    extraConfig = json[HardwareStream.EXTRA_CONFIG_FIELD];
  }

  List<NotificationStreamContact>? notificationContacts;
  if (json.containsKey(HardwareStream.NOTIFICATION_FIELD)) {
    final List<dynamic> result = json[HardwareStream.NOTIFICATION_FIELD];
    final List<NotificationStreamContact> notifications = [];
    for (final element in result) {
      notifications.add(NotificationStreamContact.fromJson(element));
    }
    notificationContacts = notifications;
  }

  if (type == StreamType.RELAY ||
      type == StreamType.CHANGER_RELAY ||
      type == StreamType.TIMESHIFT_PLAYER ||
      type == StreamType.TIMESHIFT_RECORDER ||
      type == StreamType.CATCHUP ||
      type == StreamType.TEST_LIFE ||
      type == StreamType.VOD_RELAY ||
      type == StreamType.COD_RELAY) {
    VideoParser? videoParser;
    if (json.containsKey(RelayStream.VIDEO_PARSER_FIELD)) {
      videoParser = VideoParser.fromString(json[RelayStream.VIDEO_PARSER_FIELD]);
    }

    AudioParser? audioParser;
    if (json.containsKey(RelayStream.AUDIO_PARSER_FIELD)) {
      audioParser = AudioParser.fromString(json[RelayStream.AUDIO_PARSER_FIELD]);
    }

    if (type == StreamType.RELAY) {
      return RelayStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser);
    } else if (type == StreamType.CHANGER_RELAY) {
      return ChangerRelayStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser);
    } else if (type == StreamType.COD_RELAY) {
      return CodRelayStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser);
    } else if (type == StreamType.TEST_LIFE) {
      return TestLifeStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser);
    } else if (type == StreamType.TIMESHIFT_RECORDER) {
      return TimeshiftRecorderStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser);
    } else if (type == StreamType.TIMESHIFT_PLAYER) {
      final String timeshiftDir = json[TimeshiftPlayerStream.TIMESHIFT_DIR_FIELD];
      final int timeshiftDelay = json[TimeshiftPlayerStream.TIMESHIFT_DELAY_FIELD];
      return TimeshiftPlayerStream(
          timeshiftDelay: timeshiftDelay,
          timeshiftDir: timeshiftDir,
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser);
    } else if (type == StreamType.VOD_RELAY) {
      return VodRelayStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          archive: archive,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser,
          primeDate: primeDate,
          vodType: vodType,
          backgroundUrl: backgroundUrl,
          trailerUrl: trailerUrl,
          userScore: userScore,
          country: country,
          duration: duration,
          directors: directors,
          production: production,
          cast: cast,
          genres: genres);
    } else if (type == StreamType.CATCHUP) {
      final start = json[CatchupStream.START_FIELD];
      final stop = json[CatchupStream.STOP_FIELD];
      return CatchupStream(
          id: id,
          createdDate: createdDate,
          name: name,
          icon: icon,
          epgId: epgId,
          groups: groups,
          price: price,
          visible: visible,
          iarc: iarc,
          views: views,
          output: output,
          meta: copyMeta,
          description: description,
          location: location,
          input: input,
          logLevel: logLevel,
          feedbackDirectory: feedbackDirectory,
          haveVideo: haveVideo,
          haveAudio: haveAudio,
          loop: loop,
          selectedInput: selectedInput,
          restartAttempts: restartAttempts,
          autoStart: autoStart,
          audioTracksCount: audioTracksCount,
          autoExit: autoExit,
          extraConfig: extraConfig,
          audioSelect: audioSelect,
          notificationContacts: notificationContacts,
          videoParser: videoParser,
          audioParser: audioParser,
          start: start,
          stop: stop);
    }
  }

  final bool relayVideo = json[EncodeStream.RELAY_VIDEO_FIELD];
  final bool relayAudio = json[EncodeStream.RELAY_AUDIO_FIELD];
  final VideoCodec videoCodec = VideoCodec.fromString(json[EncodeStream.VIDEO_CODEC_FIELD]);
  final AudioCodec audioCodec = AudioCodec.fromString(json[EncodeStream.AUDIO_CODEC_FIELD]);

  // optional
  bool? deinterlace;
  if (json.containsKey(EncodeStream.DEINTERLACE_FIELD)) {
    deinterlace = json[EncodeStream.DEINTERLACE_FIELD];
  }

  bool? resample;
  if (json.containsKey(EncodeStream.RESAMPLE_FIELD)) {
    resample = json[EncodeStream.RESAMPLE_FIELD];
  }

  double? volume;
  if (json.containsKey(EncodeStream.VOLUME_FIELD)) {
    volume = (json[EncodeStream.VOLUME_FIELD] as num).toDouble();
  }

  Rational? frameRate;
  if (json.containsKey(EncodeStream.FRAME_RATE_FIELD)) {
    frameRate = Rational.fromJson(json[EncodeStream.FRAME_RATE_FIELD]);
  }
  final int? audioChannelsCount = json[EncodeStream.AUDIO_CHANNELS_COUNT_FIELD];
  final int? videoBitRate = json[EncodeStream.VIDEO_BITRATE_FIELD];
  final int? audioBitRate = json[EncodeStream.AUDIO_BITRATE_FIELD];
  Size? size;
  if (json.containsKey(EncodeStream.SIZE_FIELD)) {
    size = Size.fromJson(json[EncodeStream.SIZE_FIELD]);
  }
  MachineLearning? machineLearning;
  if (json.containsKey(EncodeStream.MACHINE_LEARNING_FIELD)) {
    machineLearning = MachineLearning.fromJson(json[EncodeStream.MACHINE_LEARNING_FIELD]);
  }
  Rational? aspectRatio;
  if (json.containsKey(EncodeStream.ASPECT_RATIO_FIELD)) {
    aspectRatio = Rational.fromJson(json[EncodeStream.ASPECT_RATIO_FIELD]);
  }
  Logo? logo;
  if (json.containsKey(EncodeStream.LOGO_FIELD)) {
    logo = Logo.fromJson(json[EncodeStream.LOGO_FIELD]);
  }
  RsvgLogo? rsvgLogo;
  if (json.containsKey(EncodeStream.RSVG_LOGO_FIELD)) {
    rsvgLogo = RsvgLogo.fromJson(json[EncodeStream.RSVG_LOGO_FIELD]);
  }
  TextOverlay? textOverlay;
  if (json.containsKey(EncodeStream.TEXT_OVERLAY_FIELD)) {
    textOverlay = TextOverlay.fromJson(json[EncodeStream.TEXT_OVERLAY_FIELD]);
  }
  int? videoFlip;
  if (json.containsKey(EncodeStream.VIDEO_FLIP_FIELD)) {
    videoFlip = json[EncodeStream.VIDEO_FLIP_FIELD];
  }
  StreamOverlay? streamOverlay;
  if (json.containsKey(EncodeStream.STREAM_OVERLAY_FIELD)) {
    streamOverlay = StreamOverlay.fromJson(json[EncodeStream.STREAM_OVERLAY_FIELD]);
  }
  BackgroundEffect? backgroundEffect;
  if (json.containsKey(EncodeStream.BACKGROUND_EFFECT_FIELD)) {
    backgroundEffect = BackgroundEffect.fromJson(json[EncodeStream.BACKGROUND_EFFECT_FIELD]);
  }
  AudioStabilization? audioStabilization;
  if (json.containsKey(EncodeStream.AUDIO_STABILIZATION)) {
    audioStabilization = AudioStabilization.fromJson(json[EncodeStream.AUDIO_STABILIZATION]);
  }

  if (type == StreamType.ENCODE) {
    return EncodeStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: output,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: input,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        relayVideo: relayVideo,
        relayAudio: relayAudio,
        deinterlace: deinterlace,
        resample: resample,
        volume: volume,
        videoCodec: videoCodec,
        audioCodec: audioCodec,
        audioChannelsCount: audioChannelsCount,
        frameRate: frameRate,
        size: size,
        machineLearning: machineLearning,
        videoBitRate: videoBitRate,
        audioBitRate: audioBitRate,
        aspectRatio: aspectRatio,
        logo: logo,
        rsvgLogo: rsvgLogo,
        backgroundEffect: backgroundEffect,
        textOverlay: textOverlay,
        videoFlip: videoFlip,
        streamOverlay: streamOverlay,
        audioStabilization: audioStabilization);
  } else if (type == StreamType.CHANGER_ENCODE) {
    return ChangerEncodeStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: output,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: input,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  } else if (type == StreamType.COD_ENCODE) {
    return CodEncodeStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: output,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: input,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  } else if (type == StreamType.CV_DATA) {
    return CvDataStream(
      id: id,
      createdDate: createdDate,
      name: name,
      icon: icon,
      epgId: epgId,
      groups: groups,
      price: price,
      visible: visible,
      iarc: iarc,
      views: views,
      output: output,
      meta: copyMeta,
      description: description,
      location: location,
      archive: archive,
      input: input,
      logLevel: logLevel,
      feedbackDirectory: feedbackDirectory,
      haveVideo: haveVideo,
      haveAudio: haveAudio,
      loop: loop,
      selectedInput: selectedInput,
      restartAttempts: restartAttempts,
      autoStart: autoStart,
      audioTracksCount: audioTracksCount,
      autoExit: autoExit,
      extraConfig: extraConfig,
      audioSelect: audioSelect,
      notificationContacts: notificationContacts,
      relayVideo: relayVideo,
      relayAudio: relayAudio,
      deinterlace: deinterlace,
      resample: resample,
      volume: volume,
      videoCodec: videoCodec,
      audioCodec: audioCodec,
      audioChannelsCount: audioChannelsCount,
      frameRate: frameRate,
      size: size,
      machineLearning: machineLearning,
      videoBitRate: videoBitRate,
      audioBitRate: audioBitRate,
      aspectRatio: aspectRatio,
      logo: logo,
      rsvgLogo: rsvgLogo,
      backgroundEffect: backgroundEffect,
      textOverlay: textOverlay,
      videoFlip: videoFlip,
      streamOverlay: streamOverlay,
      audioStabilization: audioStabilization,
    );
  } else if (type == StreamType.VOD_ENCODE) {
    return VodEncodeStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: output,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: input,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        relayVideo: relayVideo,
        relayAudio: relayAudio,
        deinterlace: deinterlace,
        resample: resample,
        volume: volume,
        videoCodec: videoCodec,
        audioCodec: audioCodec,
        audioChannelsCount: audioChannelsCount,
        frameRate: frameRate,
        size: size,
        machineLearning: machineLearning,
        videoBitRate: videoBitRate,
        audioBitRate: audioBitRate,
        aspectRatio: aspectRatio,
        logo: logo,
        rsvgLogo: rsvgLogo,
        backgroundEffect: backgroundEffect,
        textOverlay: textOverlay,
        videoFlip: videoFlip,
        streamOverlay: streamOverlay,
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        audioStabilization: audioStabilization,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  } else if (type == StreamType.EVENT) {
    return EventStream(
        id: id,
        createdDate: createdDate,
        name: name,
        icon: icon,
        epgId: epgId,
        groups: groups,
        price: price,
        visible: visible,
        iarc: iarc,
        views: views,
        output: output,
        meta: copyMeta,
        description: description,
        location: location,
        archive: archive,
        input: input,
        logLevel: logLevel,
        feedbackDirectory: feedbackDirectory,
        haveVideo: haveVideo,
        haveAudio: haveAudio,
        loop: loop,
        selectedInput: selectedInput,
        restartAttempts: restartAttempts,
        autoStart: autoStart,
        audioTracksCount: audioTracksCount,
        autoExit: autoExit,
        extraConfig: extraConfig,
        audioSelect: audioSelect,
        notificationContacts: notificationContacts,
        relayVideo: relayVideo,
        relayAudio: relayAudio,
        deinterlace: deinterlace,
        resample: resample,
        volume: volume,
        videoCodec: videoCodec,
        audioCodec: audioCodec,
        audioChannelsCount: audioChannelsCount,
        frameRate: frameRate,
        size: size,
        machineLearning: machineLearning,
        videoBitRate: videoBitRate,
        audioBitRate: audioBitRate,
        aspectRatio: aspectRatio,
        logo: logo,
        rsvgLogo: rsvgLogo,
        backgroundEffect: backgroundEffect,
        textOverlay: textOverlay,
        videoFlip: videoFlip,
        streamOverlay: streamOverlay,
        primeDate: primeDate,
        vodType: vodType,
        backgroundUrl: backgroundUrl,
        trailerUrl: trailerUrl,
        userScore: userScore,
        country: country,
        duration: duration,
        audioStabilization: audioStabilization,
        directors: directors,
        cast: cast,
        production: production,
        genres: genres);
  }

  return null;
}

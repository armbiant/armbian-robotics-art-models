import 'package:fastocloud_dart_models/src/models/input_urls.dart';
import 'package:fastocloud_dart_models/src/models/rtmp/rtmp_urls.dart';
import 'package:fastocloud_dart_models/src/models/types.dart';

// enums
class HlsType {
  static const int _HLS_PULL_CONSTANT = 0;
  static const int _HLS_PUSH_CONSTANT = 1;

  final int _value;

  const HlsType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _HLS_PULL_CONSTANT) {
      return 'HLS PULL';
    }

    assert(_value == _HLS_PUSH_CONSTANT);
    return 'HLS PUSH';
  }

  factory HlsType.fromInt(int type) {
    if (type == _HLS_PULL_CONSTANT) {
      return HlsType.HLS_PULL;
    } else if (type == _HLS_PUSH_CONSTANT) {
      return HlsType.HLS_PUSH;
    }

    throw 'Unknown hls type: $type';
  }

  static List<HlsType> get values => [HLS_PULL, HLS_PUSH];

  static const HlsType HLS_PULL = HlsType._(_HLS_PULL_CONSTANT);
  static const HlsType HLS_PUSH = HlsType._(_HLS_PUSH_CONSTANT);
}

class HlsSinkType {
  static const int _HLS_SINK_CONSTANT = 0;
  static const int _HLS_SINK2_CONSTANT = 1;

  final int _value;

  const HlsSinkType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _HLS_SINK_CONSTANT) {
      return 'HLSSINK';
    }

    assert(_value == _HLS_SINK2_CONSTANT);
    return 'HLSSINK2';
  }

  factory HlsSinkType.fromInt(int type) {
    if (type == _HLS_SINK_CONSTANT) {
      return HlsSinkType.HLSSINK;
    } else if (type == _HLS_SINK2_CONSTANT) {
      return HlsSinkType.HLSSINK2;
    }

    throw 'Unknown hls sink type: $type';
  }

  static List<HlsSinkType> get values => [HLSSINK, HLSSINK2];

  static const HlsSinkType HLSSINK = HlsSinkType._(_HLS_SINK_CONSTANT);
  static const HlsSinkType HLSSINK2 = HlsSinkType._(_HLS_SINK2_CONSTANT);
}

class RtmpSinkType {
  static const int _RTMP_SINK_CONSTANT = 0;
  static const int _RTMP2_SINK_CONSTANT = 1;

  final int _value;

  const RtmpSinkType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _RTMP_SINK_CONSTANT) {
      return 'RTMPSINK';
    }

    assert(_value == _RTMP2_SINK_CONSTANT);
    return 'RTMP2SINK';
  }

  factory RtmpSinkType.fromInt(int type) {
    if (type == _RTMP_SINK_CONSTANT) {
      return RtmpSinkType.RTMPSINK;
    } else if (type == _RTMP2_SINK_CONSTANT) {
      return RtmpSinkType.RTMP2SINK;
    }

    throw 'Unknown rtmp sink type: $type';
  }

  static List<RtmpSinkType> get values => [RTMPSINK, RTMP2SINK];

  static const RtmpSinkType RTMPSINK = RtmpSinkType._(_RTMP_SINK_CONSTANT);
  static const RtmpSinkType RTMP2SINK = RtmpSinkType._(_RTMP2_SINK_CONSTANT);
}

class AzureProp {
  static const _ACCOUNT_NAME_FIELD = 'account_name';
  static const _ACCOUNT_KEY_FIELD = 'account_key';
  static const _LOCATION_FIELD = 'location';

  String accountName;
  String accountKey;
  String location;

  AzureProp(this.accountName, this.accountKey, this.location);

  AzureProp.createDefault()
      : accountName = 'FastoCloud',
        accountKey = 'accessKey',
        location = 'container_name/blob_name';

  AzureProp copy() {
    return AzureProp(accountName, accountKey, location);
  }

  factory AzureProp.fromJson(Map<String, dynamic> json) {
    final String acc = json[_ACCOUNT_NAME_FIELD];
    final String key = json[_ACCOUNT_KEY_FIELD];
    final String loc = json[_LOCATION_FIELD];
    return AzureProp(acc, key, loc);
  }

  Map<String, dynamic> toJson() {
    return {
      _ACCOUNT_NAME_FIELD: accountName,
      _ACCOUNT_KEY_FIELD: accountKey,
      _LOCATION_FIELD: location
    };
  }
}

class GoogleProp {
  static const _ACCOUNT_CREDS_FIELD = 'account_creds';

  String accountCreds;

  GoogleProp(this.accountCreds);

  GoogleProp.createDefault() : accountCreds = 'Stringify content of json credentials file';

  GoogleProp copy() {
    return GoogleProp(accountCreds);
  }

  factory GoogleProp.fromJson(Map<String, dynamic> json) {
    final String acc = json[_ACCOUNT_CREDS_FIELD];
    return GoogleProp(acc);
  }

  Map<String, dynamic> toJson() {
    return {_ACCOUNT_CREDS_FIELD: accountCreds};
  }
}

class KVSProp {
  static const _STREAM_NAME_FIELD = 'stream_name';
  static const _ACCESS_KEY_FIELD = 'access_key';
  static const _SECRET_KEY_FIELD = 'secret_key';
  static const _AWS_REGION_FIELD = 'aws_region';
  static const _STORAGE_SIZE_FIELD = 'storage_size';

  String streamName;
  String accessKey;
  String secretKey;
  String awsRegion;
  int storageSize;

  KVSProp(this.streamName, this.accessKey, this.secretKey, this.awsRegion, this.storageSize);

  KVSProp.createDefault()
      : streamName = 'FastoCloud',
        accessKey = 'accessKey',
        secretKey = 'secretKey',
        awsRegion = 'awsRegion',
        storageSize = 512;

  KVSProp copy() {
    return KVSProp(streamName, accessKey, secretKey, awsRegion, storageSize);
  }

  factory KVSProp.fromJson(Map<String, dynamic> json) {
    final String stream = json[_STREAM_NAME_FIELD];
    final String access = json[_ACCESS_KEY_FIELD];
    final String sec = json[_SECRET_KEY_FIELD];
    final String reg = json[_AWS_REGION_FIELD];
    final int storage = json[_STORAGE_SIZE_FIELD];
    return KVSProp(stream, access, sec, reg, storage);
  }

  Map<String, dynamic> toJson() {
    return {
      _STREAM_NAME_FIELD: streamName,
      _ACCESS_KEY_FIELD: accessKey,
      _SECRET_KEY_FIELD: secretKey,
      _AWS_REGION_FIELD: awsRegion,
      _STORAGE_SIZE_FIELD: storageSize
    };
  }
}

class OutputUrl {
  static const ID_FIELD = 'id';
  static const URI_FIELD = 'uri';

  int id;
  String uri;

  OutputUrl({required this.id, required this.uri});

  OutputUrl.createInvalid({required this.id}) : uri = '';

  OutputUrl copy() {
    return OutputUrl(id: id, uri: uri);
  }

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, URI_FIELD: uri};
  }

  bool isValid() {
    if (uri.isEmpty) {
      return false;
    }
    final parsed = Uri.tryParse(uri);
    return parsed != null;
  }
}

class UnknownOutputUrl extends OutputUrl {
  static const KVS_FIELD = 'kvs';
  static const AZURE_FIELD = 'azure';

  static const TEST_URL = 'unknown://test';
  static const FAKE_URL = 'unknown://fake';
  static const WEBRTC_URL = 'unknown://webrtc';
  static const KVS_URL = 'unknown://kvs';
  static const AZURE_URL = 'unknown://azure';

  static const List<Protocol> protocols = [Protocol.UNKNOWN];

  KVSProp? kvs;
  AzureProp? azure;

  UnknownOutputUrl({required int id, required String uri, this.kvs, this.azure})
      : super(id: id, uri: uri);

  UnknownOutputUrl.createTest({required int id}) : super(id: id, uri: TEST_URL);

  UnknownOutputUrl.createFake({required int id}) : super(id: id, uri: FAKE_URL);

  UnknownOutputUrl.createWebRTC({required int id}) : super(id: id, uri: WEBRTC_URL);

  UnknownOutputUrl.createKVS({required int id, required this.kvs}) : super(id: id, uri: KVS_URL);

  UnknownOutputUrl.createAzure({required int id, required this.azure})
      : super(id: id, uri: AZURE_URL);

  @override
  UnknownOutputUrl copy() {
    return UnknownOutputUrl(id: id, uri: uri, kvs: kvs, azure: azure);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (kvs != null) {
      result[KVS_FIELD] = kvs!.toJson();
    }
    if (azure != null) {
      result[AZURE_FIELD] = azure!.toJson();
    }
    return result;
  }

  factory UnknownOutputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[OutputUrl.ID_FIELD];
    final uri = json[OutputUrl.URI_FIELD];
    KVSProp? kvs;
    if (json.containsKey(KVS_FIELD)) {
      kvs = KVSProp.fromJson(json[KVS_FIELD]);
    }
    AzureProp? azure;
    if (json.containsKey(AZURE_FIELD)) {
      azure = AzureProp.fromJson(json[AZURE_FIELD]);
    }
    final UnknownOutputUrl result = UnknownOutputUrl(id: id, uri: uri, kvs: kvs, azure: azure);
    return result;
  }
}

class HttpOutputUrl extends OutputUrl {
  static const HTTP_ROOT_FIELD = 'http_root';
  static const HLS_TYPE_FIELD = 'hls_type';
  static const HLSSINK_TYPE_FIELD = 'hlssink_type';
  static const CHUNK_DURATION_FIELD = 'chunk_duration';
  static const PLAYLIST_ROOT_FIELD = 'playlist_root';
  static const TOKEN_FIELD = 'token';

  static const List<Protocol> protocols = [Protocol.HTTP, Protocol.HTTPS];

  HlsSinkType? hlsSinkType;
  String? httpRoot;
  HlsType? hlsType;
  int? chunkDuration;
  String? playlistRoot;
  bool? token;

  HttpOutputUrl(
      {required int id,
      required String uri,
      this.hlsSinkType,
      this.httpRoot,
      this.chunkDuration,
      this.hlsType,
      this.playlistRoot,
      this.token})
      : super(id: id, uri: uri);

  @override
  HttpOutputUrl copy() {
    return HttpOutputUrl(
        id: id,
        uri: uri,
        httpRoot: httpRoot,
        hlsType: hlsType,
        hlsSinkType: hlsSinkType,
        chunkDuration: chunkDuration,
        playlistRoot: playlistRoot,
        token: token);
  }

  HttpOutputUrl.createDefaultHLS(
      {required int id, String uri = 'http://fastocloud.com:8010/master.m3u8'})
      : hlsSinkType = HlsSinkType.HLSSINK2,
        httpRoot = '~/streamer/hls/',
        hlsType = HlsType.HLS_PULL,
        token = false,
        super(id: id, uri: uri);

  HttpOutputUrl.createDefaultHTTP({required int id, String uri = 'http://fastocloud.com/master'})
      : token = false,
        super(id: id, uri: uri);

  bool isHls() {
    if (httpRoot != null && hlsSinkType != null && hlsType != null) {
      return true;
    }
    return false;
  }

  factory HttpOutputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[OutputUrl.ID_FIELD];
    final uri = json[OutputUrl.URI_FIELD];
    final HttpOutputUrl result = HttpOutputUrl(id: id, uri: uri);
    if (json.containsKey(TOKEN_FIELD)) {
      result.token = json[TOKEN_FIELD];
    }
    if (json.containsKey(HTTP_ROOT_FIELD) &&
        json.containsKey(HLS_TYPE_FIELD) &&
        json.containsKey(HLSSINK_TYPE_FIELD)) {
      result.hlsSinkType = HlsSinkType.fromInt(json[HLSSINK_TYPE_FIELD]);
      result.httpRoot = json[HTTP_ROOT_FIELD];
      result.hlsType = HlsType.fromInt(json[HLS_TYPE_FIELD]);
      if (json.containsKey(CHUNK_DURATION_FIELD)) {
        result.chunkDuration = json[CHUNK_DURATION_FIELD];
      }
      if (json.containsKey(PLAYLIST_ROOT_FIELD)) {
        result.playlistRoot = json[PLAYLIST_ROOT_FIELD];
      }
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (token != null) {
      result[HttpOutputUrl.TOKEN_FIELD] = token!;
    }

    if (httpRoot != null && hlsType != null && hlsSinkType != null) {
      result[HttpOutputUrl.HLSSINK_TYPE_FIELD] = hlsSinkType!.toInt();
      result[HttpOutputUrl.HTTP_ROOT_FIELD] = httpRoot;
      result[HttpOutputUrl.HLS_TYPE_FIELD] = hlsType!.toInt();
      if (chunkDuration != null) {
        result[HttpOutputUrl.CHUNK_DURATION_FIELD] = chunkDuration;
      }
      if (playlistRoot != null) {
        result[PLAYLIST_ROOT_FIELD] = playlistRoot;
      }
    }
    return result;
  }
}

class UdpOutputUrl extends OutputUrl {
  static const MULTICAST_IFACE_FIELD = 'multicast_iface';

  static const protocol = Protocol.UDP;

  String? multicastIface;

  UdpOutputUrl({required int id, required String uri, this.multicastIface})
      : super(id: id, uri: uri);

  @override
  UdpOutputUrl copy() {
    return UdpOutputUrl(id: id, uri: uri, multicastIface: multicastIface);
  }

  UdpOutputUrl.createDefault({required int id}) : super(id: id, uri: 'udp://fastocloud.com:5004');

  factory UdpOutputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[InputUrl.ID_FIELD];
    final uri = json[InputUrl.URI_FIELD];
    final UdpOutputUrl result = UdpOutputUrl(id: id, uri: uri);
    if (json.containsKey(MULTICAST_IFACE_FIELD)) {
      result.multicastIface = json[MULTICAST_IFACE_FIELD];
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (multicastIface != null) {
      result[MULTICAST_IFACE_FIELD] = multicastIface;
    }
    return result;
  }
}

class SrtOutputUrl extends OutputUrl {
  static const SRT_KEY_FIELD = 'srt_key';
  static const SRT_MODE_FIELD = 'srt_mode';

  static const protocol = Protocol.SRT;

  SrtMode? mode;
  SrtKey? srtKey;

  SrtOutputUrl({required int id, required String uri, this.mode, this.srtKey})
      : super(id: id, uri: uri);

  @override
  SrtOutputUrl copy() {
    final SrtKey? key = srtKey?.copy();
    return SrtOutputUrl(id: id, uri: uri, mode: mode, srtKey: key);
  }

  SrtOutputUrl.createDefault({required int id})
      : mode = SrtMode.LISTENER,
        super(id: id, uri: 'srt://:2001');

  factory SrtOutputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[InputUrl.ID_FIELD];
    final uri = json[InputUrl.URI_FIELD];
    final SrtOutputUrl result = SrtOutputUrl(id: id, uri: uri);

    if (json.containsKey(SrtInputUrl.SRT_MODE_FIELD)) {
      result.mode = SrtMode.fromInt(json[SrtInputUrl.SRT_MODE_FIELD]);
    }
    if (json.containsKey(SrtOutputUrl.SRT_KEY_FIELD)) {
      result.srtKey = SrtKey.fromJson(json[SrtOutputUrl.SRT_KEY_FIELD]);
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (mode != null) {
      result[SrtInputUrl.SRT_MODE_FIELD] = mode!.toInt();
    }
    if (srtKey != null) {
      result[SrtOutputUrl.SRT_KEY_FIELD] = srtKey!.toJson();
    }
    return result;
  }
}

class GoogleOutputUrl extends OutputUrl {
  static const GOOGLE_FIELD = 'google';

  static const protocol = Protocol.GS;

  GoogleProp google;

  GoogleOutputUrl({required int id, required String uri, required this.google})
      : super(id: id, uri: uri);

  @override
  GoogleOutputUrl copy() {
    final GoogleProp key = google.copy();
    return GoogleOutputUrl(id: id, uri: uri, google: key);
  }

  GoogleOutputUrl.createDefault({required int id})
      : google = GoogleProp.createDefault(),
        super(id: id, uri: 'gs://example.com/war/test.mp4');

  factory GoogleOutputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[OutputUrl.ID_FIELD];
    final uri = json[OutputUrl.URI_FIELD];
    final google = json[GoogleOutputUrl.GOOGLE_FIELD];
    return GoogleOutputUrl(id: id, uri: uri, google: GoogleProp.fromJson(google));
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[GoogleOutputUrl.GOOGLE_FIELD] = google.toJson();
    return result;
  }
}

class RtmpOutputUrl extends OutputUrl {
  static const RTMPSINK_TYPE_FIELD = 'rtmpsink_type';

  static const List<Protocol> protocols = [
    Protocol.RTMP,
    Protocol.RTMPS,
    Protocol.RTMPT,
    Protocol.RTMPE,
    Protocol.RTMFP
  ];

  RtmpSinkType? rtmpSinkType;

  RtmpOutputUrl({required int id, required String uri, this.rtmpSinkType})
      : super(id: id, uri: uri);

  @override
  RtmpOutputUrl copy() {
    return RtmpOutputUrl(id: id, uri: uri, rtmpSinkType: rtmpSinkType);
  }

  RtmpOutputUrl.createDefault({required int id})
      : rtmpSinkType = RtmpSinkType.RTMPSINK,
        super(id: id, uri: 'rtmp://fastocloud.com:1935/live/test');

  factory RtmpOutputUrl.fromJson(Map<String, dynamic> json) {
    final id = json[OutputUrl.ID_FIELD];
    final uri = json[OutputUrl.URI_FIELD];
    final result = RtmpOutputUrl(id: id, uri: uri);
    if (json.containsKey(RTMPSINK_TYPE_FIELD)) {
      result.rtmpSinkType = RtmpSinkType.fromInt(json[RTMPSINK_TYPE_FIELD]);
    }
    return result;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (rtmpSinkType != null) {
      result[RTMPSINK_TYPE_FIELD] = rtmpSinkType!.toInt();
    }
    return result;
  }
}

OutputUrl makeOutputUrl(Map<String, dynamic> json) {
  final id = json[OutputUrl.ID_FIELD];
  final uri = json[OutputUrl.URI_FIELD];
  final Uri parsed = Uri.parse(uri);
  final proto = Protocol.fromString(parsed.scheme);
  if (HttpOutputUrl.protocols.contains(proto)) {
    return HttpOutputUrl.fromJson(json);
  } else if (proto == SrtOutputUrl.protocol) {
    return SrtOutputUrl.fromJson(json);
  } else if (proto == GoogleOutputUrl.protocol) {
    return GoogleOutputUrl.fromJson(json);
  } else if (proto == UdpOutputUrl.protocol) {
    return UdpOutputUrl.fromJson(json);
  } else if (RtmpOutputUrl.protocols.contains(proto)) {
    if (!json.containsKey(IRtmpOutputUrl.TYPE_FIELD)) {
      return RtmpOutputUrl.fromJson(json);
    }
    final type = json[IRtmpOutputUrl.TYPE_FIELD];
    return IRtmpOutputUrl.fromJson(PubSubStreamType.fromInt(type), json);
  } else if (UnknownOutputUrl.protocols.contains(proto)) {
    return UnknownOutputUrl.fromJson(json);
  }
  return OutputUrl(id: id, uri: uri);
}

extension OutputUrls on List<OutputUrl> {
  bool isValidOutputUrls() {
    if (isEmpty) {
      return false;
    }

    for (final OutputUrl url in this) {
      if (!url.isValid()) {
        return false;
      }
    }
    return true;
  }
}

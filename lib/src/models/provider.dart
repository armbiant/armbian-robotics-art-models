import 'package:fastocloud_dart_models/src/models/types.dart';

class ProviderStatus {
  static const _NOT_ACTIVE_CONSTANT = 0;
  static const _ACTIVE_CONSTANT = 1;

  final int _value;

  const ProviderStatus._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NOT_ACTIVE_CONSTANT) {
      return 'NOT ACTIVE';
    }

    assert(_value == _ACTIVE_CONSTANT);
    return 'ACTIVE';
  }

  factory ProviderStatus.fromInt(int status) {
    if (status == _NOT_ACTIVE_CONSTANT) {
      return ProviderStatus.NOT_ACTIVE;
    } else if (status == _ACTIVE_CONSTANT) {
      return ProviderStatus.ACTIVE;
    }

    throw 'Unknown provider status: $status';
  }

  static List<ProviderStatus> get values => [NOT_ACTIVE, ACTIVE];

  static const ProviderStatus NOT_ACTIVE = ProviderStatus._(_NOT_ACTIVE_CONSTANT);
  static const ProviderStatus ACTIVE = ProviderStatus._(_ACTIVE_CONSTANT);
}

class ProviderType {
  static const int _ADMIN_CONSTANT = 0;
  static const int _RESELLER_CONSTANT = 1;

  final int _value;

  const ProviderType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _ADMIN_CONSTANT) {
      return 'ADMIN';
    }

    assert(_value == _RESELLER_CONSTANT);
    return 'RESELLER';
  }

  factory ProviderType.fromInt(int type) {
    if (type == _ADMIN_CONSTANT) {
      return ProviderType.ADMIN;
    } else if (type == _RESELLER_CONSTANT) {
      return ProviderType.RESELLER;
    }

    throw 'Unknown provider type: $type';
  }

  static List<ProviderType> get values => [ADMIN, RESELLER];

  static const ProviderType ADMIN = ProviderType._(_ADMIN_CONSTANT);
  static const ProviderType RESELLER = ProviderType._(_RESELLER_CONSTANT);
}

class Provider {
  static const _ID_FIELD = 'id';
  static const EMAIL_FIELD = 'email';
  static const FIRST_NAME_FIELD = 'first_name';
  static const LAST_NAME_FIELD = 'last_name';
  static const _PASSWORD_FIELD = 'password';
  static const _CREATED_DATE_FIELD = 'created_date';
  static const TYPE_FIELD = 'type';
  static const STATUS_FIELD = 'status';
  static const LANGUAGE_FIELD = 'language';
  static const COUNTRY_FIELD = 'country';
  static const CREDITS_FIELD = 'credits';
  static const CREDITS_REMAINING_RO_FIELD = 'credits_remaining';

  static const MIN_NAME = 2;
  static const MAX_NAME = 64;

  final String? id;
  final int? createdDate;
  String email;
  String? password;
  String firstName;
  String lastName;
  ProviderType type;
  ProviderStatus status;
  String? language;
  String? country;
  int credits;
  int creditsRemaining;

  Provider(
      {this.id,
      required this.email,
      this.password,
      required this.firstName,
      required this.lastName,
      this.createdDate,
      required this.status,
      required this.type,
      required this.credits,
      required this.creditsRemaining,
      required this.language,
      required this.country});

  Provider.create(
      {required this.email,
      this.password,
      required this.firstName,
      required this.lastName,
      required this.createdDate,
      required this.type,
      required this.status,
      required this.credits,
      required this.creditsRemaining,
      this.language,
      this.country})
      : id = null;

  Provider.createDefault()
      : id = null,
        email = '',
        firstName = '',
        lastName = '',
        createdDate = null,
        password = null,
        status = ProviderStatus.ACTIVE,
        type = ProviderType.ADMIN,
        credits = Credits.DEFAULT,
        creditsRemaining = 0;

  Provider copy() {
    return Provider(
        id: id,
        email: email,
        firstName: firstName,
        password: password,
        lastName: lastName,
        createdDate: createdDate,
        language: language,
        country: country,
        type: type,
        status: status,
        credits: credits,
        creditsRemaining: creditsRemaining);
  }

  bool isAdmin() {
    return type == ProviderType.ADMIN;
  }

  bool isValid() {
    if (language == null || country == null) {
      return false;
    }

    final bool base = email.isNotEmpty &&
        firstName.isNotEmpty &&
        lastName.isNotEmpty &&
        language!.isNotEmpty &&
        country!.isNotEmpty &&
        credits.isValidCredits();

    if (id == null) {
      return base && (password?.isNotEmpty ?? false);
    }

    return base && createdDate != 0;
  }

  factory Provider.fromJson(Map<String, dynamic> json) {
    String? id;
    if (json.containsKey(_ID_FIELD)) {
      id = json[_ID_FIELD];
    }
    String? password;
    if (json.containsKey(_PASSWORD_FIELD)) {
      password = json[_PASSWORD_FIELD];
    }
    int? createdDate;
    if (json.containsKey(_CREATED_DATE_FIELD)) {
      createdDate = json[_CREATED_DATE_FIELD];
    }
    return Provider(
        id: id,
        email: json[EMAIL_FIELD],
        firstName: json[FIRST_NAME_FIELD],
        lastName: json[LAST_NAME_FIELD],
        createdDate: createdDate,
        type: ProviderType.fromInt(json[TYPE_FIELD]),
        status: ProviderStatus.fromInt(json[STATUS_FIELD]),
        credits: json[CREDITS_FIELD],
        creditsRemaining: json[CREDITS_REMAINING_RO_FIELD],
        language: json[LANGUAGE_FIELD],
        country: json[COUNTRY_FIELD],
        password: password);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {
      EMAIL_FIELD: email,
      FIRST_NAME_FIELD: firstName,
      LAST_NAME_FIELD: lastName,
      TYPE_FIELD: type.toInt(),
      STATUS_FIELD: status.toInt(),
      CREDITS_FIELD: credits,
      LANGUAGE_FIELD: language,
      COUNTRY_FIELD: country
    };
    if (id != null) {
      result[_ID_FIELD] = id;
    }
    if (password != null) {
      result[_PASSWORD_FIELD] = password;
    }
    if (createdDate != null) {
      result[_CREATED_DATE_FIELD] = createdDate;
    }
    return result;
  }
}

import 'package:fastocloud_dart_models/src/models/types.dart';
import 'package:fastotv_dart/commands_info/season_info.dart';

class ServerSeason extends Season {
  // #FIXME: CreateDate should be optional like in streams (server fill it)
  ServerSeason(
      {String? id,
      String name = StreamName.SEASON,
      required String icon,
      String backgroundUrl = '',
      List<String>? groups,
      String description = '',
      int season = 1,
      List<String>? episodes,
      int viewCount = 0,
      int createdDate = 0,
      String? pid})
      : super(id, name, backgroundUrl, icon, groups ?? [], description, season, episodes ?? [],
            viewCount, createdDate, pid);

  @override
  ServerSeason copy() {
    final ServerSeason serial = ServerSeason(
        id: id,
        name: name,
        backgroundUrl: background,
        icon: icon,
        groups: groups,
        description: description,
        season: season,
        episodes: episodes,
        viewCount: viewCount);
    return serial;
  }

  ServerSeason copyWith({required String? id, required String name}) {
    final other = copy();
    other.id = id;
    other.name = name;
    return other;
  }

  bool isValid() {
    return name.isNotEmpty && icon.isValidIconUrl();
  }

  factory ServerSeason.fromJson(Map<String, dynamic> json) {
    final Season base = Season.fromJson(json);
    final ServerSeason serial = ServerSeason(
        id: base.id,
        name: base.name,
        backgroundUrl: base.background,
        icon: base.icon,
        groups: base.groups,
        description: base.description,
        season: base.season,
        episodes: base.episodes,
        viewCount: base.viewCount);
    return serial;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    return result;
  }
}

import 'package:fastocloud_dart_models/src/models/server_base.dart';
import 'package:fastotv_dart/commands_info/client_info.dart';

class Machine {
  static const _CPU_FIELD = 'cpu';
  static const _GPU_FIELD = 'gpu';
  static const _LOAD_AVERAGE_FIELD = 'load_average';
  static const _MEMORY_TOTAL_FIELD = 'memory_total';
  static const _MEMORY_FREE_FIELD = 'memory_free';
  static const _HDD_TOTAL_FIELD = 'hdd_total';
  static const _HDD_FREE_FIELD = 'hdd_free';
  static const _BANDWIDTH_IN_FIELD = 'bandwidth_in';
  static const _BANDWIDTH_OUT_FIELD = 'bandwidth_out';
  static const _UPTIME_FIELD = 'uptime';
  static const _TIMESTAMP_FIELD = 'timestamp';
  static const _TOTAL_BYTES_IN_FIELD = 'total_bytes_in';
  static const _TOTAL_BYTES_OUT_FIELD = 'total_bytes_out';

  // actived service fileds
  static const _PROJECT_FIELD = 'project';
  static const _VERSION_FIELD = 'version';
  static const _EXP_TIME = 'expiration_time';
  static const _OS_FIELD = 'os';

  // connector fields
  static const _STATUS_FIELD = 'status';
  static const _SYNCTIME_FIELD = 'synctime';

  double cpu;
  double gpu;
  String loadAverage;
  int memoryTotal;
  int memoryFree;
  int hddTotal;
  int hddFree;
  int bandwidthIn;
  int bandwidthOut;
  int uptime;
  int timestamp;
  int totalBytesIn;
  int totalBytesOut;

  String? version;
  String? project;
  int? expirationTime;
  OperationSystem? os;

  int? syncTime;
  ServerStatus? status;

  Machine(
      {required this.cpu,
      required this.gpu,
      required this.loadAverage,
      required this.memoryTotal,
      required this.memoryFree,
      required this.hddTotal,
      required this.hddFree,
      required this.bandwidthIn,
      required this.bandwidthOut,
      required this.timestamp,
      required this.uptime,
      required this.totalBytesIn,
      required this.totalBytesOut,
      required this.syncTime,
      this.version,
      this.project,
      this.status,
      this.expirationTime,
      this.os});

  Machine.createInit()
      : cpu = 0.0,
        gpu = 0.0,
        loadAverage = '',
        memoryTotal = 0,
        memoryFree = 0,
        hddTotal = 0,
        hddFree = 0,
        bandwidthIn = 0,
        bandwidthOut = 0,
        timestamp = 0,
        uptime = 0,
        totalBytesIn = 0,
        totalBytesOut = 0,
        syncTime = null,
        version = null,
        project = null,
        status = null,
        expirationTime = null,
        os = null;

  factory Machine.fromJson(Map<String, dynamic> json) {
    final double gpu = (json[_GPU_FIELD] as num).toDouble();
    final double cpu = (json[_CPU_FIELD] as num).toDouble();
    final loadAverage = json[_LOAD_AVERAGE_FIELD];
    final memoryTotal = json[_MEMORY_TOTAL_FIELD];
    final memoryFree = json[_MEMORY_FREE_FIELD];
    final hddTotal = json[_HDD_TOTAL_FIELD];
    final hddFree = json[_HDD_FREE_FIELD];
    final bandwidthIn = json[_BANDWIDTH_IN_FIELD];
    final bandwidthOut = json[_BANDWIDTH_OUT_FIELD];
    final uptime = json[_UPTIME_FIELD];
    final timestamp = json[_TIMESTAMP_FIELD];
    final totalBytesIn = json[_TOTAL_BYTES_IN_FIELD];
    final totalBytesOut = json[_TOTAL_BYTES_OUT_FIELD];

    String? version; // from media available on active state
    if (json[_VERSION_FIELD] != null) {
      version = json[_VERSION_FIELD];
    }
    String? project; // from media available on active state
    if (json[_PROJECT_FIELD] != null) {
      project = json[_PROJECT_FIELD];
    }
    int? expirationTime; // from media available on active state
    if (json[_EXP_TIME] != null) {
      expirationTime = json[_EXP_TIME];
    }
    OperationSystem? os; // from media available on active state
    if (json[_OS_FIELD] != null) {
      os = OperationSystem.fromJson(json[_OS_FIELD]);
    }
    ServerStatus? status; // from connector
    if (json[_STATUS_FIELD] != null) {
      status = ServerStatus.fromInt(json[_STATUS_FIELD]);
    }
    int? syncTime; // from connector
    if (json[_SYNCTIME_FIELD] != null) {
      syncTime = json[_SYNCTIME_FIELD];
    }

    return Machine(
        gpu: gpu,
        cpu: cpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        timestamp: timestamp,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        syncTime: syncTime,
        version: version,
        project: project,
        status: status,
        expirationTime: expirationTime,
        os: os);
  }

  Map<String, dynamic> toJson() {
    return {
      _CPU_FIELD: cpu,
      _LOAD_AVERAGE_FIELD: loadAverage,
      _MEMORY_TOTAL_FIELD: memoryTotal,
      _MEMORY_FREE_FIELD: memoryFree,
      _HDD_TOTAL_FIELD: hddTotal,
      _HDD_FREE_FIELD: hddFree,
      _BANDWIDTH_IN_FIELD: bandwidthIn,
      _BANDWIDTH_OUT_FIELD: bandwidthOut,
      _UPTIME_FIELD: uptime,
      _TOTAL_BYTES_IN_FIELD: totalBytesIn,
      _TOTAL_BYTES_OUT_FIELD: totalBytesOut,
      _TIMESTAMP_FIELD: timestamp,
      _VERSION_FIELD: version,
      _PROJECT_FIELD: project,
      _OS_FIELD: os?.toJson(),
      _EXP_TIME: expirationTime,
      _SYNCTIME_FIELD: syncTime,
      _STATUS_FIELD: status?.toInt()
    };
  }

  Machine copy() {
    return Machine(
        cpu: cpu,
        gpu: gpu,
        loadAverage: loadAverage,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        uptime: uptime,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        timestamp: timestamp,
        version: version,
        project: project,
        os: os,
        expirationTime: expirationTime,
        syncTime: syncTime,
        status: status);
  }

  bool isPro() {
    return isActive() && (project == 'fastocloud_pro' || project == 'fastocloud_pro_ml');
  }

  bool isActive() {
    return status == ServerStatus.ACTIVE;
  }
}

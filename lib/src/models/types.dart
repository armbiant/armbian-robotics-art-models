const int _MAX_INTEGER_NUMBER = 1000000;

const DEFAULT_BACKGROUND_URL = 'https://api.fastocloud.com/install/assets/unknown_background.png';
const INVALID_TRAILER_URL = 'https://fastocloud.com/static/video/invalid_trailer.m3u8';

class SrtKey {
  static const _PASSPHRASE_FIELD = 'passphrase';
  static const _KEY_LEN_FIELD = 'pbkeylen';

  String passphrase;
  int keyLen;

  SrtKey(this.passphrase, this.keyLen);

  SrtKey copy() {
    return SrtKey(passphrase, keyLen);
  }

  factory SrtKey.fromJson(Map<String, dynamic> json) {
    final String pass = json[_PASSPHRASE_FIELD];
    final int kl = json[_KEY_LEN_FIELD];
    return SrtKey(pass, kl);
  }

  Map<String, dynamic> toJson() {
    return {_PASSPHRASE_FIELD: passphrase, _KEY_LEN_FIELD: keyLen};
  }
}

class SrtMode {
  static const int _NONE_CONSTANT = 0;
  static const int _CALLER_CONSTANT = 1;
  static const int _LISTENER_CONSTANT = 2;
  static const int _RENDEZVOUS_CONSTANT = 3;

  final int _value;

  const SrtMode._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _NONE_CONSTANT) {
      return 'NONE';
    } else if (_value == _CALLER_CONSTANT) {
      return 'CALLER';
    } else if (_value == _LISTENER_CONSTANT) {
      return 'LISTENER';
    }

    assert(_value == _RENDEZVOUS_CONSTANT);
    return 'RENDEZVOUS';
  }

  factory SrtMode.fromInt(int mode) {
    if (mode == _NONE_CONSTANT) {
      return SrtMode.NONE;
    } else if (mode == _CALLER_CONSTANT) {
      return SrtMode.CALLER;
    } else if (mode == _LISTENER_CONSTANT) {
      return SrtMode.LISTENER;
    } else if (mode == _RENDEZVOUS_CONSTANT) {
      return SrtMode.RENDEZVOUS;
    }

    throw 'Unknown srt mode: $mode';
  }

  static List<SrtMode> get values => [NONE, CALLER, LISTENER, RENDEZVOUS];

  static const SrtMode NONE = SrtMode._(_NONE_CONSTANT);
  static const SrtMode CALLER = SrtMode._(_CALLER_CONSTANT);
  static const SrtMode LISTENER = SrtMode._(_LISTENER_CONSTANT);
  static const SrtMode RENDEZVOUS = SrtMode._(_RENDEZVOUS_CONSTANT);
}

extension Credits on int {
  static const int MIN = 1;
  static const int MAX = _MAX_INTEGER_NUMBER;
  static const int DEFAULT = 100;

  bool isValidCredits() {
    return this >= MIN && this <= MAX;
  }
}

extension IARC on int {
  static const int MIN = 0;
  static const int MAX = 21;
  static const int DEFAULT = 18;

  bool isValidIARC() {
    return this >= MIN && this <= MAX;
  }
}

extension Price on double {
  static const double MIN = 0.0;
  static const double MAX = 1000.0;
  static const double DEFAULT = Price.MIN;

  bool isValidPrice() {
    return this >= MIN && this <= MAX;
  }
}

extension ServiceName on String {
  static const String DEFAULT = 'Server';

  static const MIN_LENGTH = 3;
  static const MAX_LENGTH = 30;

  bool isValidServiceName() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension StreamName on String {
  static const String DEFAULT = 'Stream';
  static const String SERIAL = 'Serial';
  static const String SEASON = 'Season';
  static const String VOD = 'Vod';

  static const MIN_LENGTH = 1;
  static const MAX_LENGTH = 256;

  bool isValidStreamName() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension Country on String {
  static const String DEFAULT = 'USA';

  static const MIN_LENGTH = 1;
  static const MAX_LENGTH = 2048;

  bool isValidCountry() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension StreamDescription on String {
  static const String DEFAULT = 'Some description';

  static const MIN_LENGTH = 1;
  static const MAX_LENGTH = 4096;

  bool isValidStreamDescription() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension EpgID on String {
  static const MIN_LENGTH = 1;
  static const MAX_LENGTH = 64;

  bool isValidEpgID() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension EpgName on String {
  static const MIN_LENGTH = 1;
  static const MAX_LENGTH = 64;

  bool isValidEpgName() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension IconUrl on String {
  static const int MIN_LENGTH = 3;
  static const int MAX_LENGTH = 2048;

  bool isValidIconUrl() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension StreamUrl on String {
  static const int MIN_LENGTH = 3;
  static const int MAX_LENGTH = 2048;

  bool isValidStreamUrl() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension RestartAttempts on int {
  static const int MIN = 1;
  static const int MAX = _MAX_INTEGER_NUMBER;
  static const int DEFAULT = 10;

  bool isValidRestartAttempts() {
    return this >= MIN && this <= MAX;
  }
}

extension AutoExitTime on int {
  static const MIN = 1;
  static const MAX = _MAX_INTEGER_NUMBER;

  bool isValidAutoExitTime() {
    return this >= MIN && this <= MAX;
  }
}

extension AudioSelect on int {
  static const MIN = 0;
  static const MAX = _MAX_INTEGER_NUMBER;

  bool isValidAudioSelect() {
    return this >= MIN && this <= MAX;
  }
}

extension AudioTracksCount on int {
  static const MIN = 1;
  static const MAX = _MAX_INTEGER_NUMBER;

  bool isValidAudioTracksCount() {
    return this >= MIN && this <= MAX;
  }
}

extension AudioChannelsCount on int {
  static const MIN = 1;
  static const MAX = 8;

  bool isValidAudioChannelsCount() {
    return this >= MIN && this <= MAX;
  }
}

extension UserScore on double {
  static const double MIN = 0.0;
  static const double MAX = 10.0;
  static const double DEFAULT = UserScore.MIN;

  bool isValidUserScore() {
    return this >= MIN && this <= MAX;
  }
}

extension TrailerUrl on String {
  static const int MIN_LENGTH = 3;
  static const int MAX_LENGTH = 2048;

  bool isValidTrailerUrl() {
    return length >= MIN_LENGTH && length < MAX_LENGTH;
  }
}

extension VodDuration on int {
  static const MAX = 3600 * 1000 * 24 * 365;
  static const int MIN = 0;
  static const int DEFAULT = 3600 * 1000;

  bool isValidVodDuration() {
    return this >= MIN && this <= MAX;
  }
}

extension Volume on double {
  static const double DEFAULT = 1.0;
  static const double MAX = 10.0;
  static const double MIN = 0.0;

  bool isValidVolume() {
    return this >= MIN && this <= MAX;
  }
}

extension BitRate on int {
  bool isValidBitRate() {
    return this != 0;
  }
}

extension VideoFlip on int {
  static const int MIN = 0;
  static const int MAX = 8;
  static const int DEFAULT = VideoFlip.MIN;

  bool isValidVideoFlip() {
    return this >= MIN && this <= MAX;
  }
}

extension Alpha on double {
  static const double MIN = 0.0;
  static const double MAX = 1.0;
  static const double DEFAULT = Alpha.MAX;

  bool isValidAlpha() {
    return this >= MIN && this <= MAX;
  }
}

extension FilePath on String {
  static const int MIN_PATH_LENGTH = 1;
  static const int MAX_PATH_LENGTH = 256;

  bool isValidFilePath() {
    return length >= MIN_PATH_LENGTH && length < MAX_PATH_LENGTH;
  }
}

class Protocol {
  static const String HTTP_PROTOCOL = 'http';
  static const String HTTPS_PROTOCOL = 'https';
  static const String FILE_PROTOCOL = 'file';
  static const String UDP_PROTOCOL = 'udp';
  static const String TCP_PROTOCOL = 'tcp';
  static const String RTMP_PROTOCOL = 'rtmp';
  static const String RTMPS_PROTOCOL = 'rtmps';
  static const String RTMPT_PROTOCOL = 'rtmpt';
  static const String RTMPE_PROTOCOL = 'rtmpe';
  static const String RTMFP_PROTOCOL = 'rtmfp';
  static const String RTMPTE_PROTOCOL = 'rtmpte';
  static const String RTMPTS_PROTOCOL = 'rtmpts';
  static const String DEV_PROTOCOL = 'dev';
  static const String RTSP_PROTOCOL = 'rtsp';
  static const String RTP_PROTOCOL = 'rtp';
  static const String SRT_PROTOCOL = 'srt';
  static const String GS_PROTOCOL = 'gs';
  static const String UNKNOWN_PROTOCOL = 'unknown';

  final String _value;

  const Protocol._(this._value);

  @override
  String toString() {
    return _value;
  }

  factory Protocol.fromString(String value) {
    if (value == HTTP_PROTOCOL) {
      return HTTP;
    } else if (value == HTTPS_PROTOCOL) {
      return HTTPS;
    } else if (value == FILE_PROTOCOL) {
      return FILE;
    } else if (value == UDP_PROTOCOL) {
      return UDP;
    } else if (value == TCP_PROTOCOL) {
      return TCP;
    } else if (value == RTMP_PROTOCOL) {
      return RTMP;
    } else if (value == RTMPS_PROTOCOL) {
      return RTMPS;
    } else if (value == RTMPT_PROTOCOL) {
      return RTMPT;
    } else if (value == RTMPE_PROTOCOL) {
      return RTMPE;
    } else if (value == RTMFP_PROTOCOL) {
      return RTMFP;
    } else if (value == RTMPTE_PROTOCOL) {
      return RTMPTE;
    } else if (value == RTMPTS_PROTOCOL) {
      return RTMPTS;
    } else if (value == DEV_PROTOCOL) {
      return DEV;
    } else if (value == RTSP_PROTOCOL) {
      return RTSP;
    } else if (value == RTP_PROTOCOL) {
      return RTP;
    } else if (value == SRT_PROTOCOL) {
      return SRT;
    } else if (value == GS_PROTOCOL) {
      return GS;
    }
    return UNKNOWN;
  }

  static List<Protocol> get values => [
        HTTP,
        HTTPS,
        FILE,
        UDP,
        TCP,
        RTMP,
        RTMPS,
        RTMPT,
        RTMPE,
        RTMFP,
        DEV,
        RTSP,
        RTP,
        SRT,
        GS,
        UNKNOWN
      ];

  static const Protocol HTTP = Protocol._(HTTP_PROTOCOL);
  static const Protocol HTTPS = Protocol._(HTTPS_PROTOCOL);
  static const Protocol FILE = Protocol._(FILE_PROTOCOL);
  static const Protocol UDP = Protocol._(UDP_PROTOCOL);
  static const Protocol TCP = Protocol._(TCP_PROTOCOL);
  static const Protocol RTMP = Protocol._(RTMP_PROTOCOL);
  static const Protocol RTMPS = Protocol._(RTMPS_PROTOCOL);
  static const Protocol RTMPT = Protocol._(RTMPT_PROTOCOL);
  static const Protocol RTMPE = Protocol._(RTMPE_PROTOCOL);
  static const Protocol RTMFP = Protocol._(RTMFP_PROTOCOL);
  static const Protocol RTMPTE = Protocol._(RTMPTE_PROTOCOL);
  static const Protocol RTMPTS = Protocol._(RTMPTS_PROTOCOL);
  static const Protocol DEV = Protocol._(DEV_PROTOCOL);
  static const Protocol RTSP = Protocol._(RTSP_PROTOCOL);
  static const Protocol RTP = Protocol._(RTP_PROTOCOL);
  static const Protocol SRT = Protocol._(SRT_PROTOCOL);
  static const Protocol GS = Protocol._(GS_PROTOCOL);
  static const Protocol UNKNOWN = Protocol._(UNKNOWN_PROTOCOL);
}

extension ActivationKey on String {
  static const int KEY_LENGTH = 97;

  bool isValidActivationKey() {
    if (length != KEY_LENGTH) {
      return false;
    }

    for (var i = 0; i < length; i++) {
      final c = this[i];
      if (!_HEX_ARRAY.contains(c)) {
        return false;
      }
    }
    return true;
  }

  static const _HEX_ARRAY = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f'
  ];
}

extension EpgUpdatePeriod on int {
  static const MIN = 3600;
  static const MAX = _MAX_INTEGER_NUMBER;
  static const DEFAULT = 3600 * 24;

  bool isValidEpgUpdatePeriod() {
    return this >= MIN && this <= MAX;
  }
}

extension DevicesCount on int {
  static const int MAX = _MAX_INTEGER_NUMBER;
  static const int MIN = 0;
  static const DEFAULT = 10;

  bool isValidDevicesCount() {
    return this >= MIN && this <= MAX;
  }
}

class Wpe {
  static const GL_FIELD = 'gl';

  bool gl;

  Wpe({required this.gl});

  Wpe copy() {
    return Wpe(gl: gl);
  }

  Wpe.createDefault() : gl = false;

  factory Wpe.fromJson(Map<String, dynamic> json) {
    final gl = json[GL_FIELD];
    return Wpe(gl: gl);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {GL_FIELD: gl};
    return result;
  }
}

class Cef {
  static const GPU_FIELD = 'gpu';

  bool gpu;

  Cef({required this.gpu});

  Cef copy() {
    return Cef(gpu: gpu);
  }

  Cef.createDefault() : gpu = false;

  factory Cef.fromJson(Map<String, dynamic> json) {
    final gpu = json[GPU_FIELD];
    return Cef(gpu: gpu);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {GPU_FIELD: gpu};
    return result;
  }
}

typedef ExtraConfig = Map<String, dynamic>;

class StreamTTL {
  static const TTL_FIELD = 'ttl';
  static const PHOENIX_FIELD = 'phoenix';

  int ttl;
  bool phoenix;

  StreamTTL({required this.ttl, required this.phoenix});

  StreamTTL copy() {
    return StreamTTL(ttl: ttl, phoenix: phoenix);
  }

  StreamTTL.createDefault(this.ttl) : phoenix = false;

  factory StreamTTL.fromJson(Map<String, dynamic> json) {
    final ttl = json[TTL_FIELD];
    final phoenix = json[PHOENIX_FIELD];
    return StreamTTL(ttl: ttl, phoenix: phoenix);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {TTL_FIELD: ttl, PHOENIX_FIELD: phoenix};
    return result;
  }
}

class NotificationStreamType {
  static const int _STREAM_FINISHED = 0;
  static const int _STREAM_HIGHT_CPU_LOAD = 1;

  final int _value;

  const NotificationStreamType._(this._value);

  NotificationStreamType copy() {
    return NotificationStreamType._(_value);
  }

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _STREAM_FINISHED) {
      return 'STREAM FINISHED';
    }

    assert(_value == _STREAM_HIGHT_CPU_LOAD);
    return 'STREAM HIGHT CPU LOAD';
  }

  factory NotificationStreamType.fromInt(int type) {
    if (type == _STREAM_FINISHED) {
      return NotificationStreamType.STREAM_FINISHED;
    } else if (type == _STREAM_HIGHT_CPU_LOAD) {
      return NotificationStreamType.STREAM_HIGHT_CPU_LOAD;
    }

    throw 'Unknown notification type: $type';
  }

  static List<NotificationStreamType> get values => [STREAM_FINISHED, STREAM_HIGHT_CPU_LOAD];

  static const NotificationStreamType STREAM_FINISHED = NotificationStreamType._(_STREAM_FINISHED);
  static const NotificationStreamType STREAM_HIGHT_CPU_LOAD =
      NotificationStreamType._(_STREAM_HIGHT_CPU_LOAD);
}

class NotificationStreamContact {
  static const EMAIL_FIELD = 'email';
  static const TYPE_FIELD = 'type';

  String email;
  NotificationStreamType type;

  NotificationStreamContact({required this.email, required this.type});

  NotificationStreamContact copy() {
    return NotificationStreamContact(email: email, type: type);
  }

  factory NotificationStreamContact.fromJson(Map<String, dynamic> json) {
    final email = json[EMAIL_FIELD];
    final type = NotificationStreamType.fromInt(json[TYPE_FIELD]);
    return NotificationStreamContact(email: email, type: type);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {EMAIL_FIELD: email, TYPE_FIELD: type.toInt()};
    return result;
  }
}

import 'dart:math';

double fixedDouble(double? value, {int precision = 2}) {
  if (value == null || value == 0 || value.isNaN) {
    return 0.00;
  }

  final double mod = pow(10.0, precision) as double;
  final int rounded = (value * mod).round();
  return rounded.toDouble() / mod;
}

String fixedDoubleString(double value, {int precision = 2}) {
  return value.toStringAsFixed(precision);
}

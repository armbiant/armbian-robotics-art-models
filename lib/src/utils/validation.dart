bool isValidDomain(String text) {
  if (text == 'localhost') {
    return true;
  }

  return RegExp(r'^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$').hasMatch(text);
}

bool isValidIPV4(String text) {
  return RegExp(r'^(\d?\d?\d)\.(\d?\d?\d)\.(\d?\d?\d)\.(\d?\d?\d)$').hasMatch(text);
}

bool isValidIPV6(String text) {
  return RegExp(r'^::|^::1|^([a-fA-F0-9]{1,4}::?){1,7}([a-fA-F0-9]{1,4})$').hasMatch(text);
}
